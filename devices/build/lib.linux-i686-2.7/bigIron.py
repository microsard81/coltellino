# AUTHOR: Luca Carta

__author__  = "Luca Carta - CRS4 - luca@crs4.it"
__date__    = "2015/05/05"
__version__ = "0.1"
__comment__ = "Script per l'esecuzione di vari comandi sui BigIron"

import pexpect
from collections import OrderedDict
from numpy import arange

def connetti(username, password, secret, device, prompt):	
	try:
		ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
		child = pexpect.spawn(ssh + username + "@" + device)
		child.expect(username + "@" + device + "\'s password:")
		child.sendline(password)
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password:")
		child.sendline(secret)
		child.expect(prompt + "#")
		child.sendline("ter len 0")
		child.expect(prompt + "#")
		return (child, prompt)
		#
	except Exception, e:
		print "Error in connetti(): %s" % str(e)

def esegui(chi, comando):
	try:
		child = chi[0]
		prompt = chi[1]
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		child.sendline(comando)
		child.expect(prompt + "#", timeout=240)
		da_loggare = str(child.before)
		return da_loggare.split("\r\n")[1:]
	except Exception, e:
		print "Error in esegui(): %s" % str(e)
		return {"error": str(error)}

def chiudi(chi):
	try:
		child = chi[0]
		prompt = chi[1]
		child.sendline("exit")
		child.expect(prompt + ">")
		child.sendline("exit")
		child.close()
	except Exception, e:
		print "Error in chiudi(): %s" % str(e)

def vlanPortDetail(vlan, chi):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		#
		ports = OrderedDict({})
		#
		for i in vlan:
			unt = []
			tag = []
			for s in vlan[i]:
				b = esegui(chi, "sh vlan " + s)
				b = [ x for x in b if x ]
				for l in b:
					if l.split()[0] == "Untagged":	
						_app = l.replace("ethe","").replace("/",".")
						_app = _app.split(":")[1].strip().split()
						if i in _app: 	
							unt.append(int(s))
						else:
							ver = []
							ff = _app
							for u in range(len(ff)):
								if ff[u][0].isdigit():	ver.append(ff[u].replace(".","/"))
								else:
									ran = arange(float(ff[u-1]), float(ff[u+1])+0.1, 0.1)
									for t in ran: 
										ver.append(str(round(t,2)).replace(".","/"))	
							ver = list(set(ver))
							if i in ver:	unt.append(int(s))
					elif l.split()[0] == "Tagged":
						_app = l.replace("ethe","").replace("/",".")
						_app = _app.split(":")[1].strip().split()
						if i in _app: 	
							tag.append(int(s))
						else:
							ver = []
							ff = _app
							for u in range(len(ff)):
								if ff[u][0].isdigit():	ver.append(ff[u].replace(".","/"))
								else:
									ran = arange(float(ff[u-1]), float(ff[u+1])+0.1, 0.1)
									for t in ran: 
										ver.append(str(round(t,2)).replace(".","/"))	
							ver = list(set(ver))
							if i in ver:	tag.append(int(s))
			#	
			ports["ethernet "+i] = {"untagged": unt, "tagged": tag}
		return ports
	except Exception, error:
		print "Error in vlanPort(): %s" % str(error)

def getPortVlan(chi, _port='', _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port and _filter:
			b = esegui(chi, "sh interf brief %s | %s" % (_port, _filter))	
		elif _port:
			b = esegui(chi, "sh interf brief " + _port)	
		elif _filter:
			b = esegui(chi, "sh interf brief | " + _filter)	
		else:
			b = esegui(chi, "sh interf brief")
		#
		b = [ x for x in b if x ]
		vlan = OrderedDict({})
		#
		for i in b:
			if i.split()[0][0].isdigit():
				a =  esegui(chi,'sh vlan ethernet ' + i.split()[0])
				a = [ x for x in a if x ]
				for z in a:	
					if z.split()[0] == "VLANs":
						_val = z.split()
						_val.remove("VLANs")
						_add = []
						for f in range(len(_val)):
							if _val[f] != "to":
									_add.append(_val[f])
							else:
								el = range(int(_val[f-1])+1, int(_val[f+1]))
								if el:	
									for y in el:	
										_add.append(str(y))
						vlan[i.split()[0]] = _add
		#
		ports = vlanPortDetail(vlan, chi)
		return ports
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}

def getPorts(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "sh interf brief " + _port)	
		else:
			b = esegui(chi, "sh interf brief")
		#
		b = [ x for x in b if x ]
		ports = []
		for i in b:
			if i.split()[0][0].isdigit():
				ports.append("ethernet "+i.split()[0])
		#
		return ports
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}

def getPortsAndDesc(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "sh interf brief " + _port)	
		else:
			b = esegui(chi, "sh interf brief")
		#
		b = [ x for x in b if x ]
		ports = OrderedDict({})
		for i in b:
			if i.split()[0][0].isdigit():
				ports["ethernet "+i.split()[0]] = i.split()[-1].strip()
		#
		return ports
	except Exception, error:
		print "Error in getPortsAndDesc(): %s" % str(error)
		return {"error": str(error)}

def getMacAddress(chi, _mac='', _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _mac and _filter:
			_mval = ".".join(_mac.replace(":","")[s:s+4] for s in range(0, len(_mac.replace(":","")), 4)).lower()
			b = esegui(chi, "sh mac-address " + _mval + " | " + _filter)
		elif _filter:
			b = esegui(chi, "sh mac-address all | " + _filter)
		elif _mac:
			_mval = ".".join(_mac.replace(":","")[s:s+4] for s in range(0, len(_mac.replace(":","")), 4)).lower()
			b = esegui(chi, "sh mac-address " + _mval)	
		else:
			b = esegui(chi, "sh mac-address all")
		#
		mac = {}
		b = [ x for x in b if x ]
		for i in b:
			if len(i.split()[0]) == 14:	
				mvalue = ":".join(i.split()[0].replace(".","")[s:s+2] for s in range(0, len(i.split()[0].replace(".","")), 2)).upper()
				mac[mvalue] = { "vlan": i.split()[3].strip(), 
								"port": "ethernet "+i.split()[1].strip() }
		#
		return mac
	except Exception, error:
		print "Error in getMacAddress(): %s" % str(error)
		return { "error": str(error) }

def getArp(chi, _address='', _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _address.find(":") != -1 and _filter:
			_address = ".".join(_address.replace(":","")[s:s+4] for s in range(0, len(_address.replace(":","")), 4)).lower()
			print _address
			b = esegui(chi, "sh arp mac-address " + _address + " | " + _filter)
		elif _address and _filter:
			b = esegui(chi, "sh arp " + _address + " | " + _filter)	
		elif _address.find(":") != -1:
			_address = ".".join(_address.replace(":","")[s:s+4] for s in range(0, len(_address.replace(":","")), 4)).lower()
			print _address
			b = esegui(chi, "sh arp mac-address " + _address)
		elif _address:
			b = esegui(chi, "sh arp " + _address)
		elif _filter:
			b = esegui(chi, "sh arp | " + _filter)	
		else:
			b = esegui(chi, "sh arp")
		#
		arp = OrderedDict({})
		b = [ x for x in b if x ]
		for i in b:
			if i.split()[0].isdigit():	
				if i.split()[2].find(":") != -1 or i.split()[2].find(".") != -1:
					mvalue = ":".join(i.split()[2].replace(".","")[s:s+2] for s in range(0, len(i.split()[2].replace(".","")), 2)).upper()
				else:
					mvalue = i.split()[2]
				arp[i.split()[1]] = { "port": ("ethernet "+i.split()[5].strip() if i.split()[6].strip() == "Valid" else i.split()[5].strip()),
									  "mac": mvalue,
									  "status": i.split()[6].strip() }
		#
		return arp
	except Exception, error:
		print "Error in getArp(): %s" % str(error)
		return { "error": str(error) }
