#!/usr/bin python
# -*- coding: utf-8 -*-
# AUTHOR: Luca Carta

from distutils.core import setup
setup(name='hp', 
	  version='0.2',
	  description='Python utilities for HP switches', 
	  author='Luca Carta',
      author_email='luca@crs4.it',
	  py_modules=['hp'])
setup(name='brocade', 
	  version='0.12', 
	  description='Python utilities for Brocade switches',
	  author='Luca Carta',
      author_email='luca@crs4.it',
	  py_modules=['brocade'])
setup(name='extreme', 
	  version='0.4', 
	  description='Python utilities for Extreme Networks switches',
	  author='Luca Carta',
      author_email='luca@crs4.it',
	  py_modules=['extreme'])
setup(name='cisco', 
	  version='0.4',
	  description='Python utilities for Cisco switches', 
	  author='Luca Carta',
      author_email='luca@crs4.it',
	  py_modules=['cisco'])
setup(name='c300', 
	  version='0.9', 
	  description='Python utilities for Force10 switches',
	  author='Luca Carta',
      author_email='luca@crs4.it',
	  py_modules=['c300'])
setup(name='SRX', 
	  version='0.2', 
	  description='Python utilities for Juniper SRX firewalls',
	  author='Luca Carta',
      author_email='luca@crs4.it',
	  py_modules=['SRX'])
