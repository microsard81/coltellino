# AUTHOR: Luca Carta

__author__  = "Luca Carta - CRS4 - luca@crs4.it"
__date__    = "2015/07/17"
__version__ = "0.12"
__comment__ = "Script per l'esecuzione di vari comandi sui Brocade"

import pexpect
from collections import OrderedDict
from numpy import arange
from time import sleep

def connetti(username, password, secret, device, prompt):	
	try:
		ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
		child = pexpect.spawn(ssh + username + "@" + device)
		try:
			child.expect(username + "@" + device + "\'s password:", timeout=6)
			child.sendline(password)
		except:
			child.expect("Password:", timeout=6)
			child.sendline(password)
		child.expect(prompt + ">", timeout=6)
		child.sendline("enable")
		child.expect("Password:", timeout=6)
		child.sendline(secret)
		child.expect(prompt + "#", timeout=6)
		device = getDevice((child, prompt))
		if device[0] == "BigIron RX Router":
			child.sendline("ter len 0")
			child.expect(prompt + "#", timeout=6)
		else:
			child.sendline("skip-page-display")
			child.expect(prompt + "#", timeout=6)
		#
		return (child, prompt)
		#
	except Exception, e:
		print "Error in connetti(): %s" % str(e)

def esegui(chi, comando):
	try:
		child = chi[0]
		prompt = chi[1]
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		child.sendline(comando)
		child.expect(prompt + "#", timeout=8)
		da_loggare = str(child.before)
		return da_loggare.split("\r\n")[1:]
	except Exception, e:
		print "Error in esegui(): %s" % str(e)
		return {"error": str(e)}

def chiudi(chi):
	try:
		child = chi[0]
		prompt = chi[1]
		child.sendline("exit")
		child.expect(prompt + ">")
		child.sendline("exit")
		child.close()
	except Exception, e:
		print "Error in chiudi(): %s" % str(e)


def getDevice(chi):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		b = esegui(chi, "sh version | inc HW:")
		b = [ x for x in b if x ]
		version = ""
		platform = b[0].split(": ")[1].strip()
		b = esegui(chi, "sh version | inc SW:")
		b = [ x for x in b if x ]
		if b:
			version = b[0].split(": ")[1].split()[1][:-4]
		else:
			b = esegui(chi, "sh version | inc IronWare")
			b = [ x for x in b if x ]
			version = b[0].split(": ")[1].split()[1][:-4]
		if not version or not platform:
			raise Exception("Something was wrong. Please, check you hardware platform!")
		return [ platform, version ]
	except Exception, error:
		print "Error in getVersion(): %s" % str(error)
		return {"error": str(error)}


def vlanPortDetail(vlan, chi):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		#
		ports = OrderedDict({})
		#
		for i in vlan:
			unt = []
			tag = []
			for s in vlan[i]:
				b = esegui(chi, "sh vlan " + s)
				b = [ x for x in b if x ]
				for l in b:
					if l.split()[0] == "Untagged":	
						_app = l.replace("ethe","").replace("/",".")
						_app = _app.split(":")[1].strip().split()
						if i in _app: 	
							unt.append(int(s))
						else:
							ver = []
							ff = _app
							for u in range(len(ff)):
								if ff[u][0].isdigit():	ver.append(ff[u].replace(".","/"))
								else:
									ran = arange(float(ff[u-1]), float(ff[u+1])+0.1, 0.1)
									for t in ran: 
										ver.append(str(round(t,2)).replace(".","/"))	
							ver = list(set(ver))
							if i in ver:	unt.append(int(s))
					elif l.split()[0] == "Tagged":
						_app = l.replace("ethe","").replace("/",".")
						_app = _app.split(":")[1].strip().split()
						if i in _app: 	
							tag.append(int(s))
						else:
							ver = []
							ff = _app
							for u in range(len(ff)):
								if ff[u][0].isdigit():	ver.append(ff[u].replace(".","/"))
								else:
									ran = arange(float(ff[u-1]), float(ff[u+1])+0.1, 0.1)
									for t in ran: 
										ver.append(str(round(t,2)).replace(".","/"))	
							ver = list(set(ver))
							if i in ver:	tag.append(int(s))
			#	
			ports["ethernet "+i] = {"untagged": unt, "tagged": tag}
		return ports
	except Exception, error:
		print "Error in vlanPortDetail(): %s" % str(error)

def vlanPortDetailEdge(vlan, chi):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		#
		ports = OrderedDict({})
		#
		for i in vlan:
			unt = []
			tag = []
			for s in vlan[i]:
				b = esegui(chi, "sh vlan " + s)
				b = [ x for x in b if x ]
				_ad = False
				for z in b:
					if "PORT-VLAN" in z.split()[0]:
						_ad = True
					elif "Uplink" in z:
						_ad = False
					if _ad:
						_p = z.split(": ")
						if "Untagged" in _p[0]:
							if _p[1].split()[0] != "None":
								to_v = _p[1].split()[0].strip()[1:-1]
								to_v = "/".join([ x for x in to_v if x.isdigit() ]) + "/"
								_v = _p[1].split()[1:]
								for y in range(len(_v)):
									_v[y] = to_v + _v[y] 
								for y in _v:
									if i == y:
										unt.append(s)
						elif "Tagged" in _p[0]:
							if _p[1].split()[0] != "None":
								to_v = _p[1].split()[0].strip()[1:-1]
								to_v = "/".join([ x for x in to_v if x.isdigit() ]) + "/"
								_v = _p[1].split()[1:]
								for y in range(len(_v)):
									_v[y] = to_v + _v[y] 
								for y in _v:
									if i == y:
										tag.append(s)
			#	
			ports["ethernet "+i] = {"untagged": unt, "tagged": tag}
		return ports
	except Exception, error:
		print "Error in vlanPortDetailEdge(): %s" % str(error)


def getPortVlan(chi, _port='', _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port and _filter:
			b = esegui(chi, "sh interf brief %s | %s" % (_port, _filter))	
		elif _port:
			b = esegui(chi, "sh interf brief " + _port)	
		elif _filter:
			b = esegui(chi, "sh interf brief | " + _filter)	
		else:
			b = esegui(chi, "sh interf brief")
		#
		b = [ x for x in b if x ]
		vlan = OrderedDict({})
		#
		device = getDevice(chi)
		if type(device) == dict:
			raise Exception(device['error'])
		else:
			platform = device[0]
			version = device[1]
		#
		if platform != "BigIron RX Router":
			for i in b:
				if i.split()[0][0].isdigit():
					a =  esegui(chi,'sh vlan ethernet ' + i.split()[0])
					a = [ x for x in a if x ]
					_add = []
					for z in a:
						if z.split()[0] in "PORT-VLAN":
							_add.append(z.split(",")[0].split()[1])
					vlan[i.split()[0]] = _add
			ports = vlanPortDetailEdge(vlan, chi)
		else:
			for i in b:
				if i.split()[0][0].isdigit():
					a =  esegui(chi,'sh vlan ethernet ' + i.split()[0])
					a = [ x for x in a if x ]
					for z in a:	
						if z.split()[0] == "VLANs":
							_val = z.split()
							_val.remove("VLANs")
							_add = []
							for f in range(len(_val)):
								if _val[f] != "to":
										_add.append(_val[f])
								else:
									el = range(int(_val[f-1])+1, int(_val[f+1]))
									if el:	
										for y in el:	
											_add.append(str(y))
							vlan[i.split()[0]] = _add
			ports = vlanPortDetail(vlan, chi)
		#
		return ports
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}

def getPorts(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "sh interf brief " + _port)	
		else:
			b = esegui(chi, "sh interf brief")
		#
		b = [ x for x in b if x ]
		ports = []
		for i in b:
			if i.split()[0][0].isdigit():
				ports.append("ethernet "+i.split()[0])
		#
		return ports
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}

def getPortsAndDesc(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "sh interfaces " + _port)
		else:
			b = esegui(chi, "sh interfaces")
		#
		ports = OrderedDict({})
		b = [ x for x in b if x ]
		ETHE = ""
		for i in b:
			if i[0] != " " and "Ethernet" in i: 
				ETHE = i.split()[0].split("net")[1]
			if "Port name is " in i:
				ports["ethernet " + ETHE] = i.split("Port name is ")[-1].strip()
		#
		#
		return ports
	except Exception, error:
		print "Error in getPortsAndDesc(): %s" % str(error)
		return {"error": str(error)}

def getPortsDetail(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "sh interfaces " + _port)
		else:
			b = esegui(chi, "sh interfaces")
		#
		ports = OrderedDict({})
		ETHE = ""
		b = [ x for x in b if x ]
		for i in b:
			if i[0] != " " and "Ethernet" in i: 
				ETHE = i.split()[0].split("net")[1]
				ports["ethernet " + ETHE] = ""
			#
			ports["ethernet " + ETHE] += i + "\n"
		#
		#
		return ports
	except Exception, error:
		print "Error in getPortsDetail(): %s" % str(error)
		return {"error": str(error)}

def getMacAddress(chi, _mac='', _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _mac and _filter:
			_mval = ".".join(_mac.replace(":","")[s:s+4] for s in range(0, len(_mac.replace(":","")), 4)).lower()
			b = esegui(chi, "sh mac-address " + _mval + " | " + _filter)
		elif _filter:
			b = esegui(chi, "sh mac-address all | " + _filter)
		elif _mac:
			_mval = ".".join(_mac.replace(":","")[s:s+4] for s in range(0, len(_mac.replace(":","")), 4)).lower()
			b = esegui(chi, "sh mac-address " + _mval)	
		else:
			b = esegui(chi, "sh mac-address all")
		#
		mac = {}
		b = [ x for x in b if x ]
		for i in b:
			if len(i.split()[0]) == 14:	
				mvalue = ":".join(i.split()[0].replace(".","")[s:s+2] for s in range(0, len(i.split()[0].replace(".","")), 2)).upper()
				mac[mvalue] = { "vlan": i.split()[-1], 
								"port": "ethernet "+i.split()[1] }
		#
		return mac
	except Exception, error:
		print "Error in getMacAddress(): %s" % str(error)
		return { "error": str(error) }

def getArp(chi, _address='', _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _address.find(":") != -1 and _filter:
			_address = ".".join(_address.replace(":","")[s:s+4] for s in range(0, len(_address.replace(":","")), 4)).lower()
			print _address
			b = esegui(chi, "sh arp mac-address " + _address + " | " + _filter)
		elif _address and _filter:
			b = esegui(chi, "sh arp " + _address + " | " + _filter)	
		elif _address.find(":") != -1:
			_address = ".".join(_address.replace(":","")[s:s+4] for s in range(0, len(_address.replace(":","")), 4)).lower()
			print _address
			b = esegui(chi, "sh arp mac-address " + _address)
		elif _address:
			b = esegui(chi, "sh arp " + _address)
		elif _filter:
			b = esegui(chi, "sh arp | " + _filter)	
		else:
			b = esegui(chi, "sh arp")
		#
		arp = OrderedDict({})
		b = [ x for x in b if x ]
		for i in b:
			if i.split()[0].isdigit():	
				if i.split()[2].find(":") != -1 or i.split()[2].find(".") != -1:
					mvalue = ":".join(i.split()[2].replace(".","")[s:s+2] for s in range(0, len(i.split()[2].replace(".","")), 2)).upper()
				else:
					mvalue = i.split()[2]
				arp[i.split()[1]] = { "port": ("ethernet "+i.split()[5].strip() if i.split()[6].strip() == "Valid" else i.split()[5].strip()),
									  "mac": mvalue,
									  "status": i.split()[6].strip() }
		#
		return arp
	except Exception, error:
		print "Error in getArp(): %s" % str(error)
		return { "error": str(error) }

def setPortsDesc(chi, _port='', _desc=''):
	try:
		child = chi[0]
		prompt = chi[1]
		ports = {}
		#
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		if not _port:
			raise Exception("No port selected. Please, specify port!")
		#
		child.sendline("conf t")
		child.expect("#")
		child.sendline("interf " + _port)
		child.expect("#")
		child.sendline("""port-name %s""" % _desc)
		child.expect("#")
		child.sendline("end")
		child.expect(prompt + "#")
		child.sendline("write mem")
		child.expect(prompt + "#")
		#
		ports = getPortsAndDesc(chi, _port)
		#
		ports = getPortsAndDesc(chi, _port)
		for i in ports:
			ports[i]= (ports[i], "Done")
		#
		return ports
	except Exception, error:
		print "Error in setPortsDesc(): %s" % str(error)
		return {"error": str(error)}

