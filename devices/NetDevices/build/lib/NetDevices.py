# -*- coding: utf-8 -*-
# AUTHOR: Luca Carta

__author__  = "Luca Carta - CRS4 - luca@crs4.it"
__date__    = "2015/09/16"
__version__ = "1.7"
__comment__ = "A complete suite for Brocade, Cisco, HP, Extreme, H3C and Force10 switches management"


import pexpect
import pudb
from collections import OrderedDict
from time import sleep
from numpy import arange


#########################################
#
# HP Class
#
#########################################

class HP():
	"""
	Class for HP switches management.

	"""

	def __init__(self, name=None):
		self.child = pexpect
		self.prompt = None
		self.name = name
		self.device = None

	def getName(self):
		"""
		This method return the instance name
		"""
		return self.name

	def setName(self, name):
		"""
		This method set the instance name.
		"""
		self.name = name

	def connect(self, username, password, secret, device, prompt):	
		"""
		This method is used to start a new switch connection. 
		You must specify: device, username, password, secret and prompt

		Params:
			username = string
			password = string
			secret = string
			device = string
			prompt = string
		"""
		try:
			ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
			self.child = pexpect.spawn(ssh + username + "@" + device)
			self.child.expect(username + "@" + device + "\'s password: ", timeout=8)
			self.child.sendline(password)
			self.child.expect("Press any key to continue", timeout=8)
			self.child.sendline(" ")
			self.child.expect(prompt + ">", timeout=8)
			self.child.sendline("enable")
			self.child.expect("Password: ", timeout=8)
			self.child.sendline(secret)
			self.child.expect(prompt + "#", timeout=10)
			self.child.sendline("no page")
			self.child.expect(prompt + "#", timeout=10)
			self.child.sendline("ter len 1000")
			self.child.expect(prompt + "#", timeout=10)
			self.prompt = prompt
			self.device = device
			#
		except Exception, e:
			return "Error in connect(): %s" % str(e)
	
	def run(self, command, timeout=15):
		"""
		Return a list with the specified command output. Yuo can specify
		a custom timeout. When timeout is different to 15 seconds, the method 
		wait a switch response until timeout is reached.  

		Params:
			command = string
			timeout = integer
		"""
		try:
			self._isClosed()
			self.child.sendline(command)
			self.child.expect(self.prompt + "#", timeout=timeout)
			to_log = str(self.child.before)
			return to_log.replace("\r", "").split("\n")[1:-1]
		except Exception, e:
			return {"error": "Error in run(): %s" % str(e)}
	
	def close(self):
		"""
		This method is used to close the switch connection.
		"""
		try:
			self._isClosed()
			self.child.sendline("exit")
			self.child.expect(self.prompt + ">")
			self.child.sendline("exit")
			self.child.expect("Do you want to log out [y/n]?")
			self.child.close()
		except Exception, e:
			return ["Error in close(): %s" % str(e)]
	
	def _isClosed(self):
		try:
			if self.child.closed:
				return Exception("Session closed. Please, reconnect!")
		except Exception, e:
			return "Error: " + str(e)

	def status(self):
		"""
		This method return the connection status.
		"""
		try:
			if self.child.closed:
				return "Session closed. Please, reconnect!"
			else:
				return "Connected to " + str(self.device)
		except Exception, e:
			return "Error: " + str(e)
	
	def getVersion(self):
		"""
		This method return the switch version.
		"""
		try:
			self._isClosed()
			ver = self.run("sh ver")[1].strip()
			return ver
		except Exception, e:
			return {'error': "Error in getVersion(): " + str(e)}

	def getPortVlan(self, interface=''):
		"""
		Return a dictionary with port vlan details. If you not specify an interface, 
		this method return all port vlan details for each port in the switch.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			vlans = OrderedDict({})
			ports = OrderedDict({})
			version = self.getVersion()
			PORT = ""
			#
			if interface:
				if version[0] >= "S": 
					b = self.run("sh vlans ports " + interface + " detail")	
					b = [x for x in b if x.strip()]
					for i in b:
						if i.split()[0] == "Status":
							PORT = i.split()[-1]
							ports[PORT] = {'untagged': [], 'tagged': []}
						elif i.split()[-1] == "Untagged":
							ports[PORT]['untagged'] += [int(i.split()[0])]
						elif i.split()[-1] == "Tagged":
							ports[PORT]['tagged'] += [int(i.split()[0])]		
				else:
					vlans = self.getVlans()
					_ports = {}
					for i in vlans:
						b = self.run("sh vlans " +i)
						b = [x for x in b if x.strip()]
						for z in b:
							if "Tagged" in z:
								if z.split()[0] not in _ports.keys():	_ports[z.split()[0]] = {'tagged': [int(i)], 'untagged': []}
								else:	_ports[z.split()[0]]['tagged'] += [int(i)]
							elif "Untagged" in z:
								if z.split()[0] not in _ports.keys():	_ports[z.split()[0]] = {'untagged': [int(i)], 'tagged': []}
								else:	_ports[z.split()[0]]['untagged'] += [int(i)]
					ports[interface] = _ports[interface] 
			else:
				trunks = self.getTrunks()
				tot = self.getPorts()
				_ports = []
				for i in trunks:
					_ports += trunks[i]
				_ports = [ int(x) for x in _ports ]
				tot = [ int(x) for x in tot ]
				tot = [x for x in tot if x not in _ports]
				_ports = tot
				ranges = sum((list(t) for t in zip(_ports, _ports[1:]) if t[0]+1 != t[1]), [])
				iranges = iter(_ports[0:1] + ranges + _ports[-1:])
				z = ','.join([str(n) + '-' + str(next(iranges)) for n in iranges])
				s = sorted([x for x in trunks.keys()])
				z += ","+",".join(s)
				if version[0] >= "S":	
					b = self.run("sh vlans ports " + str(z) + " detail")
					b = [x for x in b if x.strip()]
					for i in b:
						if i.split()[0] == "Status":
							PORT = i.split()[-1]
							ports[PORT] = {'untagged': [], 'tagged': []}
						elif i.split()[-1] == "Untagged":
							ports[PORT]['untagged'] += [int(i.split()[0])]
						elif i.split()[-1] == "Tagged":
							ports[PORT]['tagged'] += [int(i.split()[0])]
				else:
					vlans = self.getVlans()
					for i in vlans:
						b = self.run("sh vlans " +i)
						b = [x for x in b if x.strip()]
						for z in b:
							if "Tagged" in z:
								ports[z.split()[0]]['tagged'] += [int(i)]
								ports[z.split()[0]]['untagged'] += []
							elif "Untagged" in z:
								ports[z.split()[0]]['untagged'] += [int(i)]
								ports[z.split()[0]]['tagged'] += []	
			#
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortVlan(): %s" % str(error)}
	
	def getPortsAndDesc(self, interface=''):
		"""
		Return a dictionary with port description. If you not specify an interface, 
		this method return all ports descriptions for each port in the switch.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			ports = OrderedDict({})
			#
			if interface:
				b = self.run("sh interf " + interface)
				for i in b:
					if "Port Counters for port" in i:
						interface = i.split()[-1]
					if "Name  :" in i:
						ports[interface] = i.split(":")[1].strip()	
			else:
				_ports = self.getPorts()
				first = _ports[0]
				last = _ports[-1]
				b = self.run("sh interf "+first+"-"+last)
				for i in b:
					if "Port Counters for port" in i:
						interface = i.split()[-1]
					if "Name  :" in i:
						ports[interface] = i.split(":")[1].strip()
			#
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsAndDesc(): %s" % str(error)}
	
	def getPortsDetail(self, interface=''):
		"""
		Return a dictionary with all port details. If you not specify an interface, 
		this method return all ports details for each port in the switch.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			ports = OrderedDict({})
			#
			PORT = ""
			if interface:
				b = self.run("sh interf " + interface)
				b = [x for x in b if x]
				for i in b:
					if "Port Counters for port" in i:
						PORT = i.split()[-1]
						ports[PORT] = i + "\n"
					else:
						ports[PORT] += i + "\n"	
			else:
				_ports = self.getPorts()
				first = _ports[0]
				last = _ports[-1]
				b = self.run("sh interf "+first+"-"+last)
				b = [x for x in b if x]
				for i in b:
					if "Port Counters for port" in i:
						PORT = i.split()[-1]
						ports[PORT] = i + "\n"
					else:
						ports[PORT] += i + "\n"
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsDetail(): %s" % str(error)}
	
	def getMacAddress(self, macAddress=''):
		"""
		Return a dictionary with port and vlan details, for each mac address.
		If you not specify mac address, this method return all mac address in
		the switch.

		Params:
			macAddress = string
		"""
		try:
			self._isClosed()
			mac = {}
			#
			for i in self.getVlans():
				b = self.run("sh mac-address vlan " + i)
				c = "-------------"
				b = [x for x in b if c not in x and x.strip() and len(x.split()[0]) == 13]
				if b:
					for s in b:
						MAC = ":".join(s.split()[0].replace("-","")[l:l+2] for l in range(0, len(s.split()[0].replace("-","")), 2)).upper()
						mac[MAC] = { "port": s.split()[1], "vlan": i }
			if macAddress:
				_app = {}
				_app[macAddress.upper()] = mac[macAddress.upper()]
				mac = _app
			#
			return mac
		except Exception, error:
			return { "error": "Error in getMacAddress(): %s" % str(error) }
	
	def getVlans(self):
		"""
		Return a list with all vlans.

		Params:
			none
		"""
		try:
			self._isClosed()
			b = self.run("sh vlans")
			b = [x for x in b if x.strip() and x.split()[0].isdigit()]	
			#
			vlans = []
			for i in b:
				vlans.append(i.split()[0])
			#
			return vlans
		except Exception, error:
			return {"error": "Error in getVlans(): %s" % str(error)}
	
	def getPorts(self, port=''):
		"""
		Return a list with all interfaces. Doesn't return Trunk interfaces.

		Params:
			port = string
		"""
		try:
			self._isClosed()
			b = self.run("sh interf brief " + port)
			b = [x for x in b if x.strip()]	
			#
			ports = []
			log = False
			for i in b:
				if log:
					ports.append((i.split()[0]+"-").split("-")[0])
				if "--" in i.split()[0]:
					log = True
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPorts(): %s" % str(error)}
	
	def getTrunks(self, trunk=''):
		"""
		Return a dictionary with all Trunk interfaces.

		Params:
			trunk = string
		"""
		try:
			self._isClosed()
			b = self.run("sh trunks " + trunk)
			b = [x.split("|")[0].strip()+"|"+x.split("|")[2].strip().split()[0] for x in b if x.strip() and x.split()[0].isdigit()]
			#
			trunks = {}
			for i in b:
				if i.split("|")[1] in trunks.keys():
					trunks[i.split("|")[1]] += [i.split("|")[0]]
				else:
					trunks[i.split("|")[1]] = [i.split("|")[0]]
			#
			return trunks
		except Exception, error:
			return {"error": "Error in getTrunks(): %s" % str(error)}
	
	def setPortsDesc(self, port='', desc=''):
		"""
		Set port description. Return a dictionary with vlan and port description.

		ATTENTION!!! DO NOT USE WITH TRUNK INTERFACES!!!

		Params:
			port = string
			desc = string
		"""
		try:
			self._isClosed()
			if not port:
				raise Exception("No port selected. Please, specify port!")
			#
			self.child.sendline("conf t")
			self.child.expect("#")
			self.child.sendline("interf " + port)
			self.child.expect("#")
			self.child.sendline("""name "%s" """ % desc)
			self.child.expect("#")
			self.child.sendline("end")
			self.child.expect(self.prompt + "#")
			self.child.sendline("write mem")
			self.child.expect(self.prompt + "#")
			#
			sleep(0.1)
			ports = self.getPortsAndDesc(port)
			for i in ports:
				ports[i]= (ports[i], "Done")
			#
			return ports
		except Exception, error:
			return {"error": "Error in setPortsDesc(): %s" % str(error)}

	def setPortVlan(self, port='', vlan={"id": 0, "tagged": True, "remove": []}):
		"""
		Set port vlan. You can add or remove vlan from tagged ports, or you can
		change vlan on untagged ports.

		Params:
			port = string
			vlan = dict({"id": int, "tagged": Boolean, "remove": list})
		"""
		try:
			#
			__ret__ = []
			self._isClosed()
			if not port or not vlan['id']:
				raise Exception("No port or vlan provided. Please, specify all parameters!")
			ieeeVlan = range(1,4096) + [5000]
			if vlan['id'] not in ieeeVlan:
				raise Exception("Vlan ID incorrect!")
			#
			#
			if vlan['tagged']:
				self.child.sendline("conf t")
				self.child.expect("#")
				if vlan['remove']:
					for i in vlan['remove']:
						self.child.sendline("vlan " + str(i))
						self.child.expect("#")
						self.child.sendline("no tag " + port)
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				if vlan['id'] == 5000:
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					self.child.sendline("write mem")
					self.child.expect(self.prompt + "#")
					return __ret__
				#
				self.child.sendline("vlan " + str(vlan["id"]))
				self.child.expect("#")
				self.child.sendline("tag " + port)
				self.child.expect("#")
				__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				self.child.sendline("end")
				self.child.expect(self.prompt + "#")
				self.child.sendline("write mem")
				self.child.expect(self.prompt + "#")
			else:
				#
				vlanp = self.getPortVlan(port)[port]
				if not vlanp['tagged']:
					self.child.sendline("conf t")
					self.child.expect("#")
					if vlanp['untagged'][0] != 1:
						self.child.sendline("vlan " + str(vlanp['untagged'][0]))
						self.child.expect("#")
						self.child.sendline("no unt " + port)
						self.child.expect("#")
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					#
					self.child.sendline("vlan " + str(vlan["id"]))
					self.child.expect("#")
					self.child.sendline("untag " + port)
					self.child.expect("#")
					__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					#
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					self.child.sendline("write mem")
					self.child.expect(self.prompt + "#")
				else:
					raise Exception("Port %s is not in access mode. Please contact your network administrator!" % port)
			#
			return __ret__
		except Exception, error:
			return {"error": "Error in setPortVlan(): %s" % str(error)}


#########################################
#
# Brocade Class
#
#########################################

class Brocade():
	"""
	Class for Brocade switches management.

	"""

	def __init__(self, name=None):
		self.child = pexpect
		self.prompt = None
		self.name = name
		self.device = None

	def getName(self):
		"""
		This method return the instance name
		"""
		return self.name

	def setName(self, name):
		"""
		This method set the instance name.
		"""
		self.name = name

	def connect(self, username, password, secret, device, prompt):
		"""
		This method is used to start a new switch connection. 
		You must specify: device, username, password, secret and prompt

		Params:
			username = string
			password = string
			secret = string
			device = string
			prompt = string
		"""
		try:
			ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
			self.prompt = prompt
			self.device = device
			self.child = pexpect.spawn(ssh + username + "@" + device)
			try:
				self.child.expect(username + "@" + device + "\'s password:", timeout=6)
				self.child.sendline(password)
			except:
				self.child.expect("Password:", timeout=6)
				self.child.sendline(password)
			self.child.expect(self.prompt + ">", timeout=6)
			self.child.sendline("enable")
			self.child.expect("Password:", timeout=6)
			self.child.sendline(secret)
			self.child.expect(self.prompt + "#", timeout=6)
			dev = self.getDevice()
			if dev[0] == "BigIron RX Router":
				self.child.sendline("ter len 0")
				self.child.expect(self.prompt + "#", timeout=6)
			else:
				self.child.sendline("skip-page-display")
				self.child.expect(self.prompt + "#", timeout=6)
			#
		except Exception, e:
			return "Error in connect(): %s" % str(e)
	
	def run(self, command, timeout=15):
		"""
		Return a list with the specified command output. Yuo can specify
		a custom timeout. When timeout is different to 15 seconds, the method 
		wait a switch response until timeout is reached.  

		Params:
			command = string
			timeout = integer
		"""
		try:
			self._isClosed()
			self.child.sendline(command)
			self.child.expect(self.prompt + "#", timeout=timeout)
			to_log = str(self.child.before)
			return to_log.replace("\r", "").split("\n")[1:-1]
		except Exception, e:
			return {"error": "Error in run(): %s" % str(e)}
	
	def close(self):
		"""
		This method is used to close the switch connection.
		"""
		try:
			self._isClosed()
			self.child.sendline("exit")
			self.child.expect(self.prompt + ">")
			self.child.sendline("exit")
			self.child.close()
		except Exception, e:
			return ["Error in close(): %s" % str(e)]

	def _isClosed(self):
		try:
			if self.child.closed:
				return Exception("Session closed. Please, reconnect!")
		except Exception, e:
			return "Error: " + str(e)

	def status(self):
		"""
		This method return the connection status.
		"""
		try:
			if self.child.closed:
				return "Session closed. Please, reconnect!"
			else:
				return "Connected to " + str(self.device)
		except Exception, e:
			return "Error: " + str(e)
	
	def getDevice(self):
		"""
		Return a list with platform and version parameters.
		"""
		try:
			self._isClosed()
			b = self.run("sh version | inc HW:")
			b = [ x for x in b if x ]
			version = ""
			platform = b[0].split(": ")[1].strip()
			b = self.run("sh version | inc SW:")
			b = [ x for x in b if x ]
			if b:
				version = b[0].split(": ")[1].split()[1][:-4]
			else:
				b = self.run("sh version | inc IronWare")
				b = [ x for x in b if x ]
				version = b[0].split(": ")[1].split()[1][:-4]
			if not version or not platform:
				raise Exception("Something was wrong. Please, check you hardware platform!")
			return [ platform, version ]
		except Exception, error:
			return {"error": "Error in getDevice(): %s" % str(error)}

	def getVsrp(self):
		"""
		Return a dictionary with vlan keys and vsrp .
		"""
		try:
			self._isClosed()
			b = self.run("show vsrp brief")
			b = [ x for x in b if x ]
			vsrp = {}
			for i in b:
			    if i.split()[0].isdigit():
			            vsrp[int(i.split()[0])]=int(i.split()[1])
			return vsrp
		except Exception, error:
			return {"error": "Error in getVsrp(): %s" % str(error)}
	
	def __vlanPortDetail(self, vlan):
		"""
		Return a dictionary with all ports vlan detail. Yuo can specify
		wich vlan you want check.

		USED ONLY FOR BigIron SWITCHES!

		Params:
			vlan = string
		"""
		try:
			self._isClosed()
			#
			ports = OrderedDict({})
			# print vlan
			#
			for i in vlan:
				unt = []
				tag = []
				for s in vlan[i]:
					b = self.run("sh vlan " + s)
					b = [ x for x in b if x ]
					for l in b:
						if l.split()[0] == "Untagged":	
							_app = l.replace("ethe","").replace("/",".")
							_app = _app.split(":")[1].strip().split()
							if i in _app: 	
								unt.append(int(s))
							else:
								ver = []
								ff = _app
								# print ff
								for u in range(len(ff)):
									if ff[u][0].isdigit():	
										ver.append(ff[u].replace(".","/"))
									else:
										ran = arange(int(ff[u-1].split(".")[1]), int(ff[u+1].split(".")[1])+1, 1)
										for t in ran: 
											ver.append(str(ff[u-1].split(".")[0]+"/"+str(t)))	
								ver = list(set(ver))
								if i in ver:	
									unt.append(int(s))
						elif l.split()[0] == "Tagged":
							_app = l.replace("ethe","").replace("/",".")
							_app = _app.split(":")[1].strip().split()
							if i in _app: 	
								tag.append(int(s))
							else:
								ver = []
								ff = _app
								# print ff
								for u in range(len(ff)):
									if ff[u][0].isdigit():	
										ver.append(ff[u].replace(".","/"))
									else:
										ran = arange(int(ff[u-1].split(".")[1]), int(ff[u+1].split(".")[1])+1, 1)
										for t in ran: 
											ver.append(str(ff[u-1].split(".")[0]+"/"+str(t)))								
								ver = list(set(ver))
								if i in ver:	
									tag.append(int(s))
				#
				if tag and not unt: unt.append(1)
				ports["ethernet "+i] = {"untagged": unt, "tagged": tag}
			return ports
		except Exception, error:
			return "Error in vlanPortDetail(): %s" % str(error)
	
	def __vlanPortDetailEdge(self, vlan):
		"""
		Return a dictionary with all ports vlan detail. Yuo can specify
		wich vlan you want check.

		USED ONLY FOR Edge SWITCHES!

		Params:
			vlan = string
		"""
		try:
			self._isClosed()
			#
			ports = OrderedDict({})
			#
			for i in vlan:
				unt = []
				tag = []
				for s in vlan[i]:
					b = self.run("sh vlan " + s)
					b = [ x for x in b if x ]
					for z in b:
						_p = (z+": ").split(": ")
						if "Untagged" in _p[0]:
							_vlan = _p[1].split()
							if _vlan[0] != "None":
								if _vlan[0].strip()[0].isdigit():
									_v = _vlan
								else:
									to_v = "/".join([ x for x in _vlan[0] if x.isdigit() ]) + "/"
									_v = [ str(to_v + x) for x in _vlan[1:] if x.isdigit() ]
								for y in _v:
									if i == y:
										unt.append(int(s))
						elif "Tagged" in _p[0]:
							_vlan = _p[1].split()
							if _vlan[0] != "None":
								if _vlan[0].strip()[0].isdigit():
									_v = _vlan
								else:
									to_v = "/".join([ x for x in _vlan[0] if x.isdigit() ]) + "/"
									_v = [ str(to_v + x) for x in _vlan[1:] if x.isdigit() ]
								for y in _v:
									if i == y:
										tag.append(int(s))
						elif "DualMode" in _p[0]:
							_vlan = _p[1].split()
							if _vlan[0] != "None":
								if _vlan[0].strip()[0].isdigit():
									_v = _vlan
								else:
									to_v = "/".join([ x for x in _vlan[0] if x.isdigit() ]) + "/"
									_v = [ str(to_v + x) for x in _vlan[1:] if x.isdigit() ]
								for y in _v:
									if i == y:
										unt.append(int(s))
				#	
				if not unt and tag:	unt.append(1)
				ports["ethernet "+i] = {"untagged": unt, "tagged": tag}
			return ports
		except Exception, error:
			return "Error in vlanPortDetailEdge(): %s" % str(error)
	
	def getPortVlan(self, interface='', filters=''):
		"""
		Return a dictionary with port vlan details. If you not specify an interface, 
		this method return all port vlan details for each port in the switch.

		Params:
			interface = string
			filters = string
		"""
		try:
			self._isClosed()
			if interface and filters:
				b = self.run("sh interf brief %s | %s" % (interface, filters))	
			elif interface:
				b = self.run("sh interf brief " + interface)	
			elif filters:
				b = self.run("sh interf brief | " + filters)	
			else:
				b = self.run("sh interf brief")
			#
			b = [ x for x in b if x ]
			vlan = OrderedDict({})
			#
			device = self.getDevice()
			if type(device) == dict:
				raise Exception(device['error'])
			else:
				platform = device[0]
				version = device[1]
			#
			if platform != "BigIron RX Router":
				for i in b:
					if i.split()[0][0].isdigit():
						a =  self.run('sh vlan ethernet ' + i.split()[0])
						a = [ x for x in a if x ]
						_add = []
						for z in a:
							if z.split()[0] in "PORT-VLAN":
								_add.append(z.split(",")[0].split()[1])
						vlan[i.split()[0]] = _add
				ports = self.__vlanPortDetailEdge(vlan)
			else:
				for i in b:
					if i.split()[0][0].isdigit():
						a =  self.run('sh vlan ethernet ' + i.split()[0])
						a = [ x for x in a if x ]
						for z in a:	
							if z.split()[0] == "VLANs":
								_val = z.split()
								_val.remove("VLANs")
								_add = []
								for f in range(len(_val)):
									if _val[f] != "to":
											_add.append(_val[f])
									else:
										el = range(int(_val[f-1])+1, int(_val[f+1]))
										if el:	
											for y in el:	
												_add.append(str(y))
								vlan[i.split()[0]] = _add
				ports = self.__vlanPortDetail(vlan)
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortVlan(): %s" % str(error)}
	
	def getPorts(self, port=''):
		"""
		Return a list with all interfaces.

		Params:
			port = string
		"""
		try:
			self._isClosed()
			if port:
				b = self.run("sh interf brief " + port)	
			else:
				b = self.run("sh interf brief")
			#
			b = [ x for x in b if x ]
			ports = []
			for i in b:
				if i.split()[0][0].isdigit():
					ports.append("ethernet "+i.split()[0])
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPorts(): %s" % str(error)}
	
	def getPortsAndDesc(self, interface=''):
		"""
		Return a dictionary with port description. If you not specify an interface, 
		this method return all ports descriptions for each port in the switch.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			if interface:
				b = self.run("sh interfaces " + interface)
			else:
				b = self.run("sh interfaces")
			#
			ports = OrderedDict({})
			b = [ x for x in b if x ]
			ETHE = ""
			for i in b:
				if i[0] != " " and "Ethernet" in i: 
					ETHE = i.split()[0].split("net")[1]
				if "Port name is " in i:
					ports["ethernet " + ETHE] = i.split("Port name is ")[-1].strip()
			#
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsAndDesc(): %s" % str(error)}
	
	def getPortsDetail(self, interface=''):
		"""
		Return a dictionary with all port details. If you not specify an interface, 
		this method return all ports details for each port in the switch.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			if interface:
				b = self.run("sh interfaces " + interface)
			else:
				b = self.run("sh interfaces", timeout=60)
			#
			ports = OrderedDict({})
			ETHE = ""
			b = [ x for x in b if x ]
			for i in b:
				if i[0] != " " and "Ethernet" in i: 
					ETHE = i.split()[0].split("net")[1]
					ports["ethernet " + ETHE] = ""
				#
				ports["ethernet " + ETHE] += i + "\n"
			#
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsDetail(): %s" % str(error)}
	
	def getMacAddress(self, macAddress='', filters=''):
		"""
		Return a dictionary with port and vlan details, for each mac address.
		If you not specify mac address, this method return all mac address in
		the switch. You could specify filters.

		Params:
			macAddress = string
			filters = string
		"""
		try:
			self._isClosed()
			if macAddress and filters:
				_mval = ".".join(macAddress.replace(":","")[s:s+4] for s in range(0, len(macAddress.replace(":","")), 4)).lower()
				b = self.run("sh mac-address " + _mval + " | " + filters)
			elif filters:
				b = self.run("sh mac-address all | " + filters)
			elif macAddress:
				_mval = ".".join(macAddress.replace(":","")[s:s+4] for s in range(0, len(macAddress.replace(":","")), 4)).lower()
				b = self.run("sh mac-address " + _mval)	
			else:
				b = self.run("sh mac-address all")
			#
			mac = {}
			b = [ x for x in b if x ]
			for i in b:
				if len(i.split()[0]) == 14:	
					mvalue = ":".join(i.split()[0].replace(".","")[s:s+2] for s in range(0, len(i.split()[0].replace(".","")), 2)).upper()
					mac[mvalue] = { "vlan": i.split()[-1], 
									"port": "ethernet "+i.split()[1] }
			#
			return mac
		except Exception, error:
			return { "error": "Error in getMacAddress(): %s" % str(error) }
	
	def getArp(self, address='', filters=''):
		"""
		Return a dictionary with port, mac and status details, for each ip address in the ARP table.
		If you not specify any address, this method return all address in the switch. 
		You could specify filters.

		Params:
			address = string
			filters = string
		"""
		try:
			self._isClosed()
			if address.find(":") != -1 and filters:
				address = ".".join(address.replace(":","")[s:s+4] for s in range(0, len(address.replace(":","")), 4)).lower()
				b = self.run("sh arp mac-address " + address + " | " + filters)
			elif address and filters:
				b = self.run("sh arp " + address + " | " + filters)	
			elif address.find(":") != -1:
				address = ".".join(address.replace(":","")[s:s+4] for s in range(0, len(address.replace(":","")), 4)).lower()
				b = self.run("sh arp mac-address " + address)
			elif address:
				b = self.run("sh arp " + address)
			elif filters:
				b = self.run("sh arp | " + filters)	
			else:
				b = self.run("sh arp")
			#
			arp = OrderedDict({})
			b = [ x for x in b if x ]
			for i in b:
				if i.split()[0].isdigit():	
					if i.split()[2].find(":") != -1 or i.split()[2].find(".") != -1:
						mvalue = ":".join(i.split()[2].replace(".","")[s:s+2] for s in range(0, len(i.split()[2].replace(".","")), 2)).upper()
					else:
						mvalue = i.split()[2]
					arp[i.split()[1]] = { "port": ("ethernet "+i.split()[5].strip() if i.split()[6].strip() == "Valid" else i.split()[5].strip()),
										  "mac": mvalue,
										  "status": i.split()[6].strip() }
			#
			return arp
		except Exception, error:
			return { "error": "Error in getArp(): %s" % str(error) }
	
	def setPortsDesc(self, port='', desc=''):
		"""
		Set port description. Return a dictionary with vlan and port description.

		Params:
			port = string
			desc = string
		"""
		try:
			ports = {}
			#
			self._isClosed()
			if not port:
				raise Exception("No port selected. Please, specify port!")
			#
			self.child.sendline("conf t")
			self.child.expect("#")
			self.child.sendline("interf " + port)
			self.child.expect("#")
			self.child.sendline("""port-name %s""" % desc)
			self.child.expect("#")
			self.child.sendline("end")
			self.child.expect(self.prompt + "#")
			self.child.sendline("write mem")
			self.child.expect(self.prompt + "#")
			#
			ports = self.getPortsAndDesc(port)
			#
			ports = self.getPortsAndDesc(port)
			for i in ports:
				ports[i]= (ports[i], "Done")
			#
			return ports
		except Exception, error:
			return {"error": "Error in setPortsDesc(): %s" % str(error)}

	def setPortVlan(self, port='', vlan={"id": 0, "tagged": True, "remove": []}, novsrp=True):
		"""
		Set port vlan. You can add or remove vlan from tagged ports, or you can
		change vlan on untagged ports. You can specify if the port would be vsrp
		member.

		Params:
			port = string
			vlan = dict({"id": int, "tagged": Boolean, "remove": list})
			novsrp = Boolean
		"""
		try:
			#
			__ret__ = []
			self._isClosed()
			if not port or not vlan['id']:
				raise Exception("No port or vlan provided. Please, specify all parameters!")
			ieeeVlan = range(1,4096) + [5000]
			if vlan['id'] not in ieeeVlan:
				raise Exception("Vlan ID incorrect!")
			#
			vsrp = {}
			if not novsrp:	vsrp = self.getVsrp()
			#
			if vlan['tagged']:
				self.child.sendline("conf t")
				self.child.expect("#")
				if vlan['remove']:
					for i in vlan['remove']:
						self.child.sendline("vlan " + str(i))
						self.child.expect("#")
						self.child.sendline("no tag " + port)
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				if vlan['id'] == 5000:
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					self.child.sendline("write mem")
					self.child.expect(self.prompt + "#")
					return __ret__
				#
				self.child.sendline("vlan " + str(vlan["id"]))
				self.child.expect("#")
				self.child.sendline("tag " + port)
				self.child.expect("#")
				__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				if not novsrp and vlan['id'] in vsrp.keys():
					self.child.sendline("vsrp vrid " + str(vsrp[vlan['id']]))
					self.child.expect("#")
					self.child.sendline("no include-ports " + port)
					self.child.expect("#")
					__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				self.child.sendline("end")
				self.child.expect(self.prompt + "#")
				self.child.sendline("write mem")
				self.child.expect(self.prompt + "#")
			else:
				vlanp = self.getPortVlan(port)[port]
				if not vlanp['tagged']:
					self.child.sendline("conf t")
					self.child.expect("#")
					#
					if vlanp['untagged'][0] != 1:
						self.child.sendline("vlan " + str(vlanp['untagged'][0]))
						self.child.expect("#")
						self.child.sendline("no unt " + port)
						self.child.expect("#")
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					#
					self.child.sendline("vlan " + str(vlan["id"]))
					self.child.expect("#")
					self.child.sendline("untag " + port)
					self.child.expect("#")
					__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					if not novsrp and vlan['id'] in vsrp.keys():
						self.child.sendline("vsrp vrid " + str(vsrp[vlan['id']]))
						self.child.expect("#")
						self.child.sendline("no include-ports " + port)
						self.child.expect("#")
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					self.child.sendline("write mem")
					self.child.expect(self.prompt + "#")
				else:
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					raise Exception("Port %s is not in access mode. Please contact your network administrator!" % port)
			#
			return __ret__
		except Exception, error:
			return {"error": "Error in setPortVlan(): %s" % str(error)}


#########################################
#
# Force10 Class
#
#########################################

class Force10():
	"""
	Class for Force10 switches management.

	"""

	def __init__(self, name=None):
		self.child = pexpect
		self.prompt = None
		self.name = name
		self.device = None

	def getName(self):
		"""
		This method return the instance name
		"""
		return self.name

	def setName(self, name):
		"""
		This method set the instance name.
		"""
		self.name = name

	def connect(self, username, password, secret, device, prompt):	
		"""
		This method is used to start a new switch connection. 
		You must specify: device, username, password, secret and prompt

		Params:
			username = string
			password = string
			secret = string
			device = string
			prompt = string
		"""
		try:
			ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
			self.prompt = prompt
			self.device = device
			self.child = pexpect.spawn(ssh + username + "@" + device)
			self.child.expect(username + "@" + device + "\'s password: ", timeout=8)
			self.child.sendline(password)
			self.child.expect(prompt + ">", timeout=8)
			self.child.sendline("enable")
			self.child.expect("Password: ", timeout=8)
			self.child.sendline(secret)
			self.child.expect(prompt + "#", timeout=8)
			self.child.sendline("ter len 0")
			self.child.expect(prompt + "#", timeout=8)
			#
		except Exception, e:
			return "Error in connect(): %s" % str(e)
	
	def run(self, command, timeout=15):
		"""
		Return a list with the specified command output. Yuo can specify
		a custom timeout. When timeout is different to 15 seconds, the method 
		wait a switch response until timeout is reached.  

		Params:
			command = string
			timeout = integer
		"""
		try:
			self._isClosed()
			self.child.sendline(command)
			self.child.expect(self.prompt + "#", timeout=timeout)
			to_log = str(self.child.before)
			return to_log.replace("\r", "").split("\n")[1:-1]
		except Exception, e:
			return {"error": "Error in run(): %s" % str(e)}
	
	def close(self):
		"""
		This method is used to close the switch connection.
		"""
		try:
			self._isClosed()
			self.child.sendline("exit")
			self.child.close()
		except Exception, e:
			return ["Error in close(): %s" % str(e)]

	def _isClosed(self):
		try:
			if self.child.closed:
				return Exception("Session closed. Please, reconnect!")
		except Exception, e:
			return "Error: " + str(e)

	def status(self):
		"""
		This method return the connection status.
		"""
		try:
			if self.child.closed:
				return "Session closed. Please, reconnect!"
			else:
				return "Connected to " + str(self.device)
		except Exception, e:
			return "Error: " + str(e)
	
	def __vlanPortDetail(self, vlan):
		"""
		Return a dictionary with all ports vlan detail. Yuo can specify
		wich vlan you want check.

		Params:
			vlan = string
		"""
		try:
			ports = OrderedDict({})
			#
			for i in vlan:
				unt = []
				tag = []
				u = False
				t = False	
				for s in vlan[i]:
					if s and s[0] == "U":	
						u = True
						t = False
					elif s and s[0] == "T":	
						t = True
						u = False
					#
					if u:
						_id = s.replace(" ", "").replace("U","").split(",")
						for l in _id:
							if l and len(l.split("-")) == 1:
								unt.append(int(l))
							elif l:
								ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
								for el in ran:
									unt.append(el)
					if t:
						_id = s.replace(" ", "").replace("T","").split(",")
						for l in _id:
							if l and len(l.split("-")) == 1:
								tag.append(int(l))
							elif l:
								ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
								for el in ran:
									tag.append(el)
				if tag and not unt:	unt.append(1)
				ports[i] = {"untagged": unt, "tagged": tag}
			return ports
		except Exception, error:
			return "Error in __vlanPortDetail(): %s" % str(error)
	
	def getPortVlan(self, filters=''):
		"""
		Return a dictionary with port vlan details. If you not specify an interface, 
		this method return all port vlan details for each port in the switch.

		Params:
			filters = string (Could be interface or filter)
		"""
		try:
			self._isClosed()
			if filters:
				b = self.run("sh ip interf brief " + filters)	
			else:
				b = self.run("sh ip interf brief")
			#
			vlan = OrderedDict({})
			#
			for i in b:
				if i.split("  ")[0].strip() != "Interface":
					a =  self.run('sh int switch '+i.split("  ")[0])
					take = False
					_add = []
					for z in a:
						if take:	
							if z and z.find("Native") == -1:
								_add.append(z)
						if z.find("Q       Vlans") != -1:	take = True
					#
					vlan[i.split("  ")[0]] = _add
			#
			ports = self.__vlanPortDetail(vlan)
			return ports
		except Exception, error:
			return {"error": "Error in getPortVlan(): %s" % str(error)}
	
	def getPortsAndDesc(self, interface='', filters=''):
		"""
		Return a dictionary with port description. If you not specify an interface, 
		this method return all ports descriptions for each port in the switch.
		You can specify filters.

		Params:
			interface = string
			filters = string
		"""
		try:
			self._isClosed()
			ports = OrderedDict({})
			#
			if filters:
				b = self.run("sh interf desc | " + filters)
				for i in b:
					if i.split("  ")[0].strip() != "Interface":
						ports[i.split("  ")[0]] = i.split("        ")[-1].strip()	
			elif interface:
				b = self.run("sh interf " + interface)
				for i in b:
					if "Description: " in i:
						ports[interface] = i.split("Description: ")[1]	
			else:
				b = self.run("sh interf desc")
				for i in b:
					if i.split("  ")[0].strip() != "Interface":
						ports[i.split("  ")[0]] = i.split("        ")[-1].strip()	
			#
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsAndDesc(): %s" % str(error)}
	
	def getPortsDetail(self, interface='', filters=''):
		"""
		Return a dictionary with all port details. If you not specify an interface, 
		this method return all ports details for each port in the switch.

		Params:
			interface = string
			filters = string
		"""
		try:
			self._isClosed()
			ports = OrderedDict({})
			#
			if filters:
				b = self.run("sh interf | " + filters)	
			elif interface:
				b = self.run("sh interf " + interface)
			else:
				b = self.run("sh interf", timeout=80)
			#
			b = [x for x in b if x]
			PORT = ""
			for i in b:
				if "Ethernet" in i.split()[0].strip() or "Port-channel" in i.split()[0].strip():
					PORT = i.split()[0].strip()+i.split()[1].strip()
					ports[PORT] = i + "\n"
				else:
					ports[PORT] += i + "\n"
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsDetail(): %s" % str(error)}
	
	def getMacAddress(self, macAddress=''):
		"""
		Return a dictionary with port and vlan details, for each mac address.
		If you not specify mac address, this method return all mac address in
		the switch.

		Params:
			macAddress = string
		"""
		try:
			self._isClosed()
			if macAddress:
				b = self.run("sh mac-address-table address " + macAddress)	
				comp = -1
			else:
				b = self.run("sh mac-address-table")
				comp = -2
			#
			mac = {}
			take = False
			for i in b:
				if take and i.find("\t") != -1:	
					mac[i.split("\t")[1].strip().upper()] = { "vlan": i.split("\t")[0].strip(), "port": i.split("\t")[comp].strip() }
				if i.find("VlanId     ") != -1:	take = True	
			#
			return mac
		except Exception, error:
			return { "error": "Error in getMacAddress(): %s" % str(error) }
	
	def getPorts(self, filters=''):
		"""
		Return a list with all interfaces. You could specify filters.

		Params:
			filters = string
		"""
		try:
			self._isClosed()
			if filters:
				b = self.run("sh ip interf brief | " + filters)	
			else:
				b = self.run("sh ip interf brief")
			#
			ports = []
			for i in b:
				if i.split("  ")[0] != "Interface":
					ports.append(i.split("  ")[0])
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPorts(): %s" % str(error)}
	
	def setPortsDesc(self, port='', desc=''):
		"""
		Set port description. Return a dictionary with vlan and port description.

		Params:
			port = string
			desc = string
		"""
		try:
			ports = {}
			#
			self._isClosed()
			if not port:
				raise Exception("No port selected. Please, specify port!")
			#
			self.child.sendline("conf t")
			self.child.expect("#")
			self.child.sendline("interf " + port)
			self.child.expect("#")
			self.child.sendline("description " + desc)
			self.child.expect("#")
			self.child.sendline("end")
			self.child.expect(self.prompt + "#")
			self.child.sendline("write mem")
			self.child.expect(self.prompt + "#")
			#
			sleep(0.1)
			ports = self.getPortsAndDesc(port)
			for i in ports:
				ports[i]= (ports[i], "Done")
			#
			return ports
		except Exception, error:
			return {"error": "Error in setPortsDesc(): %s" % str(error)}

	def setPortVlan(self, port='', vlan={"id": 0, "tagged": True, "remove": []}):
		"""
		Set port vlan. You can add or remove vlan from tagged ports, or you can
		change vlan on untagged ports.

		Params:
			port = string
			vlan = dict({"id": int, "tagged": Boolean, "remove": list})
		"""
		try:
			#
			__ret__ = []
			self._isClosed()
			if not port or not vlan['id']:
				raise Exception("No port or vlan provided. Please, specify all parameters!")
			ieeeVlan = range(1,4096) + [5000]
			if vlan['id'] not in ieeeVlan:
				raise Exception("Vlan ID incorrect!")
			#
			#
			if vlan['tagged']:
				self.child.sendline("conf t")
				self.child.expect("#")
				if vlan['remove']:
					for i in vlan['remove']:
						self.child.sendline("int vlan " + str(i))
						self.child.expect("#")
						self.child.sendline("no tag " + port)
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				if vlan['id'] == 5000:
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					self.child.sendline("write mem")
					self.child.expect(self.prompt + "#")
					return __ret__
				#
				self.child.sendline("int vlan " + str(vlan["id"]))
				self.child.expect("#")
				self.child.sendline("tag " + port)
				self.child.expect("#")
				__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				self.child.sendline("end")
				self.child.expect(self.prompt + "#")
				self.child.sendline("write mem")
				self.child.expect(self.prompt + "#")
			else:
				#
				vlanp = self.getPortVlan(port)[port]
				if not vlanp['tagged']:
					self.child.sendline("conf t")
					self.child.expect("#")
					if vlanp['untagged'][0] != 1:
						self.child.sendline("int vlan " + str(vlanp['untagged'][0]))
						self.child.expect("#")
						self.child.sendline("no unt " + port)
						self.child.expect("#")
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					#
					self.child.sendline("int vlan " + str(vlan["id"]))
					self.child.expect("#")
					self.child.sendline("untag " + port)
					self.child.expect("#")
					__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					#
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					self.child.sendline("write mem")
					self.child.expect(self.prompt + "#")
				else:
					raise Exception("Port %s is not in access mode. Please contact your network administrator!" % port)
			#
			return __ret__
		except Exception, error:
			return {"error": "Error in setPortVlan(): %s" % str(error)}



#########################################
#
# Cisco Class
#
#########################################

class Cisco():
	"""
	Class for Cisco switches management.

	"""

	def __init__(self, name=None):
		self.child = pexpect
		self.prompt = None
		self.name = name
		self.device = None

	def getName(self):
		"""
		This method return the instance name
		"""
		return self.name

	def setName(self, name):
		"""
		This method set the instance name.
		"""
		self.name = name
	
	def connect(self, username, password, secret, device, prompt):	
		"""
		This method is used to start a new switch connection. 
		You must specify: device, username, password, secret and prompt

		Params:
			username = string
			password = string
			secret = string
			device = string
			prompt = string
		"""
		try:
			ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
			self.prompt = prompt
			self.device = device
			self.child = pexpect.spawn(ssh + username + "@" + device)
			try:	
				self.child.expect(username + "@" + device + "\'s password:", timeout=3)
				self.child.sendline(password)
			except:
				self.child.expect("Password: ", timeout=3)
				self.child.sendline(password)
			self.child.expect(prompt + ">", timeout=8)
			self.child.sendline("enable")
			self.child.expect("Password: ", timeout=8)
			self.child.sendline(secret)
			self.child.expect(prompt + "#", timeout=8)
			self.child.sendline("ter len 0")
			self.child.expect(prompt + "#", timeout=8)
			#
		except Exception, e:
			return "Error in connect(): %s" % str(e)
	
	def run(self, command, timeout=15):
		"""
		Return a list with the specified command output. Yuo can specify
		a custom timeout. When timeout is different to 15 seconds, the method 
		wait a switch response until timeout is reached.  

		Params:
			command = string
			timeout = integer
		"""
		try:
			self._isClosed()
			self.child.sendline(command)
			self.child.expect(self.prompt + "#", timeout=timeout)
			to_log = str(self.child.before)
			return to_log.replace("\r", "").split("\n")[1:-1]
		except Exception, e:
			return {"error": "Error in run(): %s" % str(e)}
	
	def close(self):
		"""
		This method is used to close the switch connection.
		"""
		try:
			self._isClosed()
			self.child.sendline("exit")
			self.child.close()
		except Exception, e:
			return ["Error in close(): %s" % str(e)]

	def _isClosed(self):
		try:
			if self.child.closed:
				return Exception("Session closed. Please, reconnect!")
		except Exception, e:
			return "Error: " + str(e)

	def status(self):
		"""
		This method return the connection status.
		"""
		try:
			if self.child.closed:
				return "Session closed. Please, reconnect!"
			else:
				return "Connected to " + str(self.device)
		except Exception, e:
			return "Error: " + str(e)
	
	def _vlanPortDetail(self, vlan):
		"""
		Return a dictionary with all ports vlan detail. Yuo can specify
		wich vlan you want check.

		Params:
			vlan = string
		"""
		try:
			ports = OrderedDict({})
			#
			for i in vlan:
				unt = []
				tag = []
				u = False
				t = False	
				for s in vlan[i]:
					if s and s[0] == "U":	
						u = True
						t = False
					elif s and s[0] == "T":	
						t = True
						u = False
					#
					if u:
						_id = s.replace("U","").split(",")
						for l in _id:
							if l and len(l.split("-")) == 1:
								unt.append(int(l))
							elif l:
								ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
								for el in ran:
									unt.append(el)
					if t:
						_id = s.replace("T","").split(",")
						for l in _id:
							if l and len(l.split("-")) == 1:
								tag.append(int(l))
							elif l:
								ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
								for el in ran:
									tag.append(el)
				ports[i] = {"untagged": unt, "tagged": tag}
			return ports
		except Exception, error:
			return "Error in vlanPortDetail(): %s" % str(error)
	
	def getPortVlan(self, filters=''):
		"""
		Return a dictionary with port vlan details. If you not specify an interface, 
		this method return all port vlan details for each port in the switch.

		Params:
			filters = string (Could be interface or filter)
		"""
		try:
			self._isClosed()
			if filters:
				b = self.run("sh ip interf brief " + filters)	
			else:
				b = self.run("sh ip interf brief")
			#
			vlan = OrderedDict({})
			b = [ x for x in b if x ]
			#
			for i in b:
				if (i) and (i.split()[0] != "Interface") and ("Vlan" not in i.split()[0]) and ("Mana" not in i.split()[0]):
					a =  self.run('sh int '+i.split()[0]+" switch")
					_add = []
					trunk = False
					access = False
					for z in a:
						if z and "Administrative Mode: trunk" in z:
							trunk = True
						elif z and "Administrative Mode: static access" in z:
							access = True
						if z and trunk:
							if "Trunking Native Mode VLAN:" in z:
								_add.append("U"+z.split()[4])
							elif "Trunking VLANs Enabled: " in z:
								_add.append("T"+z.split()[3])
						if z and access:
							if "Access Mode VLAN:" in z:
								_add.append("U"+z.split()[3])
					#
					vlan[i.split()[0]] = _add
			#
			ports = self._vlanPortDetail(vlan)
			return ports
		except Exception, error:
			return {"error": "Error in getPortVlan(): %s" % str(error)}
	
	def getPorts(self, filters=''):
		"""
		Return a list with all interfaces. You could specify filters.

		Params:
			filters = string
		"""
		try:
			self._isClosed()
			if filters:
				b = self.run("sh ip interf brief | " + filters)	
			else:
				b = self.run("sh ip interf brief")
			#
			b = [ x for x in b if x ]
			ports = []
			for i in b:
				if i and i.split()[0] != "Interface" and "Vlan" not in i.split()[0] and "Mana" not in i.split()[0]:
					ports.append(i.split()[0])
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortVlan(): %s" % str(error)}
	
	def getPortsAndDesc(self, filters=''):
		"""
		Return a dictionary with port description. If you not specify an interface, 
		this method return all ports descriptions for each port in the switch.
		You can specify filters.

		Params:
			filters = string (Could be interface or filter)
		"""
		try:
			self._isClosed()
			ports = OrderedDict({})
			#
			if filters:
				b = self.run("sh interf " + filters)
				b = [ x for x in b if x ]
				for i in b:
					if i and "Description: " in i:
						ports[filters] = i.split("Description: ")[1]	
			else:
				b = self.run("sh interf desc")
				b = [ x for x in b if x ]
				for i in b:
					if i and "Interface" not in i.split()[0] and "Vl" not in i.split()[0]:
						ports[i.split()[0]] = i.split("  ")[-1].strip()
			#
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsAndDesc(): %s" % str(error)}
	
	def getPortsDetail(self, interface='', filters=''):
		"""
		Return a dictionary with all port details. If you not specify an interface, 
		this method return all ports details for each port in the switch.

		Params:
			interface = string
			filters = string
		"""
		try:
			self._isClosed()
			ports = OrderedDict({})
			#
			if filters:
				b = self.run("sh interf | " + filters)	
			elif interface:
				b = self.run("sh interf " + interface)
			else:
				b = self.run("sh interf", timeout=60)
			#
			b = [x for x in b if x]
			PORT = ""
			log = False
			for i in b:
				if "Ethernet" in i.split()[0].strip() or "Port-channel" in i.split()[0].strip():
					PORT = i.split()[0].strip()
					ports[PORT] = i + "\n"
					log = True
				elif log:
					ports[PORT] += i + "\n"
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsDetail(): %s" % str(error)}
	
	def getMacAddress(self, macAddress='', filters=''):
		"""
		Return a dictionary with port and vlan details, for each mac address.
		If you not specify mac address, this method return all mac address in
		the switch. You could specify filters.

		Params:
			macAddress = string
			filters = string
		"""
		try:
			self._isClosed()
			if macAddress and filters:
				_mval = ".".join(macAddress.replace(":","")[s:s+4] for s in range(0, len(macAddress.replace(":","")), 4)).lower()
				b = self.run("show mac address-table address " + _mval + " | " + filters)
			elif filters:
				b = self.run("show mac address-table | " + filters)
			elif macAddress:
				_mval = ".".join(macAddress.replace(":","")[s:s+4] for s in range(0, len(macAddress.replace(":","")), 4)).lower()
				b = self.run("show mac address-table address " + _mval)	
			else:
				b = self.run("show mac address-table")
			#
			mac = {}
			b = [ x for x in b if x ]
			for i in b:
				if i.split()[0].isdigit():	
					mvalue = ":".join(i.split()[1].replace(".","")[s:s+2] for s in range(0, len(i.split()[1].replace(".","")), 2)).upper()
					mac[mvalue] = { "vlan": i.split()[0], 
									"port": i.split()[-1] }
			#
			return mac
		except Exception, error:
			return { "error": "Error in getMacAddress(): %s" % str(error) }
	
	def getArp(self, filters=''):
		"""
		Return a dictionary with port, mac and status details, for each ip address in the ARP table.
		If you not specify any address, this method return all address in the switch. 
		You could specify filters.

		Params:
			filters = string (filters or address)
		"""
		try:
			self._isClosed()
			if filters:
				b = self.run("sh arp | " + filters)	
			else:
				b = self.run("sh arp")
			#
			arp = OrderedDict({})
			b = [ x for x in b if x ]
			for i in b:
				if len(i.split()[3]) == 14 :	
					mvalue = ":".join(i.split()[3].replace(".","")[s:s+2] for s in range(0, len(i.split()[3].replace(".","")), 2)).upper()
					mac = self.getMacAddress(mvalue)
					arp[i.split()[1]] = { "port": mac[mvalue]['port'],
										  "mac": mvalue }
			#
			return arp
		except Exception, error:
			return { "error": "Error in getArp(): %s" % str(error) }
	
	def setPortsDesc(self, port='', desc=''):
		"""
		Set port description. Return a dictionary with vlan and port description.

		Params:
			port = string
			desc = string
		"""
		try:
			self._isClosed()
			if not port:
				raise Exception("No port selected. Please, specify port!")
			#
			self.child.sendline("conf t")
			self.child.expect("#")
			self.child.sendline("interf " + port)
			self.child.expect("#")
			self.child.sendline("description " + desc)
			self.child.expect("#")
			self.child.sendline("end")
			self.child.expect(self.prompt + "#")
			self.child.sendline("write mem")
			self.child.expect(self.prompt + "#")
			#
			sleep(0.1)
			ports = self.getPortsAndDesc(port)
			for i in ports:
				ports[i]= (ports[i], "Done")
			#
			return ports
		except Exception, error:
			return {"error": "Error in setPortsDesc(): %s" % str(error)}

	def setPortVlan(self, port='', vlan={"id": 0, "tagged": True, "remove": []}):
		"""
		Set port vlan. You can add or remove vlan from tagged ports, or you can
		change vlan on untagged ports.

		Params:
			port = string
			vlan = dict({"id": int, "tagged": Boolean, "remove": list})
		"""
		try:
			#
			__ret__ = []
			self._isClosed()
			if not port or not vlan['id']:
				raise Exception("No port or vlan provided. Please, specify all parameters!")
			ieeeVlan = range(1,4096) + [5000]
			if vlan['id'] not in ieeeVlan:
				raise Exception("Vlan ID incorrect!")
			#
			#
			vlanp = self.getPortVlan(port)[port]
			#
			if vlan['tagged']:
				if not vlanp['tagged']:
					raise Exception("Port %s is not in trunk mode. Please contact your network administrator!" % port)
				self.child.sendline("conf t")
				self.child.expect("#")
				if vlan['remove']:
					self.child.sendline("int vlan " + port)
					for i in vlan['remove']:
						self.child.sendline("switchport trunk allowed vlan remove " + str(i))
						self.child.expect("#")
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				if vlan['id'] == 5000:
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					self.child.sendline("write mem")
					self.child.expect(self.prompt + "#")
					return __ret__
				#
				self.child.sendline("int " + port)
				self.child.expect("#")
				self.child.sendline("switchport trunk allowed vlan add " + str(vlan['id']))
				self.child.expect("#")
				__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				self.child.sendline("end")
				self.child.expect(self.prompt + "#")
				self.child.sendline("write mem")
				self.child.expect(self.prompt + "#")
			else:
				if not vlanp['tagged']:
					self.child.sendline("conf t")
					self.child.expect("#")
					#
					self.child.sendline("int " + port)
					self.child.expect("#")
					self.child.sendline("switchport access vlan " + str(vlan["id"]))
					self.child.expect("#")
					__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					#
					self.child.sendline("end")
					self.child.expect(self.prompt + "#")
					self.child.sendline("write mem")
					self.child.expect(self.prompt + "#")
				else:
					raise Exception("Port %s is not in access mode. Please contact your network administrator!" % port)
			#
			return __ret__
		except Exception, error:
			return {"error": "Error in setPortVlan(): %s" % str(error)}


#########################################
#
# Extreme Class
#
#########################################

class Extreme():
	"""
	Class for Extreme switches management.

	"""

	def __init__(self, name=None):
		self.child = pexpect
		self.prompt = None
		self.name = name
		self.device = None

	def getName(self):
		"""
		This method return the instance name
		"""
		return self.name

	def setName(self, name):
		"""
		This method set the instance name.
		"""
		self.name = name
	
	def connect(self, username, password, secret, device, prompt):	
		"""
		This method is used to start a new switch connection. 
		You must specify: device, username, password, secret and prompt

		Params:
			username = string
			password = string
			secret = string
			device = string
			prompt = string
		"""
		try:
			ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
			self.prompt = prompt
			self.device = device
			self.child = pexpect.spawn(ssh + username + "@" + device)
			self.child.expect("Enter password for " + username + ":")
			self.child.sendline(password)
			self.child.expect(prompt + ".\d #")
			self.child.sendline("disable clipaging")
			self.child.expect(prompt + ".\d #")
			#
		except Exception, e:
			return "Error in connect(): %s" % str(e)
	
	def run(self, command, timeout=240):
		"""
		Return a list with the specified command output. Yuo can specify
		a custom timeout. When timeout is different to 240 seconds, the method 
		wait a switch response until timeout is reached.  

		Params:
			command = string
			timeout = integer
		"""
		try:
			self._isClosed()
			self.child.sendline(command)
			self.child.expect(self.prompt + ".\d #", timeout=timeout)
			to_log = str(self.child.before)
			return to_log.replace("\r", "").split("\n")[1:-1]
		except Exception, e:
			return {"error": "Error in run(): %s" % str(e)}
	
	def close(self):
		"""
		This method is used to close the switch connection.
		"""
		try:
			self._isClosed()
			self.child.sendline("exit")
			self.child.close()
		except Exception, e:
			return ["Error in close(): %s" % str(e)]

	def _isClosed(self):
		try:
			if self.child.closed:
				return Exception("Session closed. Please, reconnect!")
		except Exception, e:
			return "Error: " + str(e)

	def status(self):
		"""
		This method return the connection status.
		"""
		try:
			if self.child.closed:
				return "Session closed. Please, reconnect!"
			else:
				return "Connected to " + str(self.device)
		except Exception, e:
			return "Error: " + str(e)
		
	def getPortVlan(self, interface=''):
		"""
		Return a dictionary with port vlan details. If you not specify an interface, 
		this method return all port vlan details for each port in the switch.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			if interface:
				b = self.run("show ports " + interface + " information detail")	
			else:
				b = self.run("show ports information detail")
			#
			b = [ x for x in b if x ]
			d = []
			for i in b:
				z = i.split("\t")
				z = [x for x in z if x]
				d.append(z)
			#
			dic = OrderedDict({})
			k = ""
			app = []
			lo = False
			#
			for i in d:
				if i[0] == 'Port:':
					k = i[1].split("(")[0]
					lo = True
				elif i[0] == "STP cfg:":
					lo = False
					dic[k] = app
					app = []
				if lo:
					if i[0] != 'Port:': 
						app.append(i)
			#
			_dic = OrderedDict({})
			#
			for z in dic:
				lo = False
				_a = {}
				tag = []
				unt = []
				for i in dic[z]:
					if lo and i[0]:
						if "Internal" in i[0].split(",")[1]:	unt.append(int(i[0].split(",")[1].split()[-1]))
						elif "802.1Q" in i[0].split(",")[1]:	tag.append(int(i[0].split(",")[1].split()[-1]))
					if i[0] == "VLAN cfg: ":
						lo = True
				_a = {'tagged': sorted(tag), 'untagged': sorted(unt)}
				_dic[z] = _a
			#
			return _dic
			#
		except Exception, error:
			return {"error": "Error in getPortVlan(): %s" % str(error)}

	def getVlans(self):
		"""
		Return a dictionary with all vlan details.

		"""
		try:
			self._isClosed()
			#
			b = self.run("show vlan")	
			b = { int(x.split()[1]): x.split()[0] for x in b if len(x.split()) == 7 and x.split()[1].isdigit() }
			#
			return b
			#
		except Exception, error:
			return {"error": "Error in getVlans(): %s" % str(error)}
	
	def getPorts(self, interface=''):
		"""
		Return a list with all interfaces. You could specify interface.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			if interface:
				b = self.run("show ports " + interface + " no-refresh")	
			else:
				b = self.run("show ports no-refresh")
			#
			b = [ x for x in b if x ]
			ports = []
			for i in b:
				if i.split()[0][0].isdigit():
					ports.append(i.split()[0])
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPort(): %s" % str(error)}
	
	def getPortsAndDesc(self, interface=''):
		"""
		Return a dictionary with port description. If you not specify an interface, 
		this method return all ports descriptions for each port in the switch.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			if interface:
				b = self.run("show ports " + interface + " information detail")	
			else:
				b = self.run("show ports information detail")
			#
			b = [ x for x in b if x ]
			d = []
			for i in b:
				z = i.split("\t")
				z = [x for x in z if x]
				d.append(z)
			#
			dic = OrderedDict({})
			k = ""
			app = []
			#
			for i in d:
				if i[0] == 'Port:':
					if len(i[1]) > 4:
						k = i[1].split("(")[0]
						desc = i[1].split("(")[1].replace("):","")
						dic[k] = desc
					else:
						k = i[1]
						desc = "No description found"
						dic[k] = desc
			#
			return dic
		except Exception, error:
			return {"error": "Error in getPortsAndDesc(): %s" % str(error)}
	
	def getPortsDetail(self, interface=''):
		"""
		Return a dictionary with all port details. If you not specify an interface, 
		this method return all ports details for each port in the switch.

		Params:
			interface = string
		"""
		try:
			self._isClosed()
			if interface:
				b = self.run("show ports " + interface + " statistics no-refresh")
				c = self.run("show ports " + interface + " congestion no-refresh")	
				d = self.run("show ports " + interface + " txerrors no-refresh")	
				e = self.run("show ports " + interface + " rxerrors no-refresh")		
			else:
				b = self.run("show ports statistics no-refresh")
				c = self.run("show ports congestion no-refresh")	
				d = self.run("show ports txerrors no-refresh")	
				e = self.run("show ports rxerrors no-refresh")	
			#
			b = [ x for x in b if x ]
			c = [ x for x in c if x ]
			d = [ x for x in d if x ]
			e = [ x for x in e if x ]
			dic = OrderedDict({})
			dic['statistics'] = b
			dic['congestion'] = c
			dic['txerrors'] = d
			dic['rxerrors'] = e
			#
			return dic
		except Exception, error:
			return {"error": "Error in getPortsDetail(): %s" % str(error)}
	
	def getMacAddress(self, macAddress=''):
		"""
		Return a dictionary with port and vlan details, for each mac address.
		If you not specify mac address, this method return all mac address in
		the switch.

		Params:
			macAddress = string
		"""
		try:
			self._isClosed()
			if macAddress:
				b = self.run("sh fdb " + macAddress.lower())
			else:
				b = self.run("sh fdb")
			#
			mac = {}
			b = [ x for x in b if x ]
			for i in b:
				if len(i.split()[0]) == 17:	
					mvalue = i.split()[0].upper()
					mac[mvalue] = { "vlan": str(int(i.split()[1].split("(")[1].replace(")",""))), 
									"port": i.split()[-1] }
			#
			return mac
		except Exception, error:
			return { "error": "Error in getMacAddress(): %s" % str(error) }
	
	def setPortsDesc(self, port='', desc=''):
		"""
		Set port description. Return a dictionary with vlan and port description.

		Params:
			port = string
			desc = string
		"""
		try:
			self._isClosed()
			if not port:
				raise Exception("No port selected. Please, specify port!")
			#
			self.child.sendline("configure ports %s display-string %s" % (port, desc))
			self.child.expect("#")
			self.child.sendline("save")
			self.child.expect("(y/N)", timeout=10)
			self.child.sendline("Yes")
			self.child.expect("#")
			#
			sleep(0.1)
			ports = self.getPortsAndDesc(port)
			for i in ports:
				ports[i]= (ports[i], "Done")
			#
			return ports
		except Exception, error:
			return {"error": "Error in setPortsDesc(): %s" % str(error)}

	def setPortVlan(self, port='', vlan={"id": 0, "tagged": True, "remove": []}):
		"""
		Set port vlan. You can add or remove vlan from tagged ports, or you can
		change vlan on untagged ports.

		Params:
			port = string
			vlan = dict({"id": int, "tagged": Boolean, "remove": list})
		"""
		try:
			#
			__ret__ = []
			self._isClosed()
			if not port or not vlan['id']:
				raise Exception("No port or vlan provided. Please, specify all parameters!")
			ieeeVlan = range(1,4096) + [5000]
			if vlan['id'] not in ieeeVlan:
				raise Exception("Vlan ID incorrect!")
			#
			vland = self.getVlans()
			#
			if vlan['tagged']:
				if vlan['remove']:
					for i in vlan['remove']:
						self.child.sendline("configure vlan " + vland[i] + " delete port " + port)
						self.child.expect("#")
						__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				if vlan['id'] == 5000:
					self.child.sendline("save")
					self.child.expect("(y/N)", timeout=10)
					self.child.sendline("Yes")
					self.child.expect("#")
					return __ret__
				#
				self.child.sendline("configure vlan " + vland[vlan["id"]] + " add port " + port + " tag")
				self.child.expect("#")
				__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				self.child.sendline("save")
				self.child.expect("(y/N)", timeout=10)
				self.child.sendline("Yes")
				self.child.expect("#")
			else:
				#
				vlanp = self.getPortVlan(port)[port]
				if not vlanp['tagged']:
					self.child.sendline("configure vlan " + vlanp['untagged'][0] + " delete port " + port)
					self.child.expect("#")
					__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					#
					self.child.sendline("configure vlan " + vland[vlan["id"]] + " add port " + port + " untag")
					self.child.expect("#")
					__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
					#
					self.child.sendline("save")
					self.child.expect("(y/N)", timeout=10)
					self.child.sendline("Yes")
					self.child.expect("#")
				else:
					raise Exception("Port %s is not in access mode. Please contact your network administrator!" % port)
			#
			return __ret__
		except Exception, error:
			return {"error": "Error in setPortVlan(): %s" % str(error)}


#########################################
#
# H3C Class
#
#########################################

class H3C():
	"""
	Class for H3C switches management.

	"""

	def __init__(self, name=None):
		self.child = pexpect
		self.prompt = None
		self.name = name
		self.device = None

	def getName(self):
		"""
		This method return the instance name
		"""
		return self.name

	def setName(self, name):
		"""
		This method set the instance name.
		"""
		self.name = name
	
	def connect(self, username, password, secret, device, prompt):	
		"""
		This method is used to start a new switch connection. 
		You must specify: device, username, password, secret and prompt

		Params:
			username = string
			password = string
			secret = string
			device = string
			prompt = string
		"""
		try:
			ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
			self.prompt = prompt
			self.device = device
			self.child = pexpect.spawn(ssh + username + "@" + device)
			self.child.expect(username + "@" + device + "\'s password:", timeout=3)
			self.child.sendline(password)
			self.child.expect("<" + prompt + ">", timeout=8)
			self.child.sendline("super")
			self.child.expect("Password:", timeout=8)
			self.child.sendline(secret)
			self.child.expect("<" + prompt + ">", timeout=8)
			self.child.sendline("screen-length disable")
			self.child.expect("<" + prompt + ">", timeout=8)
			#
		except Exception, e:
			return "Error in connect(): %s" % str(e)
	
	def run(self, command, timeout=15):
		"""
		Return a list with the specified command output. Yuo can specify
		a custom timeout. When timeout is different to 15 seconds, the method 
		wait a switch response until timeout is reached.  

		Params:
			command = string
			timeout = integer
		"""
		try:
			self._isClosed()
			self.child.sendline(command)
			self.child.expect("<" + self.prompt + ">", timeout=timeout)
			to_log = str(self.child.before)
			return to_log.replace("\r", "").split("\n")[1:-1]
		except Exception, e:
			return {"error": "Error in run(): %s" % str(e)}
	
	def close(self):
		"""
		This method is used to close the switch connection.
		"""
		try:
			self._isClosed()
			self.child.sendline("exit")
			self.child.close()
		except Exception, e:
			return ["Error in close(): %s" % str(e)]

	def _isClosed(self):
		try:
			if self.child.closed:
				return Exception("Session closed. Please, reconnect!")
		except Exception, e:
			return "Error: " + str(e)

	def status(self):
		"""
		This method return the connection status.
		"""
		try:
			if self.child.closed:
				return "Session closed. Please, reconnect!"
			else:
				return "Connected to " + str(self.device)
		except Exception, e:
			return "Error: " + str(e)
	
	def _vlanPortDetail(self, vlan):
		"""
		Return a dictionary with all ports vlan detail. Yuo can specify
		wich vlan you want check.

		Params:
			vlan = string
		"""
		try:
			ports = OrderedDict({})
			#
			for i in vlan:
				unt = []
				tag = []
				u = False
				t = False	
				for s in vlan[i]:
					if s and s[0] == "U":	
						u = True
						t = False
					elif s and s[0] == "T":	
						t = True
						u = False
					#
					if u:
						_id = s.replace("U","").split(",")
						for l in _id:
							if l and len(l.split("-")) == 1:
								unt.append(int(l))
							elif l:
								ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
								for el in ran:
									unt.append(el)
					if t:
						_id = s.replace("T","").split(",")
						for l in _id:
							if l and len(l.split("-")) == 1:
								tag.append(int(l))
							elif l:
								ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
								for el in ran:
									tag.append(el)
				tag = [ x for x in tag if x not in unt ]
				if tag and not unt:
					unt.append(1)
				ports[i] = {"untagged": unt, "tagged": tag}
			return ports
		except Exception, error:
			return "Error in vlanPortDetail(): %s" % str(error)
	
	def getPortVlan(self, interface=''):
		"""
		Return a dictionary with port vlan details. If you not specify an interface, 
		this method return all port vlan details for each port in the switch.

		Params:
			interface = string (Could be interface or filter)
		"""
		try:
			self._isClosed()
			if interface:
				b = self.getPortsDetail(interface)
			else:
				b = self.getPortsDetail()
			#
			vlan = OrderedDict({})

			for i in b:
				_add = []
				trunk = False
				access = False
				a = b[i].split("\n")
				for z in a:
					if z and "PVID" in z:
						_add.append("U"+z.split()[1])
					if z and "Port link-type: trunk" in z:
						trunk = True
					elif z and "Port link-type: access" in z:
						access = True
					if z and trunk:
						if "VLAN passing  :" in z:
							_add.append("T"+z.split(":")[1].strip().replace("(default vlan)","").replace(" ",""))
				#
				vlan[i] = _add
			#
			ports = self._vlanPortDetail(vlan)
			return ports
		except Exception, error:
			return {"error": "Error in getPortVlan(): %s" % str(error)}
	
	def getPorts(self):
		"""
		Return a list with all interfaces. 
		"""
		try:
			self._isClosed()	
			b = self.run("display interf | in current")
			b = [ x.split()[0] for x in b if ("Ethernet" in x or "Aggre" in x) and "M-Giga" not in x ]
			ports = b
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPorts(): %s" % str(error)}
	
	def getPortsAndDesc(self, interface=''):
		"""
		Return a dictionary with port description. If you not specify an interface, 
		this method return all ports descriptions for each port in the switch.
		You can specify filters.

		Params:
			interface = string (Could be interface or filter)
		"""
		try:
			self._isClosed()
			ports = OrderedDict({})
			#
			if interface:
				b = self.run("display interf " + interface)
			else:
				b = self.run("display interf")
			#
			b = [ x for x in b if x ]
			port = ""
			log = False
			for i in b:
				if i and ("Ethernet" in i.split()[0] or "Bridge" in i.split()[0]) and ("M-GigabitEthernet" not in i.split()[0]):
					port = i.split()[0].strip()
					log = True
				#
				if i and ("Description:" in i.split()[0]) and log:
					ports[port] = i.split("Description:")[1].strip()
					log = False
			#
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsAndDesc(): %s" % str(error)}
	
	def getPortsDetail(self, interface='', filters=''):
		"""
		Return a dictionary with all port details. If you not specify an interface, 
		this method return all ports details for each port in the switch.

		Params:
			interface = string
			filters = string
		"""
		try:
			self._isClosed()
			ports = OrderedDict({})
			#
			if filters:
				b = self.run("display interf | " + filters)	
			elif interface:
				b = self.run("display interf " + interface)
			else:
				b = self.run("display interf", timeout=20)
			#
			PORT = ""
			lo = False
			for x in b: 
				if x == "":
					lo = False
				if x and ("M-Giga" not in x.split()[0]) and ("Ethernet" in x.split()[0] or "Bridge-Aggregation" in x.split()[0]):
					PORT = x.split()[0]
					ports[PORT] = ""
					lo = True
				if lo:
					ports[PORT] += x + "\n"
			#
			return ports
		except Exception, error:
			return {"error": "Error in getPortsDetail(): %s" % str(error)}
	
	def getMacAddress(self, macAddress='', filters=''):
		"""
		Return a dictionary with port and vlan details, for each mac address.
		If you not specify mac address, this method return all mac address in
		the switch. You could specify filters.

		Params:
			macAddress = string
			filters = string
		"""
		try:
			self._isClosed()
			if macAddress and filters:
				_mval = "-".join(macAddress.replace(":","")[s:s+4] for s in range(0, len(macAddress.replace(":","")), 4)).lower()
				b = self.run("display mac-address " + _mval + " | " + filters)
			elif filters:
				b = self.run("display mac-address | " + filters)
			elif macAddress:
				_mval = "-".join(macAddress.replace(":","")[s:s+4] for s in range(0, len(macAddress.replace(":","")), 4)).lower()
				b = self.run("display mac-address " + _mval)	
			else:
				b = self.run("display mac-address")
			#
			mac = {}
			b = [ x for x in b if x ]
			for i in b:
				if len(i.split()[0]) == 14:	
					cut = [x for x in i.split() if x != "Learned" and x != "AGING"]
					mvalue = ":".join(cut[0].replace("-","")[s:s+2] for s in range(0, len(cut[0].replace("-","")), 2)).upper()
					mac[mvalue] = { "vlan": cut[1], 
									"port": cut[-1] }
			#
			return mac
		except Exception, error:
			return { "error": "Error in getMacAddress(): %s" % str(error) }
	
	def getArp(self, ip='', interface=''):
		"""
		Return a dictionary with port, mac and status details, for each ip address in the ARP table.
		If you not specify any ip or interface, this method return all address in the switch. 
		You could specify ip or interface. You cannot use ip and interface at the same time.

		Params:
			ip = string
			interface = string
		"""
		try:
			if ip and interface:
				return Exception("You cannot use 'ip' and 'interface' at the same time.")
			self._isClosed()
			if ip:
				b = self.run("sh arp " + ip)
			elif interface:
				b = self.run("sh arp interface " + interface)	
			else:
				b = self.run("sh arp all")
			#
			ports = {}
			ports["BAGG"] = "Bridge-Aggregation"
			ports["GE"] = "GigabitEthernet"
			ports["XGE"] = "Ten-GigabitEthernet"
			arp = OrderedDict({})
			b = [ x for x in b if x.split()[0].replace(".","").isdigit() ]
			#
			for i in b:
				PORT = ""
				mvalue = ":".join(i.split()[1].replace("-","")[s:s+2] for s in range(0, len(i.split()[1].replace("-","")), 2)).upper()
				for z in ports:
					if z in i.split()[3]:
						PORT = i.split()[3].replace(z,ports[z])
				arp[i.split()[0]] = { "port": PORT,
									  "mac": mvalue,
									  "vlan": i.split()[2] }
			#
			return arp
		except Exception, error:
			return { "error": "Error in getArp(): %s" % str(error) }
	
	def setPortsDesc(self, port='', desc=''):
		"""
		Set port description. Return a dictionary with vlan and port description.

		Params:
			port = string
			desc = string
		"""
		try:
			self._isClosed()
			if not port:
				raise Exception("No port selected. Please, specify port!")
			#
			self.child.sendline("system-view")
			self.child.expect("]")
			self.child.sendline("interf " + port)
			self.child.expect("]")
			if desc:
				self.child.sendline("description " + desc)
			else:
				self.child.sendline("undo description")
			self.child.expect("]")
			self.child.sendline("return")
			self.child.expect("<" + self.prompt + ">")
			self.child.sendline("save")
			self.child.expect("Y/N")
			self.child.sendline("y")
			self.child.expect("enter key")
			self.child.sendline()
			self.child.expect("Y/N")
			self.child.sendline("y")
			self.child.expect("<" + self.prompt + ">")
			#
			sleep(0.1)
			ports = self.getPortsAndDesc(port)
			for i in ports:
				ports[i]= (ports[i], "Done")
			#
			return ports
		except Exception, error:
			return {"error": "Error in setPortsDesc(): %s" % str(error)}

	def setPortVlan(self, port='', vlan={"id": 0, "tagged": True}):
		"""
		Set port vlan. You can add or remove vlan from tagged ports, or you can
		change vlan on untagged ports.

		Params:
			port = string
			vlan = dict({"id": int, "tagged": Boolean})
		"""
		try:
			#
			__ret__ = []
			self._isClosed()
			if not port or not vlan['id']:
				raise Exception("No port or vlan provided. Please, specify all parameters!")
			ieeeVlan = range(1,4096)
			if vlan['id'] not in ieeeVlan:
				raise Exception("Vlan ID incorrect!")
			#
			vlanp = self.getPortVlan(port)[port]
			#
			if vlan['tagged']:
				if not vlanp['tagged']:
					raise Exception("Port %s is not in trunk mode. Please contact your network administrator!" % port)
				#
				self.child.sendline("system-view")
				self.child.expect("]")
				self.child.sendline("interf " + port)
				self.child.expect("]")
				self.child.sendline("port trunk permit vlan " + str(vlan['id']))
				self.child.expect("]")
				__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				self.child.sendline("return")
				self.child.expect("<" + self.prompt + ">")
				self.child.sendline("save")
				self.child.expect("Y/N")
				self.child.sendline("y")
				self.child.expect("enter key")
				self.child.sendline()
				self.child.expect("Y/N")
				self.child.sendline("y")
				self.child.expect("<" + self.prompt + ">")
			else:
				if vlanp['tagged']:
					raise Exception("Port %s is not in access mode. Please contact your network administrator!" % port)
				#
				self.child.sendline("system-view")
				self.child.expect("]")
				self.child.sendline("interf " + port)
				self.child.expect("]")
				self.child.sendline("port access vlan " + str(vlan['id']))
				self.child.expect("]")
				__ret__.append("\n".join(str(self.child.before).replace("\r", "").split("\n")[0:-1]))
				#
				self.child.sendline("return")
				self.child.expect("<" + self.prompt + ">")
				self.child.sendline("save")
				self.child.expect("Y/N")
				self.child.sendline("y")
				self.child.expect("enter key")
				self.child.sendline()
				self.child.expect("Y/N")
				self.child.sendline("y")
				self.child.expect("<" + self.prompt + ">")
			#
			return __ret__
		except Exception, error:
			return {"error": "Error in setPortVlan(): %s" % str(error)}
