# -*- coding: utf-8 -*-
# AUTHOR: Luca Carta

from distutils.core import setup
setup(name='NetDevices', 
		version='1.15',
		description='Python utilities for switchs and routers management', 
		author='Luca Carta',
		author_email='luca@crs4.it',
		py_modules=['NetDevices'])
