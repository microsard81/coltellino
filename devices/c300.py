# AUTHOR: Luca Carta

__author__  = "Luca Carta - CRS4 - luca@crs4.it"
__date__    = "2015/07/14"
__version__ = "0.9"
__comment__ = "Script per l'esecuzione di vari comandi sui C300"

import pexpect
from collections import OrderedDict
from time import sleep

def connetti(username, password, secret, device, prompt):	
	try:
		ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
		child = pexpect.spawn(ssh + username + "@" + device)
		child.expect(username + "@" + device + "\'s password: ", timeout=8)
		child.sendline(password)
		child.expect(prompt + ">", timeout=8)
		child.sendline("enable")
		child.expect("Password: ", timeout=8)
		child.sendline(secret)
		child.expect(prompt + "#", timeout=8)
		child.sendline("ter len 0")
		child.expect(prompt + "#", timeout=8)
		return (child, prompt)
		#
	except Exception, e:
		print "Error in connetti(): %s" % str(e)

def esegui(chi, comando):
	try:
		child = chi[0]
		prompt = chi[1]
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		child.sendline(comando)
		child.expect(prompt + "#", timeout=15)
		da_loggare = str(child.before)
		return da_loggare.split("\r\n")[1:-1]
	except Exception, e:
		print "Error in esegui(): %s" % str(e)
		return {"error": str(error)}

def chiudi(chi):
	try:
		child = chi[0]
		child.sendline("exit")
		child.close()
	except Exception, e:
		print "Error in chiudi(): %s" % str(e)

def vlanPortDetail(vlan):
	try:
		ports = OrderedDict({})
		#
		for i in vlan:
			unt = []
			tag = []
			u = False
			t = False	
			for s in vlan[i]:
				if s and s[0] == "U":	
					u = True
					t = False
				elif s and s[0] == "T":	
					t = True
					u = False
				#
				if u:
					_id = s.replace(" ", "").replace("U","").split(",")
					for l in _id:
						if l and len(l.split("-")) == 1:
							unt.append(int(l))
						elif l:
							ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
							for el in ran:
								unt.append(el)
				if t:
					_id = s.replace(" ", "").replace("T","").split(",")
					for l in _id:
						if l and len(l.split("-")) == 1:
							tag.append(int(l))
						elif l:
							ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
							for el in ran:
								tag.append(el)
			ports[i] = {"untagged": unt, "tagged": tag}
		return ports
	except Exception, error:
		print "Error in vlanPort(): %s" % str(error)

def getPortVlan(chi, _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _filter:
			b = esegui(chi, "sh ip interf brief " + _filter)	
		else:
			b = esegui(chi, "sh ip interf brief")
		#
		vlan = OrderedDict({})
		#
		for i in b:
			if i.split("  ")[0].strip() != "Interface":
				a =  esegui(chi,'sh int switch '+i.split("  ")[0])
				take = False
				_add = []
				for z in a:
					if take:	
						if z and z.find("Native") == -1:
							_add.append(z)
					if z.find("Q       Vlans") != -1:	take = True
				#
				vlan[i.split("  ")[0]] = _add
		#
		ports = vlanPortDetail(vlan)
		return ports
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}

def getPortsAndDesc(chi, _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		ports = OrderedDict({})
		#
		if "grep"  in _filter or "begin" in _filter:
			b = esegui(chi, "sh interf desc | " + _filter)
			for i in b:
				if i.split("  ")[0].strip() != "Interface":
					ports[i.split("  ")[0]] = i.split("        ")[-1].strip()	
		elif _filter:
			b = esegui(chi, "sh interf " + _filter)
			for i in b:
				if "Description: " in i:
					ports[_filter] = i.split("Description: ")[1]	
		else:
			b = esegui(chi, "sh interf desc")
			for i in b:
				if i.split("  ")[0].strip() != "Interface":
					ports[i.split("  ")[0]] = i.split("        ")[-1].strip()	
		#
		#
		return ports
	except Exception, error:
		print "Error in getPortsAndDesc(): %s" % str(error)
		return {"error": str(error)}

def getPortsDetail(chi, _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		ports = OrderedDict({})
		#
		if "grep"  in _filter or "begin" in _filter:
			b = esegui(chi, "sh interf | " + _filter)	
		elif _filter:
			b = esegui(chi, "sh interf " + _filter)
		else:
			b = esegui(chi, "sh interf")
		#
		b = [x for x in b if x]
		PORT = ""
		for i in b:
			if "Ethernet" in i.split()[0].strip() or "Port-channel" in i.split()[0].strip():
				PORT = i.split()[0].strip()+i.split()[1].strip()
				ports[PORT] = i + "\n"
			else:
				ports[PORT] += i + "\n"
		#
		return ports
	except Exception, error:
		print "Error in getPortsDetail(): %s" % str(error)
		return {"error": str(error)}

def getMacAddress(chi, _mac=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _mac:
			b = esegui(chi, "sh mac-address-table address " + _mac)	
			comp = -1
		else:
			b = esegui(chi, "sh mac-address-table")
			comp = -2
		#
		mac = {}
		take = False
		for i in b:
			if take and i.find("\t") != -1:	
				mac[i.split("\t")[1].strip().upper()] = { "vlan": i.split("\t")[0].strip(), "port": i.split("\t")[comp].strip() }
			if i.find("VlanId     ") != -1:	take = True	
		#
		return mac
	except Exception, error:
		print "Error in getMacAddress(): %s" % str(error)
		return { "error": str(error) }


def getPorts(chi, _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _filter:
			b = esegui(chi, "sh ip interf brief | " + _filter)	
		else:
			b = esegui(chi, "sh ip interf brief")
		#
		ports = []
		for i in b:
			if i.split("  ")[0] != "Interface":
				ports.append(i.split("  ")[0])
		#
		return ports
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}


def setPortsDesc(chi, _port='', _desc=''):
	try:
		child = chi[0]
		prompt = chi[1]
		#
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		if not _port:
			raise Exception("No port selected. Please, specify port!")
		#
		child.sendline("conf t")
		child.expect("#")
		child.sendline("interf " + _port)
		child.expect("#")
		child.sendline("description " + _desc)
		child.expect("#")
		child.sendline("end")
		child.expect(prompt + "#")
		child.sendline("write mem")
		child.expect(prompt + "#")
		#
		sleep(0.1)
		ports = getPortsAndDesc(chi, _port)
		for i in ports:
			ports[i]= (ports[i], "Done")
		#
		return ports
	except Exception, error:
		print "Error in setPortsDesc(): %s" % str(error)
		return {"error": str(error)}
