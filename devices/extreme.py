# AUTHOR: Luca Carta

__author__  = "Luca Carta - CRS4 - luca@crs4.it"
__date__    = "2015/07/17"
__version__ = "0.4"
__comment__ = "Script per l'esecuzione di vari comandi sugli Extreme"

import pexpect, re
from collections import OrderedDict
from numpy import arange
from time import sleep

def connetti(username, password, secret, device, prompt):	
	try:
		ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
		child = pexpect.spawn(ssh + username + "@" + device)
		child.expect("Enter password for " + username + ":")
		child.sendline(password)
		child.expect(prompt + ".\d #")
		child.sendline("disable clipaging")
		child.expect(prompt + ".\d #")
		return (child, prompt)
		#
	except Exception, e:
		print "Error in connetti(): %s" % str(e)

def esegui(chi, comando):
	try:
		child = chi[0]
		prompt = chi[1]
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		child.sendline(comando)
		child.expect(prompt + ".\d #", timeout=240)
		da_loggare = str(child.before).replace("\r", "")
		return da_loggare.split("\n")[1:]
	except Exception, e:
		print "Error in esegui(): %s" % str(e)
		return {"error": str(error)}

def chiudi(chi):
	try:
		child = chi[0]
		child.sendline("exit")
		child.close()
	except Exception, e:
		print "Error in chiudi(): %s" % str(e)

def getPortVlan(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "show ports " + _port + " information detail")	
		else:
			b = esegui(chi, "show ports information detail")
		#
		b = [ x for x in b if x ]
		d = []
		for i in b:
			z = i.split("\t")
			z = [x for x in z if x]
			d.append(z)
		#
		dic = OrderedDict({})
		k = ""
		app = []
		lo = False
		#
		for i in d:
			if i[0] == 'Port:':
				k = i[1].split("(")[0]
				lo = True
			elif i[0] == "STP cfg:":
				lo = False
				dic[k] = app
				app = []
			if lo:
				if i[0] != 'Port:': 
					app.append(i)
		#
		_dic = OrderedDict({})
		#
		for z in dic:
			lo = False
			_a = {}
			tag = []
			unt = []
			for i in dic[z]:
				if lo and i[0]:
					if "Internal" in i[0].split(",")[1]:	unt.append(int(i[0].split(",")[1].split()[-1]))
					elif "802.1Q" in i[0].split(",")[1]:	tag.append(int(i[0].split(",")[1].split()[-1]))
				if i[0] == "VLAN cfg: ":
					lo = True
			_a = {'tagged': sorted(tag), 'untagged': sorted(unt)}
			_dic[z] = _a
		#
		return _dic
		#
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}

def getPorts(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "show ports " + _port + " no-refresh")	
		else:
			b = esegui(chi, "show ports no-refresh")
		#
		b = [ x for x in b if x ]
		ports = []
		for i in b:
			if i.split()[0][0].isdigit():
				ports.append(i.split()[0])
		#
		return ports
	except Exception, error:
		print "Error in getPort(): %s" % str(error)
		return {"error": str(error)}

def getPortsAndDesc(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "show ports " + _port + " information detail")	
		else:
			b = esegui(chi, "show ports information detail")
		#
		b = [ x for x in b if x ]
		d = []
		for i in b:
			z = i.split("\t")
			z = [x for x in z if x]
			d.append(z)
		#
		dic = OrderedDict({})
		k = ""
		app = []
		#
		for i in d:
			if i[0] == 'Port:':
				if len(i[1]) > 4:
					k = i[1].split("(")[0]
					desc = i[1].split("(")[1].replace("):","")
					dic[k] = desc
				else:
					k = i[1]
					desc = "No description found"
					dic[k] = desc
		#
		return dic
	except Exception, error:
		print "Error in getPortsAndDesc(): %s" % str(error)
		return {"error": str(error)}

def getPortsDetail(chi, _port=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _port:
			b = esegui(chi, "show ports " + _port + " statistics no-refresh")
			c = esegui(chi, "show ports " + _port + " congestion no-refresh")	
			d = esegui(chi, "show ports " + _port + " txerrors no-refresh")	
			e = esegui(chi, "show ports " + _port + " rxerrors no-refresh")		
		else:
			b = esegui(chi, "show ports statistics no-refresh")
			c = esegui(chi, "show ports congestion no-refresh")	
			d = esegui(chi, "show ports txerrors no-refresh")	
			e = esegui(chi, "show ports rxerrors no-refresh")	
		#
		b = [ x for x in b if x ]
		c = [ x for x in c if x ]
		d = [ x for x in d if x ]
		e = [ x for x in e if x ]
		dic = OrderedDict({})
		dic['statistics'] = b
		dic['congestion'] = c
		dic['txerrors'] = d
		dic['rxerrors'] = e
		#
		return dic
	except Exception, error:
		print "Error in getPortsDetail(): %s" % str(error)
		return {"error": str(error)}

def getMacAddress(chi, _mac=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _mac:
			b = esegui(chi, "sh fdb " + _mac.lower())
		else:
			b = esegui(chi, "sh fdb")
		#
		mac = {}
		b = [ x for x in b if x ]
		for i in b:
			if len(i.split()[0]) == 17:	
				mvalue = i.split()[0].upper()
				mac[mvalue] = { "vlan": str(int(i.split()[1].split("(")[1].replace(")",""))), 
								"port": i.split()[-1] }
		#
		return mac
	except Exception, error:
		print "Error in getMacAddress(): %s" % str(error)
		return { "error": str(error) }


def setPortsDesc(chi, _port='', _desc=''):
	try:
		child = chi[0]
		prompt = chi[1]
		#
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		if not _port:
			raise Exception("No port selected. Please, specify port!")
		#
		child.sendline("configure ports %s display-string %s" % (_port, _desc))
		child.expect("#")
		child.sendline("save")
		child.expect("(y/N)", timeout=10)
		child.sendline("Yes")
		child.expect("#")
		#
		sleep(0.1)
		ports = getPortsAndDesc(chi, _port)
		for i in ports:
			ports[i]= (ports[i], "Done")
		#
		return ports
	except Exception, error:
		print "Error in setPortsDesc(): %s" % str(error)
		return {"error": str(error)}


