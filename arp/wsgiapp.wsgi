import sys
import os

# this assumes you have installed into virtualenv
instance_dir = '/home/achmed/.virtualenvs/sandman/'
pyenv_bin_dir = os.path.join(instance_dir, 'bin')
activate_this = os.path.join(pyenv_bin_dir, 'activate_this.py')
execfile(activate_this, dict(__file__=activate_this))

sys.stdout = sys.stderr
module = "dispatch"
current_directory = os.path.dirname(__file__)
parent_directory = os.path.dirname(current_directory)
module_name = os.path.basename(current_directory)

module_import = os.path.join(module_name, module)
#module_import = os.path.join(current_directory, module)
if parent_directory not in sys.path:
        sys.path.insert(0,parent_directory)

#module_import = os.path.join(current_directory, module)
if parent_directory not in sys.path:
        sys.path.insert(0,parent_directory)

if current_directory not in sys.path:
        sys.path.insert(0,current_directory)

if module_import not in sys.path:
        sys.path.insert(0,module_import)


sys.path.append('/home/achmed/pdns')
sys.path.append('/home/achmed/pdns/dispatch')

from dispatch import app as application
