# -*- coding:utf-8 -*-

# Copyright 2013 CRS4
# All Rights Reserved.
#
#    Licensed under the GNU General Public License, version 2 (the "License");
#    you may not use this file except in compliance with the License. You may
#    obtain a copy of the License at
#
#         http://www.gnu.org/licenses/gpl-2.0.html
#
#    Unless required by applicable law or agreed to in writing, software
#app.register_blueprint(muse.apiv2)
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


"""
    :copyright: |copy| 2013 by CRS4.
    :license: gpl-2, see License for more details.

    Various helpers, custom exceptions, login mechanism and wrappers
"""
import sys
import os

sys.path.insert(0, os.path.abspath('../'))

import subprocess
import logging
import BaseHTTPServer  # For HTTP codes.

from werkzeug.exceptions import HTTPException
from flask import current_app, request
from flask.ext.login import current_user


from dispatch.models import Records, Domains
from netaddr import IPAddress, IPNetwork

HTTP_CODES = BaseHTTPServer.BaseHTTPRequestHandler.responses
LOG = logging.getLogger(__name__)

class CustomException(HTTPException):
    """
    Cancel abortion of the current task and return with
    the given message and error code.
    """
    def __init__(self, message, format='json', state='success',
                 code=200, url=None):
        LOG.error("CiException(%s %s): %s" % (state, code, message))
        #self.response = render_message(request, message,
        #         format, state=state, code=code, url=url)
        self.response = make_response(status_code=code, status=state,
                                      url=url, message=message)

    def get_response(self, environ):
        return self.response




def not_implemented():
    """
    Quick function to reply with a Not implemented response
    """
    raise CustomException('Not implemented', state='error', code=501)


class log_activity(object):
    def __init__(self, action=None):
        self.action = action

    def __call__(self, func):
        """
        A decorator that logs the activity of the script.
        (it actually just prints it, but it could be logging!)
        """
        def wrapper(*args, **kwargs):
            # LOG.info(" ".join([current_user.uid, args[1].name.data, args[1].content.data]))
            try:
                name = args[1].name.data
            except:
                name = str(args[1].name)

            try:
                if getattr(args[1],'content', None):
                    content = str(args[1].content.data)
                else:
                    content = str(args[1].data)
            except:
                if getattr(args[1],'content', None):
                    content = str(args[1].content)
                else:
                    content = "*** Unable to fetch content informations"

            try:
                type = args[1].type.data
            except:
                type = ""

            current_app.logger.info(" ".join([current_user.uid,
                                                self.action,
                                                name,
                                                type,
                                                content]))

            res = func(*args, **kwargs)
            return res
        return wrapper


class purge_cache_class(object):
    def __init__(self, *args):
        self.func = args[0]

    def __call__(self, func):
        def wrapper(*args, **kwargs):
            try:
                name = args[1].name.data
            except:
                name = str(args[1].name)
            # current_app.logger.debug("QUA" +" ".join(['sudo', current_app.config['PDNS_CONTROL'], 'purge', name]))
            subprocess.call(['sudo', current_app.config['PDNS_CONTROL'], 'purge', name])
            current_app.logger.info("DNS PURGED!.")

            res = func(*args, **kwargs)
            return res
        return wrapper

def purge_cache(name):
    current_app.logger.debug("PURGE OP: " +" ".join(['sudo', current_app.config['PDNS_CONTROL'], 'purge', name]))
    subprocess.call(['sudo', current_app.config['PDNS_CONTROL'], 'purge', name])
    current_app.logger.info("DNS PURGED!")

def clean_reversed_sub(reversed):
    clean = ".".join(reversed.split('.')[-3::-1])
    count = clean.count(".")
    return clean + ".0"*(4 - clean.count(".")-1)


def get_subnet(subnet, mask=16):
    net = IPNetwork('%s/%s' % (subnet, mask))
    return net


# def get_free_ips(subnet, iplist):
#     # si potrebbe rifare abbondantemente usando "set"
#     # sub = subnet.subnet(16)

#     sub_dict = {}
#     free_sub_dict = {}
#     free_sub_stripped = {}

#     for ip in iplist:
        
#         net_ip = IPNetwork(ip)
#         supernets = net_ip.supernet(23)[0]
#         # if net_ip not in sub_dict:
#         if supernets not in sub_dict:
#             sub_dict[supernets] = []

#         # sub_dict[supernets].append(ip)
#         sub_dict[supernets].append(IPAddress(ip))
#     # from pprint import *
#     # pprint(sub_dict)
#     # print sub_dict
#     ordered_subs = []
#     for sub in sub_dict:
#         ordered_subs.append(sub)

#         if sub in subnet:
#             # all_ips = [vlanip for vlanip in sub]
#             all_ips = set(sub[5:-2])

#             # for free_ip in all_ips[5:-2]:
#             #     if free_ip not in sub_dict[sub]:
#             #         if str(sub) not in free_sub_dict:
#             #                     free_sub_dict[str(sub)] = []
#             #                     free_sub_stripped[str(sub)] = []
#             #                     ordered_subs.append(sub)
#             #                 free_sub_dict[str(sub)].append(str(free_ip))
#             #                 free_sub_stripped[str(sub)].append(".".join(str(free_ip).split(".")[2:]))
#             available_ips = all_ips.difference(set(sub_dict[sub]))

#             sorted_available_ips = sorted(available_ips)
            
#             free_sub_dict[str(sub)] = [str(ip )  for ip in sorted_available_ips]
#             free_sub_stripped[str(sub)] = [".".join(str(ip).split(".")[2:])  for ip in sorted_available_ips]

#             # for free_ip in available_ips:

#             #         if str(sub) not in free_sub_dict:
#             #             free_sub_dict[str(sub)] = []
#             #             free_sub_stripped[str(sub)] = []
#             #             ordered_subs.append(sub)
#                     # free_sub_dict[str(sub)].append(str(free_ip))
#                     # free_sub_stripped[str(sub)].append(".".join(str(free_ip).split(".")[2:]))
#     # print free_sub_dict
#     # pprint(free_sub_dict)
#     ordered_subs.sort()

#     return free_sub_dict, free_sub_stripped, [str(o) for o in ordered_subs]


def get_free_ips(subnet, iplist):
    # sub = subnet.subnet(16)

    sub_dict = {}
    free_sub_dict = {}
    free_sub_stripped = {}

    sub_dict = get_ips_per_subnet(subnet)

    for ip in iplist:
        addr_ip = IPAddress(ip)

        for sub in sub_dict:
            if addr_ip in sub:
                try:
                    sub_dict[sub].remove(addr_ip)
                except:
                    pass

    # from pprint import *
    # pprint(sub_dict)

    ordered_subs = [sub for sub in sub_dict.keys()]
    
    free_sub_dict = {}
    free_sub_stripped = {}
    for sub in sub_dict:
        free_sub_dict[str(sub)] = [str(ip) for ip in sub_dict[sub]]
        free_sub_stripped[str(sub)] = [".".join(str(ip).split(".")[2:])  for ip in sub_dict[sub]]

    ordered_subs.sort()
    
    # print free_sub_dict
    return free_sub_dict, free_sub_stripped, [str(o) for o in ordered_subs]

def get_gateway(sub, num):
    ip = str(sub[0])
    ip = ip.split(".")[:-1]
    return IPAddress(".".join(ip+[num]))


def get_ips_per_subnet(main_sub):
    parser = current_app.config['VLAN_PARSER']
    reserved = current_app.config['RESERVED_IPS']

    sections = parser.sections()
    sub_dict = {}
    reserved = set([IPAddress(ip) for ip in reserved])

    for section in sections:
        clean_net = parser.get(section,'net')
        gw = parser.get(section,'gw')
        no_scan = parser.get(section,'no_scan')
        net = IPNetwork(clean_net)
        if net in main_sub:
            # sub_dict[clean_net] = set(net[1:-1])
            ip_list = list(net)
            ip_list.remove(get_gateway(net, gw))
            ip_list = set(ip_list)
            if no_scan!='0':
                no_scan_set = set([IPAddress(no_ip.strip()) for no_ip in no_scan.split(",")])
                ip_list = ip_list.difference(no_scan_set)
            
            ip_list = ip_list.difference(reserved)
                
            ip_list = sorted(list(ip_list))[1:-1]

            sub_dict[net] = ip_list
            
    return sub_dict
            

def get_label_per_subnet():
    parser = current_app.config['VLAN_PARSER']
    sections = parser.sections()
    sub_dict = {}
    for section in sections:
        sub_dict[ parser.get(section,'net')] = parser.get(section,'desc')
    return sub_dict


def get_all_free_ips(domain):
    # print current_app.config['VLAN_PARSER'].sections()
    mask = 16
    reverse = current_app.config['DOMAINS_REVERSE_MAP']

    dom = Domains.query.filter(Domains.name == domain).first()
    r = Records.query.filter(Records.domain == dom ).filter(Records.type=='A').all()
    items = sorted(r, key=lambda k: "%3s.%3s.%3s.%3s" % tuple(k.content.split(".")))
    items_cleaned = [i.content for i in items]
    # print items_cleaned

    rev = clean_reversed_sub(reverse.get(domain))
    sub = get_subnet(rev, mask=mask)

    it, stripped, ordered = get_free_ips(sub, items_cleaned)

    # response = {"status": "ok", "free": items_cleaned}
    label_dict = get_label_per_subnet()
    response = {"status": "ok", "free": it, "ordered_keys": ordered, "stripped": stripped,
                "labels" : label_dict
                }

    return response
