import logging

################################
# configuration point
################################
LOG_NAME = 'pwd'
#TARGET = '/tmp/aggiornamento_pwd_bellerofonte.log'
#TARGET='/home/crs4/calendario/cal/warning_service/mail_alert.log'
TARGET = '/home/crs4/news/msg/digest_service/job.log'

#TARGET = '/home/crs4/ecp/batch_scripts/batch_scripts.log'
LOG_BASE = logging.DEBUG

##### end configuration part ###
#LEVELS = {'debug'   : logging.DEBUG,
#          'info'    : logging.INFO,
#          'warning' : logging.WARNING,
#          'error'   : logging.ERROR,
#          'critical': logging.CRITICAL}


#logging.basicConfig(level=LEVELS)
log = logging.getLogger(LOG_NAME)
hdlr = logging.FileHandler(TARGET)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
log.addHandler(hdlr)
log.setLevel(LOG_BASE)
