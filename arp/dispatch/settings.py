import os


LOGGIN_LEVEL = {'DEBUG': 10,
                'WARN': 30,
                'INFO': 20
                }

class Config(object):
    """
    Main configuration class, is intended to be used as a base class to
    inheritate
    """
    # WHERE THE WEB SERVICE SHOULD LISTEN
    HOST = '0.0.0.0'
    PORT = 5001

    LOG_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'log','activity.log')

    LOG_LEVEL = LOGGIN_LEVEL['DEBUG']

    SECRET_KEY = 'dev key pass foobar 1 2 3'

    SQLALCHEMY_DATABASE_URI = 'mysql://pdns:xBcAuPWfIjjt@localhost/pdns'
    # SQLALCHEMY_DATABASE_URI = 'mysql://pdns:xBcAuPWfIjjt@localhost/pdns_dev'

    MEDIA_CONFIG_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'media_conf')

    # LDAP_HOST = 'parliament.crs4.it'
    LDAP_HOST = "ldapcluster.crs4.it"
    LDAP_SEARCH_BASE = 'ou=people, dc=crs4'
    # LDAP_REQUIRED_GROUP = 
    CSRF_ENABLED = True
    LDAP_ALLOWED_GROUP = "cn=dns,ou=alt_groups,dc=crs4"

    DOMAINS_REVERSE_MAP = { 'crs4.it': '148.156.in-addr.arpa',
                            'crs4.int': '0.10.in-addr.arpa'
                            }
    PDNS_CONTROL = "/usr/bin/pdns_control"
    
    VLAN_EXPLAINER = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'conf.ini')
    RESERVED_IPS = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'reserved_ips.conf')

class ProductionConfig(Config):
    """ Production settings """
    DEBUG = False
    TESTING = False
    LOG_LEVEL = LOGGIN_LEVEL['INFO']


class DevelopmentConfig(Config):
    """ Development settings """
    DEBUG = True
    TESTING = True
    # LOG_FILE = "/tmp/demo_site_dev.log"
    LOG_LEVEL = LOGGIN_LEVEL['DEBUG']
