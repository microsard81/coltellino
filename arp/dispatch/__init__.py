import os
import logging
from logging.handlers import RotatingFileHandler
from logging import Formatter


from flask import Flask, render_template

from dispatch import settings

from flask.ext import login
from flask.ext.ldap import LDAP
from flask.ext.admin import Admin
from flask.ext.sqlalchemy import SQLAlchemy
from ConfigParser import ConfigParser

from dispatch.backends import ldap_auth

##################################
# DEVELOPMENT MODE 
##################################
DEV = True


def configure_app(app):
    """
    setup the flask application using setting directives.

    there are 3 ways to properly configure the flask app:

    * changing ``settings.py`` content
    * using the environmental variable ``MIDDLEWARE_SETTINGS``
    * using a custom python file ``settings_local.cfg``

    """
    if DEV:
        setup = settings.DevelopmentConfig
    else:
        setup = settings.ProductionConfig

    app.config.from_object(setup)
    app.config.from_envvar('SETTINGS', silent=True)
    
    # parent directory
    here = os.path.dirname(os.path.abspath( __file__ ))
    config_path = os.path.join(os.path.dirname(here), 'settings_local.cfg')
    if os.path.exists(config_path):
        app.config.from_pyfile(config_path)




def setup_logging(app):
    """
    setup logging facilities on setting directives
    """
    log_file_path = app.config.get('LOG_FILE')
    log_level = app.config.get('LOG_LEVEL', logging.WARN)
    if log_file_path:
        file_handler = RotatingFileHandler(log_file_path)
        file_handler.setFormatter(Formatter(
            '%(name)s:%(levelname)s:[%(asctime)s] %(message)s '
            '[in %(pathname)s:%(lineno)d]'
            ))
        file_handler.setLevel(log_level)
        app.logger.addHandler(file_handler)
        logger = logging.getLogger('dispatcher')
        logger.setLevel(log_level)
        logger.addHandler(file_handler)


def create_app():
    """
    Used to create a configured flask application
    """
    app = Flask(__name__)

    configure_app(app)
#    setup_error_email(app)
    setup_logging(app)

    return app


# Initialize flask-login
# def init_login():
#     login_manager = login.LoginManager()
#     login_manager.init_app(app)

#     # Create user loader function
#     @login_manager.user_loader
#     def load_user(user_id):
#         print user_id
#         return None
#         #return db.session.query(User).get(user_id)

def init_login(app):
    login_manager = login.LoginManager()
    login_manager.init_app(app)

    # Create user loader function
    @login_manager.user_loader
    def load_user(user_id):
        return ldap_auth.User(uidNumber=user_id)
        #return db.session.query(User).get(user_id)

def vlan_config(app):
    app.config['VLAN_PARSER'] = ConfigParser()
    app.config['VLAN_PARSER'].read(app.config['VLAN_EXPLAINER'])
    return app


def reserved_config(app):
    conf = app.config['RESERVED_IPS']
    reserved = []
    with open(conf) as f:
        r = f.readlines()
        for ip in r:
            tmp = ip
            tmp = tmp.split("#")[0].strip()
            if tmp:
                reserved.append(tmp)

    app.config['RESERVED_IPS'] = reserved
    return app

app = create_app()
app = vlan_config(app)
app = reserved_config(app)

admin = Admin(app, name="DNS")

# Initialize flask-login
init_login(app)

db = SQLAlchemy(app)
ldap = LDAP(app)


# l'import qui non e' un errore!
import dispatch.views
# from middleware.models import db, User, Role
