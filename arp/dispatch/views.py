# -*- coding:utf-8 -*-

# Copyright 2014 CRS4
# All Rights Reserved.
#
#    Licensed under the GNU General Public License, version 2 (the "License");
#    you may not use this file except in compliance with the License. You may
#    obtain a copy of the License at
#
#         http://www.gnu.org/licenses/gpl-2.0.html
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
"""
    :copyright: |copy| 2014 by CRS4.
    :license: gpl-2, see License for more details.

    View file, used to register blueprints/api versions 
"""

from flask import  render_template, request, redirect, url_for
from dispatch import app
from dispatch.components import internal
from dispatch.components import admin
from dispatch.components import ldap_endpoint
from flask import g
from flask import jsonify



from dispatch.lib.helpers import get_all_free_ips
from dispatch.components import admin_struct
from flask.ext.login import current_user



app.register_blueprint(internal.apiv1)
app.register_blueprint(admin.dnsv1)
app.register_blueprint(ldap_endpoint.authv1)


@app.before_request
def before_request():
    g.user = current_user

@app.route('/')
def index():
    #return redirect(url_for("admin.index"))
    return redirect(url_for("dispatch.components.ldap_endpoint.login"))



@app.route('/get_all_free_ips/<domain>')
def json_get_all_free_ips(domain):
    response = get_all_free_ips(domain)
    return jsonify(**response)

@app.route('/show_all_free_ips/<domain>')
def show_get_all_free_ips(domain):
    response = get_all_free_ips(domain)
    free = response['free']
    stripped = response['stripped']
    ordered_keys = response['ordered_keys']
    labels = response['labels']
    return render_template("/show_available_ips.html", ip_list=free, 
                                                        ordered_keys=ordered_keys,
                                                        ip_list_stripped = stripped,
                                                        labels= labels
                                                        )

admin_struct.register()