import simpleldap
from flask import current_app as app

from flask.ext.login import login_user, UserMixin, login_required, logout_user, current_user

#from wtforms import Form,   questo non fa funzionare CSRF
from flask.ext.wtf import Form
from wtforms import TextField, PasswordField, validators

# from dispatch import ldap

def ldap_fetch(uid=None, name=None, passwd=None, uidNumber=None):
    ldapsrv = app.config['LDAP_HOST']
    basedn = app.config['LDAP_SEARCH_BASE']

    try:
        if name is not None and passwd is not None:
            l = simpleldap.Connection(ldapsrv,
                dn='uid={0},{1}'.format(name, basedn), password=passwd)
            r = l.search('uid={0}'.format(name), base_dn=basedn)

        elif uid is not None:
            l = simpleldap.Connection(ldapsrv)
            r = l.search('uid={0}'.format(uid), base_dn=basedn)

        else:
            l = simpleldap.Connection(ldapsrv)
            r = l.search('uidNumber={0}'.format(uidNumber), base_dn=basedn)

        return {
                'uid': r[0]['uid'][0],
                'name': r[0]['cn'][0],
                'id': unicode(r[0]['uidNumber'][0]),
                'gid': int(r[0]['gidNumber'][0]),
                'dn': r[0].dn
                }
    except Exception as e:
        return None


def user_in_management_group(dn):
    ldapsrv = app.config['LDAP_HOST']
    basedn = app.config['LDAP_ALLOWED_GROUP']
    try:
        l = simpleldap.Connection(ldapsrv)
        r = l.search('uniqueMember={0}'.format(dn), base_dn=basedn)
        return dn.lower() in map(lambda x: x.lower(), r[0]['uniqueMember'])
    except Exception as e:
        return False

class User(UserMixin):
    def __init__(self, uid=None, name=None, passwd=None, uidNumber=None):

        self.active = False

        ldapres = ldap_fetch(uid=uid, name=name, passwd=passwd, uidNumber=uidNumber)
        if ldapres is not None:
            self.name = ldapres['name']
            self.id = ldapres['id']
            self.uid = ldapres['uid']
            self.dn = ldapres['dn']
            # assume that a disabled user belongs to group 404
            # if ldapres['gid'] != 404:
            if user_in_management_group(ldapres['dn']):
                self.active = True
            self.gid = ldapres['gid']

    def get_name(self):
        return self.name

    def is_active(self):
        return self.active

    def get_id(self):
        return self.id

    def __repr__(self):
        # return "%s %s" % (self.uid, self.name)
        return getattr(self, 'name', '')


class LoginForm(Form):
    username = TextField("Username", [validators.Length(min=2, max=25)])
    password = PasswordField("Password", [validators.Required()])

    # def __init__(self, *args, **kwargs):
    #     kwargs['csrf_enabled'] = True
    #     super(LoginForm, self).__init__(*args, **kwargs)

# @login_manager.user_loader
# def load_user(userid):
#     print "qua dentro! ", userid
#     return User(uid=userid)



# @app.route("/login", methods=["GET", "POST"])
# def login():
#     form = LoginForm(request.form)
#     if request.method == 'POST' and form.validate():
#         user = User(name=form.username.data, passwd=form.password.data)
#         if user.active is not False:
#             login_user(user)
#             flash("Logged in successfully.")
#             return redirect(url_for("some_secret_page"))
#     return render_template("login.html", form=form)


# @app.route("/logout", methods=["GET", "POST"])
# @login_required
# def logout():
#     logout_user()
#     return redirect(url_for("login"))


