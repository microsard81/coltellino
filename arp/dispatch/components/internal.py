# -*- coding:utf-8 -*-

# Copyright 2014 CRS4
# All Rights Reserved.
#
#    Licensed under the GNU General Public License, version 2 (the "License");
#    you may not use this file except in compliance with the License. You may
#    obtain a copy of the License at
#
#         http://www.gnu.org/licenses/gpl-2.0.html
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""

    :copyright: |copy| 2014 by CRS4.
    :license: gpl-2, see License for more details.

    Description
    ~~~~~~~~~~~

    API level v1 blueprint

"""

import os
import json
import logging

from flask import redirect, url_for
from flask import  render_template, request

from flask import jsonify
from flask import current_app
from flask import Blueprint, request

from dispatch.components import BufferAPI
from dispatch.lib.constants import Const as const


url_prefix = '/internal/v1'
apiv1 = Blueprint(__name__, __name__, url_prefix=url_prefix)


class HomeAPI(BufferAPI):

    """
    Generic root endpoint class
    """

    def get(self):
        return jsonify(status=const.OK)

    def post(self):
        d = json.loads(request.data)
        return jsonify(status=const.OK)

apiv1.add_url_rule('/', view_func=HomeAPI.as_view('apiv1'))