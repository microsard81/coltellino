# -*- coding:utf-8 -*-

# Copyright 2013 CRS4
# All Rights Reserved.
#
#    Licensed under the GNU General Public License, version 2 (the "License");
#    you may not use this file except in compliance with the License. You may
#    obtain a copy of the License at
#
#         http://www.gnu.org/licenses/gpl-2.0.html
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

"""
    .. include:: isonum.txt
    :copyright: |copy| 2013 by CRS4.
    :license: gpl-2, see License for more details.

    Base file to define routines to inherite in API versions
"""

from flask.views import MethodView

from dispatch.lib.helpers import not_implemented


class BufferAPI(MethodView):
    """
    Abstract response class, used to have global implementation for 
    unused methods
    """
    def get(self,*args,**kwargs):
        return "Dev"
    def post(self,*args,**kwargs):
        not_implemented()
    def put(self,*args,**kwargs):
        not_implemented()
    def patch(self,*args,**kwargs):
        not_implemented()
    def delete(self,*args,**kwargs):
         not_implemented()
    def options(self,*args,**kwargs):
         not_implemented()
