from flask.ext.admin import Admin, BaseView, expose
from flask import current_app
from flask import Blueprint, request, render_template

url_prefix = '/dns/v1'
dnsv1 = Blueprint(__name__, __name__, url_prefix=url_prefix)


def index():
    kwargs = {}
    base_template = 'admin/base.html'
    kwargs['admin_base_template'] = base_template
    return render_template('admin/myindex.html', **kwargs)

def test(**kwargs):
    base_template = 'admin/base.html'
    kwargs['admin_base_template'] = base_template
    return render_template('admin/test.html', **kwargs)



#dnsv1.add_view(MyView(name="Hell1", endpoint="test1", category="test"))
dnsv1.add_url_rule('/', 'index', index)
dnsv1.add_url_rule('/test', 'test', test)

