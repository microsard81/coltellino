from flask import flash
from flask import current_app
from flask import Blueprint, request
from flask import  render_template, request, redirect, url_for

from dispatch import app
from dispatch.backends.ldap_auth import LoginForm, User
from flask.ext.login import login_user, UserMixin, login_required, logout_user, current_user


authv1 = Blueprint(__name__, __name__)


@authv1.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User(name=form.username.data, passwd=form.password.data)
        if user.active is not False:
            res = login_user(user, remember = True)
            flash("Logged in successfully.")
            # print app.url_map
            return redirect(request.args.get('next') or url_for("recordsview.index_view"))
        flash('Invalid user')
    return render_template("login.html", form=form)


@authv1.route("/logout", methods=["GET", "POST"])
@login_required
def logout():
    logout_user()
    flash("Logged out successfully.")
    return redirect("/login")