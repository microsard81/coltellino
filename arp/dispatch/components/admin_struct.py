import datetime
import json

from flask import flash, session, current_app
from wtforms.validators import ValidationError

from dispatch import admin, db, ldap

from flask.ext import login
from flask.ext.login import current_user
from flask.ext.admin.actions import action
from flask.ext.admin import BaseView, expose
from flask.ext.admin.contrib.sqla import ModelView
from flask.ext.admin.contrib.sqla.tools import get_query_for_ids
from flask.ext.admin.babel import gettext, ngettext, lazy_gettext
from wtforms.fields import SelectField, HiddenField
from wtforms.validators import ValidationError
from sqlalchemy import or_, and_
from dispatch.lib.helpers import log_activity, purge_cache

from dispatch.models import Records, Domains

class MyCustomView(BaseView):
    @expose('/')
    def index(self):
        return self.render('/land/index.html')

    @expose('/pippo')
    def pippo(self):
        return self.render('/land/pippo.html')

    @expose('/test')
    def test(self):
        return self.render('/land/test.html')


class DomainsView(ModelView):

    list_template = 'admin/model/list_record.html'

    def no_duplicated_name(form, field):
        # dom = Domains.query.filter(Domains.name==form.domain.data).first()
        if 'id' in form:
            dom = Domains.query.filter(Domains.name==field.data)\
                                .filter(Domains.id!=form.id.data)\
                                .all()
        else:
            dom = Domains.query.filter(Domains.name==field.data).all()

        if dom:
            raise ValidationError('You cannot insert a duplicated record')


    column_exclude_list = ('notified_serial','account')
    form_excluded_columns = ('Records',)
    form_args = dict( name = dict(validators=[no_duplicated_name]))
    form_columns = ('id','name','master','last_check','type','notified_serial','account')
    form_overrides = dict(id=HiddenField)

    def get_create_form(self):
        form = self.scaffold_form()
        delattr(form, 'id')
        return form

    def is_accessible(self):
        return current_user.is_authenticated()

    def __init__(self, session, **kwargs):
        # You can pass name and other parameters if you want to
        super(DomainsView, self).__init__(Domains, session, **kwargs)

    @log_activity(action='[create/update dom]')
    def after_model_change(self, form, model, is_created):
        super(DomainsView, self).after_model_change(form, model, is_created)
        purge_cache(model.name)

    @log_activity(action='[delete dom]')
    def on_model_delete(self, model):
	super(DomainsView, self).on_model_delete(model)
        purge_cache(model.name)


class RecordsView(ModelView):
    def no_duplicated_name(form, field):

        dom = Domains.query.filter(Domains.name==form.domain.data).first()

        if form.type.data not in ( 'A', 'NS'):
            if 'id' in form:
                res = Records.query.filter(Records.type==form.type.data)\
                    .filter(Records.name==field.data)\
                    .filter(Records.domain==dom)\
                    .filter(Records.id!=form.id.data)\
                    .filter(Records.content!=form.content.data)\
                    .all()
            else:
                res = Records.query.filter(Records.type==form.type.data)\
                                .filter(Records.name==field.data)\
                                .filter(Records.domain==dom)\
                                .filter(Records.content!=form.content.data)\
                                .all()
            if res:
                raise ValidationError('You cannot insert a duplicated record')


    def hinfo_with_pattern(form, field):
        if form.type.data == 'HINFO':
            if form.content.data.count('"') != 4:
                raise ValidationError('HINFO field must be in this format:   "VM" "Ubuntu 14.04"')
            elif form.content.data.find('"') != 0 or form.content.data[::-1].find('"') != 0:
                raise ValidationError('HINFO field must be in this format:   "VM" "Ubuntu 14.04"')

    def no_space_in_name(form, field):
        if ' ' in form.name.data:
            raise ValidationError("This field can't contain spaces!")

    def no_space_in_content(form, field):
        if form.type.data in ('CNAME','PTR'):
            if ' ' in form.content.data:
                raise ValidationError("This field can't contain spaces!")

    def no_duplicated_content(form, field):

        dom = Domains.query.filter(Domains.name==form.domain.data).first()
        if 'id' in form:
            res = Records.query.filter(Records.content==field.data)\
                            .filter(Records.name==form.name.data)\
                            .filter(Records.type==form.type.data)\
                            .filter(Records.domain==dom)\
                            .filter(Records.id!=form.id.data)\
                            .all()
        else:
            res = Records.query.filter(Records.content==field.data)\
                            .filter(Records.name==form.name.data)\
                            .filter(Records.type==form.type.data)\
                            .filter(Records.domain==dom)\
                            .all()
        if res:
            raise ValidationError('You cannot insert a duplicated record')

    
    # form_choices = {'my_form_field': [
 #        ('db_value', 'display_value'),
 #    ]

    # Disable model creation
    # can_create = False

    # Override displayed fields
    # column_list = ('login', 'email')

    # form_overrides = dict(type=SelectField)


    create_template = "admin/model/create_record.html"
    edit_template = 'admin/model/edit_record.html'
    list_template = 'admin/model/list_record.html'

    form_args = dict( ttl = dict(label='TTL'),
                      name = dict(validators=[no_duplicated_name, no_space_in_name]),
                      content = dict(validators=[no_duplicated_content, hinfo_with_pattern, no_space_in_content]),

                      ) 

    column_labels = dict( ttl='TTL')

    column_exclude_list = ('change_date','ordername','auth')

    column_sortable_list = (('domain', Domains.name),
                            ('name', Records.name),
                            ('type', Records.type),
                            'content')

    column_searchable_list = ('content', 'name','type')

    column_filters = (Domains.name, 'content', 'name','type')
    # column_searchable_list = ('domain_id', 'name','type','content')

    # form_excluded_columns = ('ordername','auth')
    form_columns = ('id','domain','name','type','content','ttl','prio')
    

    form_choices = {'type': [
                     ('A', 'A'),
                     ('CNAME', 'CNAME'),
                     ('HINFO', 'HINFO'),
                     ('TXT', 'TXT'),
                     ('PTR', 'PTR'),
                     ('AAAA', 'AAAA'),
                     ('MX', 'MX'),
                     ('NS', 'NS'),
                     ('SRV', 'SRV'),
                     ('SOA', 'SOA')
    ]}

    page_size = 50

    form_overrides = dict(id=HiddenField)

    # def get_edit_form(self):
    #     form = self.scaffold_form()
    #     # delattr(form, 'id')
    #     print dir(form)
    #     return form


    def get_create_form(self):
        form = self.scaffold_form()
        delattr(form, 'id')
        return form


    @log_activity(action='[update SOA]')
    def update_soa_record(self, model):
        dom = model.domain
        soa = self.session.query(Records)\
                            .filter(Records.type == 'SOA')\
                            .filter(Records.domain == dom)\
                            .first()
        if soa:
            # soa_record = soa
            contents = soa.content.split(" ")
            today = datetime.datetime.now().strftime("%Y%m%d")
            counter = int(contents[2][-2:])
            counter += 1
            contents[2] = "%s%02d" % (today, counter)
            soa.content = " ".join(contents)
            self.session.commit()
            flash("%s SOA record updated" % dom.name)
        else:
            flash("You need a SOA record...")
           
    @log_activity(action='[create/update]')        
    def manage_ptr_field(self, model, is_created):
        if model.type == 'A':
            reverse_map = current_app.config['DOMAINS_REVERSE_MAP']
            reversed_domain = reverse_map.get(model.domain.name,None)
            if reversed_domain:
                rdom = self.session.query(Domains)\
                            .filter(Domains.name == reversed_domain).first()
                if is_created:
                    rname = model.name+"."
                    rrecord = self.session.query(Records)\
                                          .filter(Records.content == rname).first()
                    if not rrecord:                                          
                        new_record = Records()
                        new_record.type = "PTR"
                        new_record.content = rname
                        new_record.ttl = 86400
                        new_record.domain = rdom
                        try:
                            new_record.name = ".".join(model.content.split(".")[::-1])+".in-addr.arpa"
                            self.session.add(new_record)
                            self.session.commit()
                            flash("%s - (%s) was successfully %s" % (new_record.name,
                                                                    new_record.domain.name,
                                                                    "created" if is_created else "saved"))
                            self.update_soa_record(new_record)
                        except Exception as e:
                            flash("Something went wrong with your PTR record...")


    def __init__(self, session, **kwargs):
        # You can pass name and other parameters if you want to
        super(RecordsView, self).__init__(Records, session, **kwargs)


    @log_activity(action='[create/update]')
    def after_model_change(self, form, model, is_created):
        name = model.name
        if model.type.upper() in ['A','TXT','HINFO','PTR']:
            dom_name = model.domain.name
            if dom_name not in name:
                model.name = ".".join([name, dom_name]).strip()
                self.session.commit()
        super(RecordsView, self).after_model_change(form, model, is_created)
        self.update_soa_record(model)
        self.manage_ptr_field(model, is_created)
        purge_cache(name)
        flash("%s - (%s) was successfully %s" % (model.name, model.domain.name, "created" if is_created else "saved"))

    def on_model_delete(self, model):
        self.update_soa_record(model)
        name = model.name
        if model.type.upper() == 'A':
            # strictly related records
            res = self.session.query(Records)\
                              .filter(or_(Records.content == name, 
                                          Records.content == name+".")
                                        ).all()

            for record in res:
                try:
                    self.delete_model(record)
                except:
                    current_app.logger.error("already deleted!")

            # loosely related records
            res = self.session.query(Records)\
                              .filter(Records.name == name)\
                              .filter(Records.type != model.type)\
                              .all()

            for record in res:
                try:
                    self.delete_model(record)
                except:
                    current_app.logger.error("already deleted!")

        super(RecordsView, self).on_model_delete(model)
        flash("%s was successfully deleted." % model.name)


    @log_activity(action='[delete]')
    def delete_model(self, model):
        name = model.name
        super(RecordsView, self).delete_model(model)
        purge_cache(name)
    
    @action('delete',
            lazy_gettext('Delete'),
            lazy_gettext('Are you sure you want to delete selected models?'))
    def action_delete(self, ids):
        try:

            query = get_query_for_ids(self.get_query(), self.model, ids)

            if self.fast_mass_delete:
                count = query.delete(synchronize_session=False)
            else:
                count = 0

                qall = query.all()
                for m in qall:
                    try:
                        self.delete_model(m)
                    except:
                        current_app.logger.error("already deleted!")

                    # self.session.delete(m)
                    count += 1

            self.session.commit()

            flash(ngettext('Model was successfully deleted.',
                           '%(count)s models were successfully deleted.',
                           count,
                           count=count))
        except Exception as ex:
            if self._debug:
                raise

            flash(gettext('Failed to delete models. %(error)s', error=str(ex)), 'error')

    def is_accessible(self):
        # print session.logged_in
        return current_user.is_authenticated()



# @current_app.route('/get_first_useful_ip/<domain>')
# def get_first_useful_ip(domain):
# items = sorted(r, key=lambda k: "%3s.%3s.%3s.%3s" % tuple(k.content.split(".")))
#     response = {"status": "ok", "free": "192.168.0.1"}
#     return jsonify(**response)


def register():
    # admin.add_view(MyCustomView(name='Hello1', endpoint="test1", category="test"))
    # admin.add_view(MyCustomView(name='Hello2', endpoint="test2", category="test0"))
    # admin.add_view(MyCustomView(name='Hello3', endpoint="test3", category="test0"))
    admin.add_view(DomainsView(db.session))
    admin.add_view(RecordsView(db.session))

    
