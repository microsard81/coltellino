from dispatch import app
from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)



class Domains(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255),nullable=False )
    master = db.Column(db.String(20),nullable=False)
    last_check = db.Column(db.Integer())
    type = db.Column(db.String(255), nullable=False)
    notified_serial = db.Column(db.Integer())
    account = db.Column(db.String(40))
    def __repr__(self):
        return self.name


class Records(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    
    domain_id = db.Column(db.Integer(), db.ForeignKey('domains.id'))
    domain = db.relationship("Domains", backref=db.backref('Records', order_by=id))

    name = db.Column(db.String(255), nullable=False)
    type = db.Column(db.String(255), nullable=False)
    content = db.Column(db.String(255), nullable=False)
    ttl = db.Column(db.Integer(), default=86400, nullable=False)
    prio = db.Column(db.Integer(), default=0)
    change_date = db.Column(db.Integer())
    ordername = db.Column(db.String(255))
    auth = db.Column(db.Boolean())

# class RecordsWithIP(Records):
#     content = db.Column(db.IPAddress(), nullable=False)

