import argparse
from dispatch import app
#from sandman import app as sandman_app
from werkzeug.wsgi import DispatcherMiddleware
from werkzeug.serving import run_simple
#from sandman.model import activate

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--port', '-p', default = app.config['PORT'], type=int,
        help='port on which to run server')
    parser.add_argument(
        '--host', '-n', default = app.config['HOST'],
        help='where to run server')
    args = parser.parse_args()

    #sandman_app.debug = True
    #sandman_app.config['SQLALCHEMY_DATABASE_URI'] = app.config['SQLALCHEMY_DATABASE_URI']
    #sandman_app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = False
    #activate(browser=True)

    application = DispatcherMiddleware(app)

    #app.run(host=args.host, port=args.port, debug = app.config.get('DEBUG', True) )
    #application.run(host=args.host, port=args.port, debug = app.config.get('DEBUG', True) )
    run_simple(args.host, args.port, application, use_debugger = True, 
               use_reloader=True, use_evalex=True)

