Note
====

* serve che esista un file /etc/sudoers.d/pdns con dentro qualcosa di simile

Defaults:achmed !requiretty

achmed  ALL = (root) NOPASSWD: /usr/bin/pdns_control
%www-data       ALL = (root) NOPASSWD: /usr/bin/pdns_control



* /home/achmed/pdns/dispatch/log/activity.log e' il log di quello che viene fatto e chi lo fa

* l'accesso e' vincolato ai soli utenti che figurano nel ramo LDAP 

cn=dns,ou=alt_groups,dc=crs4

* nel file /home/achmed/pdns/dispatch/settings.py sono specificati i parametri di configurazione
necessari al funzionamento del portale.



Limitazioni
=============

L'interfaccia e' stata limitata al controllo dei domini e dei record dns, non tutti i campi del database sono stati esposti perche' 
sinora sono risultati vuoti.

Comportamenti automatici
========================

1 - Inserire / modificare / cancellare qualcosa ( record o dominio ) comporta:
	* una registrazione di log nel file sopracitato
	* modifica automatica del relativo record SOA e quindi autoincremento dello stesso
	* cancellazione cache del demone powerdns

2 - Creare / modificare un record comporta
	* quanto detto al punto 1
	* se l'azione e' la creazione di un A record avverra' la creazione del record PTR associato

3 - Cancellare un record comporta
	* quanto detto al punto 1
	* cancellazione automatica di eventuali record correlati, in soldoni cancellare un A record vuol dire cancellare anche eventuali TXT HINFO ...
	* cancellazione automatica di record PTR associato





