# AUTHOR: Luca Carta

__author__  = "Luca Carta - CRS4 - luca@crs4.it"
__date__    = "2015/06/08"
__version__ = "0.2"
__comment__ = "Script per l'esecuzione di vari comandi sui Cisco"

import pexpect
from collections import OrderedDict
from numpy import arange

def connetti(username, password, secret, device, prompt):	
	try:
		ssh = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "
		child = pexpect.spawn(ssh + username + "@" + device)
		try:
			child.expect(username + "@" + device + "\'s password:", timeout=3)
			child.sendline(password)
		except:
			child.expect("Password: ", timeout=3)
			child.sendline(password)
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password: ")
		child.sendline(secret)
		child.expect(prompt + "#")
		child.sendline("ter len 0")
		child.expect(prompt + "#")
		return (child, prompt)
		#
	except Exception, e:
		print "Error in connetti(): %s" % str(e)

def esegui(chi, comando):
	try:
		child = chi[0]
		prompt = chi[1]
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		child.sendline(comando)
		child.expect(prompt + "#", timeout=240)
		da_loggare = str(child.before)
		return da_loggare.split("\r\n")[1:]
	except Exception, e:
		print "Error in esegui(): %s" % str(e)
		return {"error": str(error)}

def chiudi(chi):
	try:
		child = chi[0]
		prompt = chi[1]
		child.sendline("exit")
		child.close()
	except Exception, e:
		print "Error in chiudi(): %s" % str(e)


def vlanPortDetail(vlan):
	try:
		ports = OrderedDict({})
		#
		for i in vlan:
			unt = []
			tag = []
			u = False
			t = False	
			for s in vlan[i]:
				if s and s[0] == "U":	
					u = True
					t = False
				elif s and s[0] == "T":	
					t = True
					u = False
				#
				if u:
					_id = s.replace("U","").split(",")
					for l in _id:
						if l and len(l.split("-")) == 1:
							unt.append(int(l))
						elif l:
							ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
							for el in ran:
								unt.append(el)
				if t:
					_id = s.replace("T","").split(",")
					for l in _id:
						if l and len(l.split("-")) == 1:
							tag.append(int(l))
						elif l:
							ran = range(int(l.split("-")[0]), int(l.split("-")[1])+1)
							for el in ran:
								tag.append(el)
			ports[i] = {"untagged": unt, "tagged": tag}
		return ports
	except Exception, error:
		print "Error in vlanPortDetail(): %s" % str(error)


def getPortVlan(chi, _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _filter:
			b = esegui(chi, "sh ip interf brief " + _filter)	
		else:
			b = esegui(chi, "sh ip interf brief")
		#
		vlan = OrderedDict({})
		b = [ x for x in b if x ]
		#
		for i in b:
			if (i) and (i.split()[0] != "Interface") and ("Vlan" not in i.split()[0]) and ("Mana" not in i.split()[0]):
				a =  esegui(chi,'sh int '+i.split()[0]+" switch")
				_add = []
				trunk = False
				access = False
				for z in a:
					if z and "Administrative Mode: trunk" in z:
						trunk = True
					elif z and "Administrative Mode: static access" in z:
						access = True
					if z and trunk:
						if "Trunking Native Mode VLAN:" in z:
							_add.append("U"+z.split()[4])
						elif "Trunking VLANs Enabled: " in z:
							_add.append("T"+z.split()[3])
					if z and access:
						if "Access Mode VLAN:" in z:
							print z.split()
							_add.append("U"+z.split()[3])
				#
				vlan[i.split()[0]] = _add
		#
		ports = vlanPortDetail(vlan)
		return ports
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}

def getPorts(chi, _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _filter:
			b = esegui(chi, "sh ip interf brief | " + _filter)	
		else:
			b = esegui(chi, "sh ip interf brief")
		#
		b = [ x for x in b if x ]
		ports = []
		for i in b:
			if i and i.split()[0] != "Interface" and "Vlan" not in i.split()[0] and "Mana" not in i.split()[0]:
				ports.append(i.split()[0])
		#
		return ports
	except Exception, error:
		print "Error in getPortVlan(): %s" % str(error)
		return {"error": str(error)}

def getPortsAndDesc(chi, _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		ports = OrderedDict({})
		#
		if _filter:
			b = esegui(chi, "sh interf " + _filter)
			b = [ x for x in b if x ]
			for i in b:
				if i and "Description: " in i:
					ports[_filter] = i.split("Description: ")[1]	
		else:
			b = esegui(chi, "sh interf desc")
			b = [ x for x in b if x ]
			for i in b:
				if i and "Interface" not in i.split()[0] and "Vl" not in i.split()[0]:
					ports[i.split()[0]] = i.split("  ")[-1].strip()
		#
		#
		return ports
	except Exception, error:
		print "Error in getPortsAndDesc(): %s" % str(error)
		return {"error": str(error)}

def getMacAddress(chi, _mac='', _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _mac and _filter:
			_mval = ".".join(_mac.replace(":","")[s:s+4] for s in range(0, len(_mac.replace(":","")), 4)).lower()
			b = esegui(chi, "show mac address-table address " + _mval + " | " + _filter)
		elif _filter:
			b = esegui(chi, "show mac address-table | " + _filter)
		elif _mac:
			_mval = ".".join(_mac.replace(":","")[s:s+4] for s in range(0, len(_mac.replace(":","")), 4)).lower()
			b = esegui(chi, "show mac address-table address " + _mval)	
		else:
			b = esegui(chi, "show mac address-table")
		#
		mac = {}
		b = [ x for x in b if x ]
		for i in b:
			if i.split()[0].isdigit():	
				mvalue = ":".join(i.split()[1].replace(".","")[s:s+2] for s in range(0, len(i.split()[1].replace(".","")), 2)).upper()
				mac[mvalue] = { "vlan": i.split()[0], 
								"port": i.split()[-1] }
		#
		return mac
	except Exception, error:
		print "Error in getMacAddress(): %s" % str(error)
		return { "error": str(error) }

def getArp(chi, _filter=''):
	try:
		if chi[0].closed:
			raise Exception("Session closed. Please, reconnect!")
		if _filter:
			b = esegui(chi, "sh arp | " + _filter)	
		else:
			b = esegui(chi, "sh arp")
		#
		arp = OrderedDict({})
		b = [ x for x in b if x ]
		for i in b:
			if len(i.split()[3]) == 14 :	
				mvalue = ":".join(i.split()[3].replace(".","")[s:s+2] for s in range(0, len(i.split()[3].replace(".","")), 2)).upper()
				mac = getMacAddress(chi, mvalue)
				print mac
				arp[i.split()[1]] = { "port": mac[mvalue]['port'],
									  "mac": mvalue }
		#
		return arp
	except Exception, error:
		print "Error in getArp(): %s" % str(error)
		return { "error": str(error) }


def setPortsDesc(chi, _port='', _desc=''):
	try:
		child = chi[0]
		prompt = chi[1]
		#
		if child.closed:
			raise Exception("Session closed. Please, reconnect!")
		if not _port:
			raise Exception("No port selected. Please, specify port!")
		#
		child.sendline("conf t")
		child.expect("#")
		child.sendline("interf " + _port)
		child.expect("#")
		child.sendline("description " + _desc)
		child.expect("#")
		child.sendline("end")
		child.expect(prompt + "#")
		#
		ports = getPortsAndDesc(chi, _port)
		for i in ports:
			ports[i]= (ports[i], "Done")
		#
		return ports
	except Exception, error:
		print "Error in setPortsDesc(): %s" % str(error)
		return {"error": str(error)}
