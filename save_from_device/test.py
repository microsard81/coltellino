import pexpect

utente = "sorveglione"
password = "v1gil4"
secret = "Qelpd&10"
prompt = "SSH@Genotyping"
ip = "10.0.0.25"

child = pexpect.spawn("ssh " + utente + "@" + ip)
child.expect(utente + "@" + ip + "\'s password: ")
child.sendline(password)
child.expect(prompt + ">")
child.sendline("enable")
child.expect("Password:")
child.sendline(secret)
child.expect(prompt + "#")
child.sendline("sh run")
da_loggare = ""
try:
	z = 0
	while z == 0:
		child.expect("--More--, next page: Space, next line: Return key, quit: Control-c", timeout=5)
		da_loggare = da_loggare + str(child.before)
		child.sendline(" ")
except:
	child.expect(prompt + "#")
	da_loggare = da_loggare + str(child.before)


da_loggare = da_loggare.replace("sh run","")

print da_loggare
