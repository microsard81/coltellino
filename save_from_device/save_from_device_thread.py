#!/root/Envs/ARP/bin/python
# -*- coding: utf-8 -*-
# AUTHOR: Luca Carta

__author__= "Luca Carta - CRS4 - luca@crs4.it"
__date__ = "2016/06/29"
__comment__= "Script per la copia delle configurazioni degli apparati"
__version__= "1.4.2"

from bs4 import BeautifulSoup
import os
import re
import sys
import time
import pexpect
import getopt
import requests
import ssl
import cookielib
import subprocess
import threading
import smtplib
from lxml import etree
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template
from Queue import Queue



file2log     = "/var/log/save_from_switch.log"
templateFile = "/root/scripts/save_from_device/template.html"
delimitatore = "-----------------------------------------------------------------------------------------------------\n"
esegui       = ""

STRINGA = """
Error, Please specify all parameters 
---------------------------------------------------------
\'-f\' or \'--filename\' - xml filename
---------------------------------------------------------
"""

def leggiFileXML(log_file):
	try:
		fXML=file(log_file, "r+")
		contenuto = fXML.read()
		return contenuto
	except Exception, e:
		sys.exit("Errore in leggiFileXML: " + str(e))

def leggiLog():
	try:
		return file(file2log, "r+").read()
	except Exception, e:
		sys.exit("Errore in leggiLog: " + str(e))

def writeLog(stringalog, mode="a"):
	# Funzione per la scrittura di un LOG File
	try:
		reLog=file(file2log, mode)
		reLog.write(str(stringalog)+"\n")
		reLog.close()
	except Exception, err:
		sys.exit("Errore in writeLog: " + str(err))

def salvaRunning(stringa2log, log_file, modo="w"):
	# Funzione per la scrittura del Running-Config
	try:
		fLog=file(log_file, modo)
		fLog.write(str(stringa2log))
		fLog.close()
	except Exception, err:
		sys.exit("Errore in salvaRunning: " + str(err))

def salvaBIN(bin2log, log_file, modo="w"):
	# Funzione per la scrittura di un File BIN
	try:
		fLog=file(log_file, modo)
		fLog.write(bin2log)
		fLog.close()
	except Exception, err:
		sys.exit("Errore in salvaBIN: " + str(err))

def sendMail(subject, h_text, s_text, toUser = ['netop@crs4.it','lcarta.mail@gmail.com']):
	# Funzione per l'invio di una email
	#
	try:
		fromUser = "cacti@crs4.it"
		fromText = "Cacti <cacti@crs4.it>"
		msg = MIMEMultipart('alternative')
		msg['Subject'] = subject
		msg['From'] = fromText
		msg['To'] = ", ".join(toUser)
		part1 = MIMEText(s_text, 'plain')
		part2 = MIMEText(h_text, 'html')
		msg.attach(part1)
		msg.attach(part2)
		server = smtplib.SMTP("mail.crs4.it")
		server.sendmail(fromUser, toUser, msg.as_string())
		server.quit()
	except Exception, e:
		sys.exit("ERRORE in sendMail: %s" % str(e))

def raggiungibile(apparato):
	# Funzione per la verifica della raggiungibilita' di un apparato
	try:
		pipe = subprocess.Popen("ping " + apparato + " -c 1", shell=True, stdout=subprocess.PIPE).stdout
		ok = False
		for i in pipe.readlines():
			if i.find(", 0% packet loss,") != -1: 
				ok = True
		if ok: return True
		else: return False
	except Exception, e:
		print "ERRORE in raggiungibile: %s" % str(e)
		
def salvaSuPfSense2(device, www_user, www_password, cartella_file, runn_file, prompt, q=None):
	try:
		# For Debug ONLY
		#
		# print device
		# print www_user
		# print www_password
		# print prompt
		# print cartella_file
		# print runn_file
		#
		# FIELDS:
		#
		# backuparea=
		# donotbackuprrd=on
		# encrypt_password=
		# encrypt_passconf=
		# Submit=Download+configuration
		# restorearea=
		# conffile=
		# decrypt_password=
		# decrypt_passconf=
		#
		#
		if not raggiungibile(device):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		www = ""
		#
		url = "https://"+device+"/diag_backup.php"
		client = requests.session()
		l = client.get(url, verify=False)
		data = {}
		data['backuparea'] = ""
		data['Submit'] = "Download configuration"
		data['restorearea'] = ""
		data['conffile'] = ""
		data['donotbackuprrd'] = "on"
		data['encrypt_password'] = ""
		data['encrypt_passconf'] = ""
		data['decrypt_password'] = ""
		data['decrypt_passconf'] = ""
		#
		login = 'Login'
		#
		page = BeautifulSoup(l.text)
		__csrf_magic = page.find('input', attrs = {'name':'__csrf_magic'})['value']
		login_data = { 'usernamefld' : www_user, 'passwordfld' : www_password, 'login': login, '__csrf_magic': __csrf_magic }
		r = client.post(url, data=login_data, headers=dict(Referer=url), verify=False)
		f = client.get(url, headers=dict(Referer=url), verify=False)
		__csrf_magic1 = BeautifulSoup(f.text).find('input', attrs = {'name':'__csrf_magic'})['value']
		data['__csrf_magic'] = __csrf_magic1
		z = client.post(url, data=data, headers=dict(Referer=url), verify=False)
		www = z.text
		client.close()
		salvaRunning(www, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception,e:
		q.put('Errore nella sottofunzione salvaSuPfSense2: %s. Su %s' % (str(e), device))

def salvaSuWAP200(device, www_user, www_password, prompt, cartella_file, runn_file, q=None):
	try:
		if not raggiungibile(device):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		conf_file = ""
		#
		# For Debug ONLY
		#
		# print device
		# print www_user
		# print www_password
		# print prompt
		# print cartella_file
		# print runn_file
		#
		#
		url = "http://"+device+"/config.bin"
		client = requests.session()
		l = client.get(url, auth=(www_user, www_password))
		conf_file = l.content
		client.close()
		#
		salvaBIN(conf_file, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception,e:
		q.put('Errore nella sottofunzione salvaSuWAP200: %s. Su %s' % (str(e), device))

def salvaSuHP(ip, prompt, cartella_file, runn_file, livello, protocollo, utente, password, modello, secret, q=None):
	try:
		#
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		da_loggare = ""
		rimpiazza = ['',
				 'Running configuration:',
				 'sh run',
				 prompt + '#',
				 '1H',
				 '19H',
				 '[24;',
				 '[?25h',
				 '18H',
				 '0HE',
				 '24H',
				 '[2K',
				 '[1;24r',
				 '20H',
				 '26H',
				 '13H',
				 '\r']
		if protocollo == "ssh" and livello == "2":
			child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
			child.expect(utente + "@" + ip + "\'s password: ")
			child.sendline(password)
			child.expect("Press any key to continue", timeout=180)
			child.sendline("z")
			child.expect(prompt + ">")
			child.sendline("enable")
			child.expect("Password:")
			child.sendline(secret)
		elif protocollo == "ssh" and livello == "1":
			child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
			child.expect(utente + "@" + ip + "\'s password: ")
			child.sendline(password)
			child.expect("Press any key to continue", timeout=180)
			child.sendline("z")
		elif protocollo == "telnet" and livello == "1" and modello == "2800":
			#print "Sono sotto Telnet 1"
			child = pexpect.spawn("telnet " + ip)
			child.expect("Username:")
			child.sendline(utente)
			child.expect("Password:")
			child.sendline(password)
		elif protocollo == "telnet" and livello == "2" and modello == "2626":
			child = pexpect.spawn("telnet " + ip)
			child.expect("Press any key to continue", timeout=60)
			child.sendline(" ")
			child.expect("Please Enter Login Name:", timeout=60)
			child.sendline(utente)
			child.sendline(password)
			child.expect(prompt + ">", timeout=5)
			child.sendline("enable")
			child.expect("Password:", timeout=5)
			child.sendline(secret)
		child.expect(prompt + "#")
		child.sendline("no page")
		child.expect(prompt + "#")
		child.sendline("sh run")
		#
		da_loggare = ""
		child.expect(prompt + "#")
		da_loggare = da_loggare + str(child.before)
		#
		child.sendline("write memory")
		child.expect(prompt + "#")
		child.sendline("exit")
		child.expect(prompt + ">")
		child.sendline("exit")
		child.expect("Do you want to log out [y/n]?")
		child.sendline("y")
		child.close()
		#
		# Pulizia da caratteri indesiderati
		#
		for element in rimpiazza:
			da_loggare = da_loggare.replace(element,"")
		#
		salvaRunning(da_loggare, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		q.put("Errore su salvaSuHP: " + str(errore))
	  
def salvaSuBrocade(ip, prompt, secret, cartella_file, runn_file, livello, more, protocollo, utente, password, prompt_ver, q=None):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		#
		if protocollo == "ssh":
			child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
			if prompt_ver == 2:	
				child.expect("Password:")
			else:	
				child.expect(utente + "@" + ip + "\'s password: ")
			child.sendline(password)
		elif protocollo == "telnet" and livello == "1":
			child = pexpect.spawn("telnet " + ip)
		elif protocollo == "telnet" and livello == "2":
			child = pexpect.spawn("telnet " + ip)
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password:")
		child.sendline(secret)
		child.expect(prompt + "#")
		da_loggare = ""
		if more:
			child.sendline("ter len 0")
			child.expect(prompt + "#")
			child.sendline("sh run")
			#
			child.expect(prompt + "#")
			#
		else:
			child.sendline("skip-page-display")
			child.expect(prompt + "#")
			child.sendline("sh run")
			#
			#
			child.expect(prompt + "#")
			da_loggare = da_loggare + str(child.before)
		#
		child.sendline("write memory")
		child.expect(prompt + "#")
		child.sendline("exit")
		child.expect(prompt + ">")
		child.sendline("exit")
		child.close()
		da_loggare = da_loggare.replace("sh run\r\n","").replace("[7m[0m[2K","").replace("","").replace("\r","")
		salvaRunning(da_loggare, cartella_file + runn_file)	
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		q.put("Errore su salvaSuBrocade: " + str(errore))

def salvaSuMDS(ip, prompt, cartella_file, runn_file, no_more, utente, password, q=None):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		# Are you sure you want to continue connecting (yes/no)?
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
		child.expect("Password:")
		child.sendline(password)
		#
		child.expect(prompt + "#")
		if no_more:	child.sendline("ter len 0")
		child.expect(prompt + "#")
		child.sendline("sh run")
		#
		child.expect(prompt + "#")
		da_loggare = str(child.before).replace("sh run","").replace("[7m[0m[2K","").replace("","").replace("\r","")
		salvaRunning(da_loggare, cartella_file + runn_file)
		child.sendline("exit")
		child.close()
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		q.put("Errore su salvaSuMDS: " + str(errore))

def salvaSuCatalystSSH(ip, prompt, cartella_file, runn_file, no_more, utente, password, secret, q=None):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		# Are you sure you want to continue connecting (yes/no)?
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
		child.expect("Password: ")
		child.sendline(password)
		#
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password: ")
		child.sendline(secret)
		#
		child.expect(prompt + "#")
		if no_more:	child.sendline("ter len 0")
		child.expect(prompt + "#")
		child.sendline("sh run")
		#
		child.expect(prompt + "#")
		da_loggare = str(child.before).replace("sh run","").replace("[7m[0m[2K","").replace("","").replace("\r","")
		salvaRunning(da_loggare, cartella_file + runn_file)
		child.sendline("exit")
		child.close()
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		q.put("Errore su salvaSuMDS: " + str(errore))

def salvaSuCatalystTelnet(ip, prompt, cartella_file, runn_file, no_more, utente, password, secret, q=None):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		# Are you sure you want to continue connecting (yes/no)?
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("telnet " + ip)
		child.expect("Username: ")
		child.sendline(utente)
		child.expect("Password: ")
		child.sendline(password)
		#
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password: ")
		child.sendline(secret)
		#
		child.expect(prompt + "#")
		if no_more:	child.sendline("ter len 0")
		child.expect(prompt + "#")
		child.sendline("sh run")
		#
		child.expect(prompt + "#")
		da_loggare = str(child.before).replace("sh run","").replace("[7m[0m[2K","").replace("","").replace("\r","")
		salvaRunning(da_loggare, cartella_file + runn_file)
		child.sendline("exit")
		child.close()
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		q.put("Errore su salvaSuMDS: " + str(errore))

def salvaSuForce10(ip, prompt, secret, cartella_file, runn_file, no_more, protocollo, utente, password, modello="S50", q=None):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("telnet " + ip)
		if modello == "C300" or modello == "C300-TFTP":	child.expect("Login: ")
		elif modello == "S50":	child.expect("User:")
		child.sendline(utente)
		child.expect("Password:")
		child.sendline(password)
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password:")
		child.sendline(secret)
		child.expect(prompt + "#")
		if no_more:	child.sendline("ter len 0")
		child.expect(prompt + "#")
		if not modello == "C300-TFTP": 
			child.sendline("sh run")
		#
			child.expect(prompt + "#")
			da_loggare = str(child.before).replace("sh run","").replace("Current Configuration ...","")
			salvaRunning(da_loggare.replace("[7m[0m[2K","").replace("","").replace("\r","").replace("show run", ""), cartella_file + runn_file)
		else:
			to_send = "copy startup-config tftp://156.148.66.100/" + cartella_file.split("/")[-2] + "/" + runn_file
			child.sendline(to_send)
			child.expect(prompt + "#")
			da_loggare = str(child.before).replace(to_send,"")
			print da_loggare
		if modello =="C300":
			child.sendline("write memory")
			child.expect(prompt + "#")
			child.sendline("exit")
		elif modello =="S50":
			child.sendline("write memory")
			child.expect("(y/n)")
			child.sendline("y")
			child.expect(prompt + "#")
			child.sendline("quit")
		child.close()
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		q.put("Errore su salvaSuForce10: " + str(errore))

def salvaSuExtreme(utente, password, ip, prompt, cartella_file, runn_file, protocollo = 'telnet', q=None):
	try:
		if protocollo == 'telnet':
			if not raggiungibile(ip):
				writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
				writeLog("-----------------------------------------------------------------------------------------------------")
				return
			writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
			child = pexpect.spawn("telnet " + ip)
			child.expect("login:")
			child.sendline(utente)
			child.expect("password:")
			child.sendline(password)
			child.expect(prompt + ":1 #")
			child.sendline("show configuration")
			da_loggare = ""
			#
			try:
				z = 0
				while z == 0:
					child.expect("Press <SPACE> to continue or <Q> to quit:", timeout=5)
					da_loggare = da_loggare + str(child.before)
					child.sendline(" ")
			except:
				child.expect(prompt + ":")
				da_loggare = da_loggare + str(child.before)
			child.sendline("save")
			child.expect("database?")
			child.sendline("yes")
			child.expect((prompt + ":"), timeout=180)
			child.sendline("exit")
			child.close()
			salvaRunning(da_loggare.replace("show configuration", "").replace("[7m[0m[2K","").replace("","").replace("\r",""), cartella_file + runn_file)
			writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
			writeLog("-----------------------------------------------------------------------------------------------------")
		else:
			if not raggiungibile(ip):
				writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
				writeLog("-----------------------------------------------------------------------------------------------------")
				return
			writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
			child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
			try:
				child.expect("Are you sure you want to continue connecting (yes/no)?", timeout=10)
				child.sendline("yes")
			except:
				pass
			child.expect("Enter password for " + utente + ":")
			child.sendline(password)
			child.expect(prompt + ".1 #")
			child.sendline("disable clipaging")
			child.expect(prompt + ".2 #")
			child.sendline("show configuration")
			da_loggare = ""
			#
			#
			child.expect(prompt + ".3 #")
			da_loggare = da_loggare + str(child.before)
			child.sendline("save")
			child.expect("(y/N)")
			child.sendline("y")
			child.expect(prompt + ".4 #")
			child.sendline("exit")
			child.close()
			da_loggare = da_loggare.replace("show configuration", "").replace("[7m[0m[2K",
										  "").replace("",
										  "").replace("[7m",
										  "").replace("[m",
										  "").replace("[60;D",
										  "").replace("[K",
										  "").replace("\r",
										  "")
			salvaRunning(da_loggare, cartella_file + runn_file)
			writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
			writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		q.put("Errore su salvaSuExtreme: " + str(errore))

def salvaSuH3C(utente, password, secret, ip, prompt, cartella_file, runn_file, q=None):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
		child.expect(utente + "@" + ip + "\'s password: ")
		child.sendline(password)
		child.expect("<" + prompt + ">")
		child.sendline("super")
		child.expect(" Password:")
		child.sendline(secret)
		child.expect("<" + prompt + ">")
		child.sendline("screen-length disable")
		child.expect("<" + prompt + ">")
		child.sendline("display current-configuration")
		da_loggare = ""
		#
		child.expect("<" + prompt + ">")
		da_loggare = da_loggare + str(child.before)
		child.sendline("save")
		child.expect("Y/N")
		child.sendline("y")
		child.expect("enter key")
		child.sendline()
		child.expect("Y/N")
		child.sendline("y")
		child.expect("<" + prompt + ">")		
		child.sendline("exit")
		child.close()
		da_loggare = da_loggare.replace("display current-configuration", "").replace("[7m[0m[2K",
								      "").replace("",
								      "").replace("[7m",
								      "").replace("[m",
								      "").replace("[60;D",
								      "").replace("[K",
								      "").replace("\r",
								      "")
		salvaRunning(da_loggare, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		q.put("Errore su salvaSuH3C: " + str(errore))

def verificaArgomentiPassati(opzioni_short = ['f'], opzioni_long = ['filename']):
	opzioni = ""
	long_opzioni = []
	try:
		for i in opzioni_short:
			opzioni = opzioni + str(i) + ":"
		for i in opzioni_long:
			long_opzioni.append(str(i) + "=")
		try:
			campi, args = getopt.getopt(sys.argv[1:], opzioni, long_opzioni)
		except getopt.GetoptError, err:
			sys.exit(STRINGA+str(err))
		#
		if len(campi) == len(opzioni_short):
			for attributo, value in campi:
				if attributo in ('-f','--filename'):	filename = value
			return filename
		else:
			sys.exit(STRINGA + "Attenzione - Specificare tutti i parametri")
	except Exception, e:
		print "ERRORE in verificaArgomentiPassati: %s" % str(e)

def qStat(q):
	r_queue = []
	count = 0
	#
	while True:
		if q.qsize():
			item = q.get()
			r_queue.append(item)
		else:
			break
	#
	return r_queue

def main():
	try:
	  writeLog("-----------------------------------------------------------------------------------------------------", "w")
	  writeLog("   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ New Attemp at %s @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   " % time.strftime('%d/%m/%Y %H:%M'))
	  writeLog("-----------------------------------------------------------------------------------------------------")
	  #
	  esegui = ""
	  #
	  mail = file(templateFile, "r").read()
	  s = Template(mail)
	  #
	  #
	  filename = verificaArgomentiPassati()
	  xml = leggiFileXML(filename)
	  #
	  stampa = etree.XML(xml.replace('\n','').replace("&","ECOMM"))
	  stampa = stampa.getchildren()
	  #
	  device = []
	  commitDir = []
	  #
	  for i in stampa:
		  iter = i.getiterator()
		  # print iter
		  dizionario = {}
		  for j in iter:
			if j.tag != 'entry':
				dizionario[j.tag] = j.text.replace("ECOMM","&")
		  device.append(dizionario)
	  # 
	  # print device
	  #
	  q = Queue()
	  #
	  thread_l = [] 
	  #
	  for valore in device:
		#
			if valore['protocol'] in ('telnet','ssh', 'www'):
				if valore['device'].find("Brocade") != -1:
					if valore['ter_len'] == "yes":  more = True
					elif valore['ter_len'] == "no": more = False
					_prompt_ver = 1
					if valore['device'].find("ICX") != -1: _prompt_ver = 2
					if valore['user'] != "":
						thread_l.append(threading.Thread(target=salvaSuBrocade, name=valore['prompt'], args=(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   valore['auth_level'],
								   more,
								   valore['protocol'],
								   valore['user'],
								   valore['password'],
								   _prompt_ver,
								   q,)))
					else:
						thread_l.append(threading.Thread(target=salvaSuBrocade, name=valore['prompt'], args=(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   valore['auth_level'],
								   more,
								   valore['protocol'],
								   "test",
								   "test",
								   _prompt_ver,
								   q,)))
					#
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					#
				elif valore['device'].find("H3C") != -1:
					thread_l.append(threading.Thread(target=salvaSuH3C, name=valore['prompt'], args=(valore['user'],
							   valore['password'],
							   valore['secret'],
							   valore['ip'],
							   valore['prompt'], 
							   valore['file_dir'],
							   valore['running_file'],
							   q,)))
					#
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					#
				elif valore['device'].find("Extreme") != -1:
					thread_l.append(threading.Thread(target=salvaSuExtreme, name=valore['prompt'], args=(valore['user'],
							   valore['password'],
							   valore['ip'],
							   valore['prompt'], 
							   valore['file_dir'],
							   valore['running_file'],
							   valore['protocol'],
							   q,)))
					#
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					#
				elif valore['device'].find("pfSense2") != -1:
					thread_l.append(threading.Thread(target=salvaSuPfSense2, name=valore['hostname'], args=(valore['ip'],
							   valore['user'],
							   valore['password'],
							   valore['file_dir'],
							   valore['running_file'],
							   valore['hostname'],
							   q,)))
					#
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					#
				elif valore['device'].find("pfSense") != -1:
					thread_l.append(threading.Thread(target=salvaSuPfSense, name=valore['hostname'], args=(valore['ip'],
							   valore['user'],
							   valore['password'],
							   valore['file_dir'],
							   valore['running_file'],
							   valore['hostname'],
							   q,)))
					#
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					#
				elif valore['device'].find("Force10") != -1:
					if valore['ter_len'] == "yes":  more = True
					elif valore['ter_len'] == "no": more = False
					# 
					if valore['device'].find("C300") != -1:
						thread_l.append(threading.Thread(target=salvaSuForce10, name=valore['prompt'], args=(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   more,
								   valore['protocol'],
								   valore['user'],
								   valore['password'],
								   "C300",
								   q,)))
					elif valore['device'].find("TFTP") != -1:
						thread_l.append(threading.Thread(target=salvaSuForce10, name=valore['prompt'], args=(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   more,
								   valore['protocol'],
								   valore['user'],
								   valore['password'],
								   "C300-TFTP",
								   q,)))
					else:
						thread_l.append(threading.Thread(target=salvaSuForce10, name=valore['prompt'], args=(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   more,
								   valore['protocol'],
								   valore['user'],
								   "",
								   q,)))
					#
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					#
				elif valore['device'].find("Cisco") != -1:
					if valore['ter_len'] == "yes":  more = True
					elif valore['ter_len'] == "no": more = False
					# 
					if valore['device'].find("MDS") != -1:
						thread_l.append(threading.Thread(target=salvaSuMDS, name=valore['prompt'], args=(valore['ip'],
							   valore['prompt'],
							   valore['file_dir'],
							   valore['running_file'],
							   more, 
							   valore['user'],
							   valore['password'],
							   q,)))
					if valore['device'].find("Catalyst") != -1 and valore['protocol'] == "ssh":
						thread_l.append(threading.Thread(target=salvaSuCatalystSSH, name=valore['prompt'], args=(valore['ip'],
								   valore['prompt'],
								   valore['file_dir'],
								   valore['running_file'],
								   more, 
								   valore['user'],
								   valore['password'],
								   valore['secret'],
								   q,)))
					if valore['device'].find("Catalyst") != -1 and valore['protocol'] == "telnet":
						thread_l.append(threading.Thread(target=salvaSuCatalystTelnet, name=valore['prompt'], args=(valore['ip'],
								   valore['prompt'],
								   valore['file_dir'],
								   valore['running_file'],
								   more, 
								   valore['user'],
								   valore['password'],
								   valore['secret'],
								   q,)))
					if valore['device'].find("WAP200") != -1:
						thread_l.append(threading.Thread(target=salvaSuWAP200, name=valore['prompt'], args=(valore['ip'],
							   valore['user'],
							   valore['password'],
							   valore['prompt'],
							   valore['file_dir'],
							   valore['running_file'],
							   q,)))
					#
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					#
				elif valore['device'].find("HP") != -1:
					# 
					if valore['device'].find("2626") != -1 or \
					   valore['device'].find("2510") != -1:
					   	thread_l.append(threading.Thread(target=salvaSuHP, name=valore['prompt'], args=(valore['ip'],
							   valore['prompt'],
							   valore['file_dir'],
							   valore['running_file'],
							   valore['auth_level'],
							   valore['protocol'],
							   valore['user'],
							   valore['password'],
							   "2626",
							   valore['secret'],
							   q,)))
					if valore['device'].find("2848") != -1 or \
					   valore['device'].find("2824") != -1 or \
					   valore['device'].find("2828") != -1:
					   	thread_l.append(threading.Thread(target=salvaSuHP, name=valore['prompt'], args=(valore['ip'],
							   valore['prompt'],
							   valore['file_dir'],
							   valore['running_file'],
							   valore['auth_level'],
							   valore['protocol'],
							   valore['user'],
							   valore['password'],
							   "2824",
							   valore['secret'],
							   q,)))
					#
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					#
			else:
				s_text = "Problemi durante l'esecuzione del backup\n\n"
				status = "Devices backup problem"
				msg =""" 					<tr>
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							Problemi durante l'esecuzione del backup
						</td>
"""
				sendMail("[Cacti] - Backup Status", s.substitute(STATUS=str(status), MSG=str(msg), YEAR=str(time.strftime('%Y'))), s_text, ['luca@crs4.it'])
				sys.exit("Errore: definizione del protocollo errata")
	  #
	  for thread in thread_l:	thread.start() 
	  #
	  for thread in thread_l:	thread.join()
	  #
	  #
	  err = qStat(q)
	  if err:
	  	for i in err:
	  		writeLog(i)
	  #
	  for z in commitDir:
	  	  svn = "svn add " + z + "*"
		  esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
		  svn = "svn ci " + z + "* -m 'Copia configurazioni apparati'"
		  esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
	  #
	  writeLog(esegui)
	  #
	  in_mail = "-----------------------------------------------------------------------------------------------------\n"
	  error = leggiLog().split("\n")
	  #
	  r = 0
	  to_log = ""
	  colour1 = "#FFFFFF"
	  colour2 = "#E8E8E8"
	  #
	  for i in error:
		if r % 2:
			color = colour2
		else:
			color = colour1
		if i.find("Impossibile") != -1 or i.find("Error") != -1:
			in_mail = in_mail + str(i) + "\n"
			to_log += """					<tr style="background-color:%s;">
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							%s
						</td>
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							%s
						</td>
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							%s
						</td>
					</tr>
""" % (color, i.split(" - ")[0], i.split(" - ")[1].split(".")[0].replace("Impossibile salvare la configurazione di ",""), i.split(" - ")[1].split(".")[1])
			r += 1
	  msg = ""
	  if to_log != "":
		msg = """					<tr>
						<th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							Date
						</th>
						<th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							Host
						</th>
						<th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							Message
						</th>
					</tr>
%s""" % to_log
		status = "Devices backup problem"
	  else:
		msg = to_log
		status = "Devices backup done"
	  in_mail = in_mail + "-----------------------------------------------------------------------------------------------------"
	  #
	  #
	  #
	  #
	  s_text = "Backup Eseguiti.\n\n" + delimitatore + "\nErrori riscontrati:\n" + in_mail
	  sendMail("[Cacti] - Backup Status", s.substitute(STATUS=str(status), MSG=str(msg), YEAR=str(time.strftime('%Y'))), s_text)
	except Exception, errore:
	  mail = file(templateFile, "r").read()
	  msg =""" 					<tr>
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							%s
						</td>
""" % str(errore)
	  s_text = "Problemi durante l'esecuzione del backup\n\n" + str(errore)
	  status = "Devices backup problem"
	  s = Template(mail)
	  #
	  sendMail("[Cacti] - Backup Status", s.substitute(STATUS=str(status), MSG=str(msg), YEAR=str(time.strftime('%Y'))), s_text, ['luca@crs4.it'])
	  print "Errore in main: %s" % str(errore)

if __name__ == "__main__":
		main()
