#!/root/Envs/ARP/bin/python
# -*- coding: utf-8 -*-
# AUTHOR: Luca Carta

__author__= "Luca Carta - CRS4 - luca@crs4.it"
__date__ = "2016/07/25"
__comment__= "Script per la copia delle configurazioni degli apparati"
__version__= "0.1.1"

from bs4 import BeautifulSoup
import os
import re
import sys
import time
import pexpect
import getopt
import requests
import ssl
import cookielib
import subprocess
import threading
import smtplib
import NetDevices
from lxml import etree
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template
from Queue import Queue
from ConfigParser import ConfigParser



file2output  = "/var/log/save-running-output.log"
file2log     = "/var/log/save-running.log"
templateFile = "/root/scripts/save_from_device/template.html"
delimitatore = "-----------------------------------------------------------------------------------------------------"
esegui       = ""

STRINGA = """
Error, Please specify all parameters 
---------------------------------------------------------
\'-f\' or \'--filename\' - device list file
---------------------------------------------------------
"""

sys.stdout = open(file2output, 'a')
sys.stderr = open(file2output, 'a')

def leggiFileCFG(cfg_file):
    try:
        fCFG=ConfigParser()
        fCFG.read(cfg_file)
        return fCFG
    except Exception, e:
        writeLog("Error in leggiFileCFG: " + str(e))
        sys.exit("Error in leggiFileCFG: " + str(e))

def leggiLog():
    try:
        return file(file2log, "r+").read()
    except Exception, e:
        writeLog("Error in leggiLog: " + str(e))
        sys.exit("Error in leggiLog: " + str(e))

def writeLog(stringalog, mode="a"):
    # Funzione per la scrittura di un LOG File
    try:
        reLog=file(file2log, mode)
        reLog.write(str(stringalog)+"\n")
        reLog.close()
    except Exception, err:
        sys.exit("Error in writeLog: " + str(err))

def salvaRunning(stringa2log, log_file, modo="w"):
    # Funzione per la scrittura del Running-Config
    try:
        fLog=file(log_file, modo)
        fLog.write(str(stringa2log))
        fLog.close()
    except Exception, err:
        writeLog("Error in salvaRunning: " + str(err))
        sys.exit("Error in salvaRunning: " + str(err))

def salvaBIN(bin2log, log_file, modo="w"):
    # Funzione per la scrittura di un File BIN
    try:
        fLog=file(log_file, modo)
        fLog.write(bin2log)
        fLog.close()
    except Exception, err:
        writeLog("Errore in salvaBIN: " + str(err))
        sys.exit("Errore in salvaBIN: " + str(err))

def sendMail(subject, h_text, s_text, toUser = ['netop@crs4.it','lcarta.mail@gmail.com']):
    # Funzione per l'invio di una email
    #
    try:
        fromUser = "cacti@crs4.it"
        fromText = "Cacti <cacti@crs4.it>"
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = fromText
        msg['To'] = ", ".join(toUser)
        part1 = MIMEText(s_text, 'plain')
        part2 = MIMEText(h_text, 'html')
        msg.attach(part1)
        msg.attach(part2)
        server = smtplib.SMTP("mail.crs4.it")
        server.sendmail(fromUser, toUser, msg.as_string())
        server.quit()
    except Exception, e:
        writeLog("ERROR in sendMail: %s" % str(e))
        sys.exit("ERROR in sendMail: %s" % str(e))

def raggiungibile(apparato):
    # Funzione per la verifica della raggiungibilita' di un apparato
    try:
        pipe = subprocess.Popen("ping " + apparato + " -c 1", shell=True, stdout=subprocess.PIPE).stdout
        ok = False
        for i in pipe.readlines():
            if i.find(", 0% packet loss,") != -1: 
                ok = True
        if ok: return True
        else: return False
    except Exception, e:
        writeLog("ERROR in raggiungibile: %s" % str(e))

def Brocade(**kwarg):
    queue = kwarg.pop('q')
    brocade = NetDevices.Brocade()
    name = kwarg['prompt']
    try:
        if not raggiungibile(kwarg['device']):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt']))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "Impossibile salvare la configurazione. Host DOWN!!!"})
            return
        #
        running = kwarg.pop('save_to')
        writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
        writeLog(delimitatore)
        kwarg.pop('brand')
        kwarg.pop('model')
        brocade.connect(**kwarg)
        ret = "\n".join(brocade.run("sh run"))
        brocade.close()
        if 'error' in ret:
            writeLog("%s - Impossibile salvare la configurazione di %s in \'%s\': %s" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running, str(ret)))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "ERROR in Brocade: " + str(ret)})
            return Exception(ret)
        salvaRunning(ret, running)
        writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
        writeLog(delimitatore)
    except Exception, e:
        queue.put({'name': name, 'error': "ERROR in Brocade: " + str(e)})

def HP(**kwarg):
    hp = NetDevices.HP()
    queue = kwarg.pop('q')
    name = kwarg['prompt']
    try:
        hp.setName(kwarg['prompt'])
        if not raggiungibile(kwarg['device']):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt']))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "Impossibile salvare la configurazione. Host DOWN!!!"})
            return
        #
        running = kwarg.pop('save_to')
        writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
        writeLog(delimitatore)
        kwarg.pop('brand')
        kwarg.pop('model')
        hp.connect(**kwarg)
        ret = "\n".join(hp.run("sh run"))
        hp.close()
        if 'error' in ret:
            writeLog("%s - Impossibile salvare la configurazione di %s in \'%s\': %s" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running, str(ret)))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "ERROR in HP: " + str(ret)})
            return Exception(ret)
        salvaRunning(ret, running)
        writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
        writeLog(delimitatore)
    except Exception, e:
        queue.put({'name': name, 'error': "ERROR in HP: " + str(e)})
    
def Cisco(**kwarg):
    cisco = NetDevices.Cisco()
    queue = kwarg.pop('q')
    name = kwarg['prompt']
    try:
        if not raggiungibile(kwarg['device']):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt']))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "Impossibile salvare la configurazione. Host DOWN!!!"})
            return
        kwarg.pop('brand')
        model = kwarg.pop('model')
        #    
        if model == 'Catalyst':
            #
            running = kwarg.pop('save_to')
            writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
            writeLog(delimitatore)
            cisco.connect(**kwarg)
            ret = "\n".join(cisco.run("sh run"))
            #print ret
            cisco.close()
            if 'error' in ret:
                writeLog("%s - Impossibile salvare la configurazione di %s in \'%s\': %s" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running, str(ret)))
                writeLog(delimitatore)
                queue.put({'name': name, 'error': "ERROR in Cisco: " + str(ret)})
                return Exception(ret)
            salvaRunning(ret, running)
            writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
            writeLog(delimitatore)
        elif model == 'WAP200':
            salvaSuWAP200(**kwarg)
        elif model == 'MDS':
            salvaSuMDS(**kwarg)
    except Exception, e:
        queue.put({'name': name, 'error': "ERROR in Cisco: " + str(e)})
    
def Extreme(**kwarg):
    extreme = NetDevices.Extreme()
    queue = kwarg.pop('q')
    name = kwarg['prompt']
    try:
        if not raggiungibile(kwarg['device']):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt']))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "Impossibile salvare la configurazione. Host DOWN!!!"})
            return
        #
        running = kwarg.pop('save_to')
        writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
        writeLog(delimitatore)
        kwarg.pop('brand')
        kwarg.pop('model')
        extreme.connect(**kwarg)
        ret = "\n".join(extreme.run("sh conf"))
        extreme.close()
        if 'error' in ret:
            writeLog("%s - Impossibile salvare la configurazione di %s in \'%s\': %s" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running, str(ret)))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "ERROR in Extreme: " + str(ret)})
            return Exception(ret)
        salvaRunning(ret, running)
        writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
        writeLog(delimitatore)
    except Exception, e:
        queue.put({'name': name, 'error': "ERROR in Extreme: " + str(e)})

def pfSense(**kwarg):
    queue = kwarg.pop('q')
    try:
        kwarg.pop('brand')
        kwarg.pop('model')
        salvaSuPfSense2(**kwarg)
        #    
    except Exception, e:
        queue.put({'name': kwarg['prompt'], 'error': str(e)})

def H3C(**kwarg):
    h3c = NetDevices.H3C()
    queue = kwarg.pop('q')
    name = kwarg['prompt']
    try:
        if not raggiungibile(kwarg['device']):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt']))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "Impossibile salvare la configurazione. Host DOWN!!!"})
            return
        #
        running = kwarg.pop('save_to')
        writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
        writeLog(delimitatore)
        kwarg.pop('brand')
        kwarg.pop('model')
        h3c.connect(**kwarg)
        ret = "\n".join(h3c.run("display current-configuration"))
        h3c.close()
        if 'error' in ret:
            writeLog("%s - Impossibile salvare la configurazione di %s in \'%s\': %s" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running, str(ret)))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "ERROR in H3C: " + str(ret)})
            return Exception(ret)
        salvaRunning(ret, running)
        writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
        writeLog(delimitatore)
    except Exception, e:
        queue.put({'name': name, 'error': "ERROR in H3C: " + str(e)})
    
def Force10(**kwarg):
    c300 = NetDevices.Force10()
    queue = kwarg.pop('q')
    name = kwarg['prompt']
    try:
        if not raggiungibile(kwarg['device']):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt']))
            writeLog(delimitatore)
            queue.put({'name': name, 'error': "Impossibile salvare la configurazione. Host DOWN!!!"})
            return
        kwarg.pop('brand')
        model = kwarg.pop('model')
        #    
        if model == 'C300':
            #
            running = kwarg.pop('save_to')
            writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
            writeLog(delimitatore)
            c300.connect(**kwarg)
            ret = "\n".join(c300.run("sh run"))
            #print ret
            c300.close()
            if 'error' in ret:
                writeLog("%s - Impossibile salvare la configurazione di %s in \'%s\': %s" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running, str(ret)))
                writeLog(delimitatore)
                queue.put({'name': name, 'error': "ERROR in Force10: " + str(ret)})
                return Exception(ret)
            salvaRunning(ret, running)
            writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), kwarg['prompt'], running))
            writeLog(delimitatore)
        elif model == 'S50':
            salvaSuForce10(**kwarg)
    except Exception, e:
        queue.put({'name': name, 'error': "ERROR in Force10: " + str(e)})
        
def salvaSuPfSense2(device, username, password, save_to, prompt):
    try:
        # For Debug ONLY
        #
        # print device
        # print username
        # print password
        # print prompt
        # print save_to
        # print runn_file
        #
        # FIELDS:
        #
        # backuparea=
        # donotbackuprrd=on
        # encrypt_password=
        # encrypt_passconf=
        # Submit=Download+configuration
        # restorearea=
        # conffile=
        # decrypt_password=
        # decrypt_passconf=
        #
        #
        if not raggiungibile(device):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
            writeLog(delimitatore)
            return Exception(["Impossibile salvare la configurazione. Host DOWN!!!", prompt])
        writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, save_to))
        writeLog(delimitatore)
        www = ""
        #
        url = "https://"+device+"/diag_backup.php"
        client = requests.session()
        l = client.get(url, verify=False)
        data = {}
        data['backuparea'] = ""
        data['Submit'] = "Download configuration"
        data['restorearea'] = ""
        data['conffile'] = ""
        data['donotbackuprrd'] = "on"
        data['encrypt_password'] = ""
        data['encrypt_passconf'] = ""
        data['decrypt_password'] = ""
        data['decrypt_passconf'] = ""
        #
        login = 'Login'
        #
        page = BeautifulSoup(l.text)
        __csrf_magic = page.find('input', attrs = {'name':'__csrf_magic'})['value']
        login_data = { 'usernamefld' : username, 'passwordfld' : password, 'login': login, '__csrf_magic': __csrf_magic }
        r = client.post(url, data=login_data, headers=dict(Referer=url), verify=False)
        f = client.get(url, headers=dict(Referer=url), verify=False)
        __csrf_magic1 = BeautifulSoup(f.text).find('input', attrs = {'name':'__csrf_magic'})['value']
        data['__csrf_magic'] = __csrf_magic1
        z = client.post(url, data=data, headers=dict(Referer=url), verify=False)
        www = z.text
        client.close()
        salvaRunning(www, save_to)
        writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, save_to))
        writeLog(delimitatore)
    except Exception, e:
        return 'Errore nella sottofunzione salvaSuPfSense2: %s. Su %s' % (str(e), device)

def salvaSuWAP200(device, username, password, prompt, save_to):
    try:
        if not raggiungibile(device):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
            writeLog(delimitatore)
            return
        writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, save_to))
        writeLog(delimitatore)
        conf_file = ""
        #
        # For Debug ONLY
        #
        # print device
        # print username
        # print password
        # print prompt
        # print save_to
        # print runn_file
        #
        #
        url = "http://"+device+"/config.bin"
        client = requests.session()
        l = client.get(url, auth=(username, password))
        conf_file = l.content
        client.close()
        #
        salvaBIN(conf_file, save_to)
        writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, save_to))
        writeLog(delimitatore)
    except Exception,e:
        return 'Errore nella sottofunzione salvaSuWAP200: %s. Su %s' % (str(e), device)

def salvaSuMDS(device, prompt, save_to, username, password):
    try:
        if not raggiungibile(device):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
            writeLog(delimitatore)
            return
        # Are you sure you want to continue connecting (yes/no)?
        writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, save_to))
        writeLog(delimitatore)
        child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + username + "@" + device)
        child.expect("Password:")
        child.sendline(password)
        #
        child.expect(prompt + "#")
        child.sendline("ter len 0")
        child.expect(prompt + "#")
        child.sendline("sh run")
        #
        child.expect(prompt + "#")
        da_loggare = str(child.before).replace("sh run","").replace("[7m[0m[2K","").replace("","").replace("\r","")
        salvaRunning(da_loggare, save_to)
        child.sendline("exit")
        child.close()
        writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, save_to))
        writeLog(delimitatore)
    except Exception, errore:
        return "Errore su salvaSuMDS: " + str(errore)

def salvaSuForce10(device, prompt, save_to, username, secret):
    try:
        if not raggiungibile(device):
            writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
            writeLog(delimitatore)
            return
        writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, save_to))
        writeLog(delimitatore)
        child = pexpect.spawn("telnet " + device)
        child.expect("User:")
        child.sendline(username)
        child.expect("Password:")
        child.sendline("")
        child.expect(prompt + ">")
        child.sendline("enable")
        child.expect("Password:")
        child.sendline(secret)
        child.expect(prompt + "#")
        child.sendline("ter len 0")
        child.expect(prompt + "#")
        child.sendline("sh run")
        child.expect(prompt + "#")
        da_loggare = str(child.before).replace("sh run","").replace("Current Configuration ...","")
        salvaRunning(da_loggare.replace("[7m[0m[2K","").replace("","").replace("\r","").replace("show run", ""), save_to)
        child.sendline("write memory")
        child.expect("(y/n)")
        child.sendline("y")
        child.expect(prompt + "#")
        child.sendline("quit")
        child.close()
        writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, save_to))
        writeLog(delimitatore)
    except Exception, errore:
        return "Errore su salvaSuForce10: " + str(errore)

def verificaArgomentiPassati(opzioni_short = ['f'], opzioni_long = ['filename']):
    opzioni = ""
    long_opzioni = []
    try:
        for i in opzioni_short:
            opzioni = opzioni + str(i) + ":"
        for i in opzioni_long:
            long_opzioni.append(str(i) + "=")
        try:
            campi, args = getopt.getopt(sys.argv[1:], opzioni, long_opzioni)
        except getopt.GetoptError, err:
            sys.exit(STRINGA+str(err))
        #
        if len(campi) == len(opzioni_short):
            for attributo, value in campi:
                if attributo in ('-f','--filename'):    filename = value
            return filename
        else:
            sys.exit(STRINGA + "Attenzione - Specificare tutti i parametri")
    except Exception, e:
        print "ERROR in verificaArgomentiPassati: %s" % str(e)

def qStat(q):
    r_queue = []
    count = 0
    #
    while True:
        if q.qsize():
            item = q.get()
            r_queue.append(item)
        else:
            break
    #
    return r_queue

def main():
    try:
        writeLog(delimitatore, "w")
        writeLog("   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ New Attemp at %s @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   " % time.strftime('%d/%m/%Y %H:%M'))
        writeLog(delimitatore)
        print delimitatore
        print "   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ New Attemp at %s @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   " % time.strftime('%d/%m/%Y %H:%M')
        print delimitatore
        #
        q = Queue()
        esegui = ""
        devices = []
        commitDir = []
        thread_l = []
        dev = { 'Brocade': Brocade, 'HP': HP, 'Cisco': Cisco, 'Extreme': Extreme, 'pfSense': pfSense, 'H3C': H3C, 'Force10': Force10 }
        #
        mail = file(templateFile, "r").read()
        s = Template(mail)
        filename = verificaArgomentiPassati()
        a = leggiFileCFG(filename)
        #
        for i in a.sections():
            devices.append(dict(a.items(i)))
        #
        brand = list(set([ x['brand'] for x in devices ]))
        models = list(set([ x['model'] for x in devices ]))
        #
        for i in devices:
            i['q'] = q
            thread_l.append(threading.Thread(target=dev[i['brand']], name=i['prompt'], kwargs=i))
            if os.path.dirname(i['save_to']) not in commitDir: commitDir.append(os.path.dirname(i['save_to']))
        #
        for thread in thread_l: thread.start() 
        for thread in thread_l: thread.join()
        #
        #
        err = qStat(q)
        if err:
            devices = [ i for i in devices if i['prompt'] in [ x['name'] for x in err ] ]
            thread_l = []
            #
            time.sleep(10)
            #
            for i in devices:
                i['q'] = q
                thread_l.append(threading.Thread(target=dev[i['brand']], name=i['prompt'], kwargs=i))
                if os.path.dirname(i['save_to']) not in commitDir: commitDir.append(os.path.dirname(i['save_to']))
            #
            for thread in thread_l: thread.start() 
            for thread in thread_l: thread.join()
            #
            err = qStat(q)
        #
        for z in commitDir:
            svn = "svn add " + z + "*"
            esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
            svn = "svn ci " + z + "* -m 'Copia configurazioni apparati'"
            esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
        #
        writeLog(esegui)
        #
        in_mail = "-----------------------------------------------------------------------------------------------------\n"
        #
        r = 0
        to_log = ""
        colour1 = "#FFFFFF"
        colour2 = "#E8E8E8"
        #
        for i in err:
            if r % 2:
                color = colour2
            else:
                color = colour1
            in_mail = in_mail + str(i) + "\n"
            to_log += """                   <tr style="background-color:%s;">
                            <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                %s
                            </td>
                            <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                %s
                            </td>
                            <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                %s
                            </td>
                        </tr>
""" % (color, time.strftime('%d/%m/%Y %H:%M:%S'), i['name'], i['error'])
            r += 1
        msg = ""
        if to_log != "":
            msg = """                   <tr>
                            <th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                Date
                            </th>
                            <th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                Host
                            </th>
                            <th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                                Message
                            </th>
                        </tr>
%s""" % to_log
            status = "Devices backup problem"
        else:
            msg = to_log
            status = "Devices backup done"
        in_mail = in_mail + "-----------------------------------------------------------------------------------------------------"
        #
        #
        #
        #
        s_text = "Backup Eseguiti.\n\n" + delimitatore + "\n\nErrori riscontrati:\n" + in_mail
        sendMail("[Cacti] - Backup Status", s.substitute(STATUS=str(status), MSG=str(msg), YEAR=str(time.strftime('%Y'))), s_text)
    except Exception, errore:
        mail = file(templateFile, "r").read()
        msg ="""                    <tr>
                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
                            %s
                        </td>
""" % str(errore)
        s_text = "Problemi durante l'esecuzione del backup\n\n" + str(errore)
        status = "Devices backup problem"
        s = Template(mail)
        #
        sendMail("[Cacti] - Backup Status", s.substitute(STATUS=str(status), MSG=str(msg), YEAR=str(time.strftime('%Y'))), s_text, ['luca@crs4.it'])
        print "Error in main: %s" % str(errore)

if __name__ == "__main__":
    main()
