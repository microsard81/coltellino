#!/usr/bin/env python
# -*- coding: utf-8 -*-
# AUTHOR: Luca Carta

__author__= "Luca Carta - CRS4 - luca@crs4.it"
__date__ = "2015/08/31"
__comment__= "Script per la copia delle configurazioni degli apparati"
__version__= "1.3.2"

from bs4 import BeautifulSoup
import os
import re
import sys
import time
import pexpect
import getopt
import urllib2
import urllib
import requests
import ssl
import cookielib
import subprocess
import smtplib
from lxml import etree
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template


file2log     = "/var/log/save_from_switch.log"
templateFile = "/opt/scripts/save_from_device/template.html"
delimitatore = "-----------------------------------------------------------------------------------------------------\n"
esegui       = ""

STRINGA = """
Error, Please specify all parameters 
---------------------------------------------------------
\'-f\' or \'--filename\' - xml filename
---------------------------------------------------------
"""

def leggiFileXML(log_file):
	try:
		fXML=file(log_file, "r+")
		contenuto = fXML.read()
		return contenuto
	except Exception, e:
		sys.exit("Errore: " + str(e))

def leggiLog():
	try:
		return file(file2log, "r+").read()
	except Exception, e:
		sys.exit("Errore: " + str(e))

def writeLog(stringalog, mode="a"):
	# Funzione per la scrittura di un LOG File
	try:
		reLog=file(file2log, mode)
		reLog.write(str(stringalog)+"\n")
		reLog.close()
	except Exception, err:
		sys.exit("Errore: " + str(err))

def salvaRunning(stringa2log, log_file, modo="w"):
	# Funzione per la scrittura del Running-Config
	try:
		fLog=file(log_file, modo)
		fLog.write(str(stringa2log))
		fLog.close()
	except Exception, err:
		sys.exit("Errore: " + str(err))

def salvaBIN(bin2log, log_file, modo="w"):
	# Funzione per la scrittura di un File BIN
	try:
		fLog=file(log_file, modo)
		fLog.write(bin2log)
		fLog.close()
	except Exception, err:
		sys.exit("Errore: " + str(err))

def sendMail(subject, h_text, s_text, toUser = ['netop@crs4.it','lcarta.mail@gmail.com']):
	# Funzione per l'invio di una email
	#
	try:
		fromUser = "cacti@crs4.it"
		fromText = "Cacti <cacti@crs4.it>"
		msg = MIMEMultipart('alternative')
		msg['Subject'] = subject
		msg['From'] = fromText
		msg['To'] = ", ".join(toUser)
		part1 = MIMEText(s_text, 'plain')
		part2 = MIMEText(h_text, 'html')
		msg.attach(part1)
		msg.attach(part2)
		server = smtplib.SMTP("mail.crs4.it")
		server.sendmail(fromUser, toUser, msg.as_string())
		server.quit()
	except Exception, e:
		print "ERRORE: %s" % str(e)

def raggiungibile(apparato):
	# Funzione per la verifica della raggiungibilita' di un apparato
	try:
		pipe = subprocess.Popen("ping " + apparato + " -c 1", shell=True, stdout=subprocess.PIPE).stdout
		ok = False
		for i in pipe.readlines():
			if i.find(", 0% packet loss,") != -1: 
				ok = True
		if ok: return True
		else: return False
	except Exception, e:
		print "ERRORE: %s" % str(e)
		
def salvaSuPfSense2(device, www_user, www_password, cartella_file, runn_file, prompt):
	try:
		# For Debug ONLY
		#
		# print device
		# print www_user
		# print www_password
		# print prompt
		# print cartella_file
		# print runn_file
		#
		# FIELDS:
		#
		# backuparea=
		# donotbackuprrd=on
		# encrypt_password=
		# encrypt_passconf=
		# Submit=Download+configuration
		# restorearea=
		# conffile=
		# decrypt_password=
		# decrypt_passconf=
		#
		#
		if not raggiungibile(device):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		www = ""
		#
		url = "https://"+device+"/index.php"
		client = requests.session()
		l = client.get(url, verify=False)
		data = {}
		data['backuparea'] = ""
		data['Submit'] = "Download configuration"
		data['restorearea'] = ""
		data['conffile'] = ""
		data['donotbackuprrd'] = "on"
		data['encrypt_password'] = ""
		data['encrypt_passconf'] = ""
		data['decrypt_password'] = ""
		data['decrypt_passconf'] = ""
		#
		login = 'Login'
		#
		page = BeautifulSoup(l.text)
		__csrf_magic = page.find('input', attrs = {'name':'__csrf_magic'})['value']
		login_data = { 'usernamefld' : www_user, 'passwordfld' : www_password, 'login': login, '__csrf_magic': __csrf_magic }
		r = client.post(url, data=login_data, headers=dict(Referer=url), verify=False)
		url2 = "https://"+device+"/diag_backup.php"
		z = client.post(url2, data=data, headers=dict(Referer=url), verify=False)
		www = z.text
		client.close()
		salvaRunning(www, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception,e:
		sys.exit('Errore nella sottofunzione salvaSuPfSense2(): %s. Su %s' % (str(e), device))

def salvaSuPfSense(device, www_user, www_password, cartella_file, runn_file, prompt):
	try:
		if not raggiungibile(device):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		www = ""
		#
		#
		# For Debug ONLY
		#
		# print device
		# print www_user
		# print www_password
		# print prompt
		# print cartella_file
		# print runn_file
		#
		data = {}
		data['backuparea'] = ""
		data['Submit'] = "Download configuration"
		data['restorearea'] = ""
		data['conffile'] = ""
		#
		url = "https://"+device+"/diag_backup.php?"
		url_values = urllib.urlencode(data)
		full_url = urllib2.Request(url, url_values)
		#
		auth_handler = urllib2.HTTPBasicAuthHandler()
		auth_handler.add_password(realm='.', uri="https://"+device+"/diag_backup.php?", user=www_user, passwd=www_password)
		opener = urllib2.build_opener(auth_handler)
		urllib2.install_opener(opener)
		#
		## print full_url
		page = urllib2.urlopen(full_url)
		www = page.read()
		page.close()
		salvaRunning(www, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception,e:
		sys.exit('Errore nella sottofunzione salvaSuPfSense(): %s. Su %s' % (str(e), device))

def salvaSuWAP200(device, www_user, www_password, prompt, cartella_file, runn_file):
	try:
		if not raggiungibile(device):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		conf_file = ""
		#auth_handler = urllib2.HTTPBasicAuthHandler()
		#
		# For Debug ONLY
		#
		# print device
		# print www_user
		# print www_password
		# print prompt
		# print cartella_file
		# print runn_file
		#
		#
		url = "http://"+device+"/config.bin"
		client = requests.session()
		l = client.get(url, auth=(www_user, www_password))
		conf_file = l.content
		client.close()
		#
		salvaBIN(conf_file, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception,e:
		sys.exit('Errore nella sottofunzione salvaSuWAP200(): %s. Su %s' % (str(e), device))

def salvaSuHP(ip, prompt, cartella_file, runn_file, livello, protocollo, utente, password, modello, secret):
	try:
		#
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		da_loggare = ""
		rimpiazza = ['',
				 'Running configuration:',
				 'sh run',
				 prompt + '#',
				 '1H',
				 '19H',
				 '[24;',
				 '[?25h',
				 '18H',
				 '0HE',
				 '24H',
				 '[2K',
				 '[1;24r',
				 '20H',
				 '26H',
				 '13H',
				 '\r']
		if protocollo == "ssh" and livello == "2":
			child = pexpect.spawn("ssh " + utente + "@" + ip)
			try:
				child.expect("Are you sure you want to continue connecting (yes/no)?", timeout=5)
				child.sendline("yes")
			except:
				pass
			child.expect(utente + "@" + ip + "\'s password: ")
			child.sendline(password)
			child.expect("Press any key to continue", timeout=180)
			child.sendline("z")
			child.expect(prompt + ">")
			child.sendline("enable")
			child.expect("Password:")
			child.sendline(secret)
		elif protocollo == "ssh" and livello == "1":
			child = pexpect.spawn("ssh " + utente + "@" + ip)
			child.expect(utente + "@" + ip + "\'s password: ")
			child.sendline(password)
			child.expect("Press any key to continue", timeout=180)
			child.sendline("z")
		elif protocollo == "telnet" and livello == "1" and modello == "2800":
			#print "Sono sotto Telnet 1"
			child = pexpect.spawn("telnet " + ip)
			child.expect("Username:")
			child.sendline(utente)
			child.expect("Password:")
			child.sendline(password)
		elif protocollo == "telnet" and livello == "2" and modello == "2626":
			child = pexpect.spawn("telnet " + ip)
			child.expect("Press any key to continue", timeout=60)
			child.sendline(" ")
			child.expect("Please Enter Login Name:", timeout=60)
			child.sendline(utente)
			child.sendline(password)
			child.expect(prompt + ">", timeout=5)
			child.sendline("enable")
			child.expect("Password:", timeout=5)
			child.sendline(secret)
		child.expect(prompt + "#")
		child.sendline("sh run")
		#
		da_loggare = ""
		#
		try:
			z = 0
			while z == 0:
				child.expect("-- MORE --, next page: Space, next line: Enter, quit: Control-C", timeout=5)
				da_loggare = da_loggare + str(child.before)
				child.sendline(" ")
		except:
			child.expect(prompt + "#")
			da_loggare = da_loggare + str(child.before)
		#
		child.sendline("write memory")
		child.expect(prompt + "#")
		child.sendline("exit")
		child.expect(prompt + ">")
		child.sendline("exit")
		child.expect("Do you want to log out [y/n]?")
		child.sendline("y")
		child.close()
		#
		# Pulizia da caratteri indesiderati
		#
		for element in rimpiazza:
			da_loggare = da_loggare.replace(element,"")
		#
		salvaRunning(da_loggare, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		sys.exit("Errore su salvaSuHP: " + str(errore))
	  
def salvaSuBrocade(ip, prompt, secret, cartella_file, runn_file, livello, more, protocollo, utente, password, prompt_ver):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		#
		if protocollo == "ssh":
			child = pexpect.spawn("ssh " + utente + "@" + ip)
			if prompt_ver == 2:	
				child.expect("Password:")
			else:	
				child.expect(utente + "@" + ip + "\'s password: ")
			child.sendline(password)
		elif protocollo == "telnet" and livello == "1":
			child = pexpect.spawn("telnet " + ip)
		elif protocollo == "telnet" and livello == "2":
			child = pexpect.spawn("telnet " + ip)
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password:")
		child.sendline(secret)
		child.expect(prompt + "#")
		da_loggare = ""
		if more:
			child.sendline("ter len 0")
			child.expect(prompt + "#")
			child.sendline("sh run")
			#
			child.expect(prompt + "#")
			#
		else:
			child.sendline("sh run")
			#
			try:
				z = 0
				while z == 0:
					child.expect("--More--, next page: Space, next line: Return key, quit: Control-c", timeout=5)
					da_loggare = da_loggare + str(child.before)
					child.sendline(" ")
			except:
				child.expect(prompt + "#")
				da_loggare = da_loggare + str(child.before)
		#
		child.sendline("write memory")
		child.expect(prompt + "#")
		child.sendline("exit")
		child.expect(prompt + ">")
		child.sendline("exit")
		child.close()
		da_loggare = da_loggare.replace("sh run\r\n","").replace("[7m[0m[2K","").replace("","").replace("\r","")
		salvaRunning(da_loggare, cartella_file + runn_file)	
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		sys.exit("Errore su salvaSuBrocade: " + str(errore))

def salvaSuMDS(ip, prompt, cartella_file, runn_file, no_more, utente, password):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		# Are you sure you want to continue connecting (yes/no)?
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("ssh " + utente + "@" + ip)
		try:
			child.expect("Are you sure you want to continue connecting (yes/no)?", timeout=10)
			child.sendline("yes")
		except:
			pass
		child.expect("Password:")
		child.sendline(password)
		#
		child.expect(prompt + "#")
		if no_more:	child.sendline("ter len 0")
		child.expect(prompt + "#")
		child.sendline("sh run")
		#
		child.expect(prompt + "#")
		da_loggare = str(child.before).replace("sh run","").replace("[7m[0m[2K","").replace("","").replace("\r","")
		salvaRunning(da_loggare, cartella_file + runn_file)
		child.sendline("exit")
		child.close()
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		sys.exit("Errore su salvaSuMDS: " + str(errore))

def salvaSuCatalystSSH(ip, prompt, cartella_file, runn_file, no_more, utente, password, secret):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		# Are you sure you want to continue connecting (yes/no)?
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("ssh " + utente + "@" + ip)
		try:
			child.expect("Are you sure you want to continue connecting (yes/no)?", timeout=10)
			child.sendline("yes")
		except:
			pass
		child.expect("Password: ")
		child.sendline(password)
		#
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password: ")
		child.sendline(secret)
		#
		child.expect(prompt + "#")
		if no_more:	child.sendline("ter len 0")
		child.expect(prompt + "#")
		child.sendline("sh run")
		#
		child.expect(prompt + "#")
		da_loggare = str(child.before).replace("sh run","").replace("[7m[0m[2K","").replace("","").replace("\r","")
		salvaRunning(da_loggare, cartella_file + runn_file)
		child.sendline("exit")
		child.close()
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		sys.exit("Errore su salvaSuMDS: " + str(errore))

def salvaSuCatalystTelnet(ip, prompt, cartella_file, runn_file, no_more, utente, password, secret):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		# Are you sure you want to continue connecting (yes/no)?
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("telnet " + ip)
		child.expect("Username: ")
		child.sendline(utente)
		child.expect("Password: ")
		child.sendline(password)
		#
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password: ")
		child.sendline(secret)
		#
		child.expect(prompt + "#")
		if no_more:	child.sendline("ter len 0")
		child.expect(prompt + "#")
		child.sendline("sh run")
		#
		child.expect(prompt + "#")
		da_loggare = str(child.before).replace("sh run","").replace("[7m[0m[2K","").replace("","").replace("\r","")
		salvaRunning(da_loggare, cartella_file + runn_file)
		child.sendline("exit")
		child.close()
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		sys.exit("Errore su salvaSuMDS: " + str(errore))

def salvaSuForce10(ip, prompt, secret, cartella_file, runn_file, no_more, protocollo, utente, password, modello="S50"):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("telnet " + ip)
		if modello == "C300" or modello == "C300-TFTP":	child.expect("Login: ")
		elif modello == "S50":	child.expect("User:")
		child.sendline(utente)
		child.expect("Password:")
		child.sendline(password)
		child.expect(prompt + ">")
		child.sendline("enable")
		child.expect("Password:")
		child.sendline(secret)
		child.expect(prompt + "#")
		if no_more:	child.sendline("ter len 0")
		child.expect(prompt + "#")
		if not modello == "C300-TFTP": 
			child.sendline("sh run")
		#
			child.expect(prompt + "#")
			da_loggare = str(child.before).replace("sh run","").replace("Current Configuration ...","")
			salvaRunning(da_loggare.replace("[7m[0m[2K","").replace("","").replace("\r","").replace("show run", ""), cartella_file + runn_file)
		else:
			to_send = "copy startup-config tftp://156.148.66.100/" + cartella_file.split("/")[-2] + "/" + runn_file
			child.sendline(to_send)
			child.expect(prompt + "#")
			da_loggare = str(child.before).replace(to_send,"")
			print da_loggare
		if modello =="C300":
			child.sendline("write memory")
			child.expect(prompt + "#")
			child.sendline("exit")
		elif modello =="S50":
			child.sendline("write memory")
			child.expect("(y/n)")
			child.sendline("y")
			child.expect(prompt + "#")
			child.sendline("quit")
		child.close()
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		sys.exit("Errore su salvaSuForce10: " + str(errore))

def salvaSuExtreme(utente, password, ip, prompt, cartella_file, runn_file, protocollo = 'telnet'):
	try:
		if protocollo == 'telnet':
			if not raggiungibile(ip):
				writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
				writeLog("-----------------------------------------------------------------------------------------------------")
				return
			writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
			child = pexpect.spawn("telnet " + ip)
			child.expect("login:")
			child.sendline(utente)
			child.expect("password:")
			child.sendline(password)
			child.expect(prompt + ":1 #")
			child.sendline("show configuration")
			da_loggare = ""
			#
			try:
				z = 0
				while z == 0:
					child.expect("Press <SPACE> to continue or <Q> to quit:", timeout=5)
					da_loggare = da_loggare + str(child.before)
					child.sendline(" ")
			except:
				child.expect(prompt + ":")
				da_loggare = da_loggare + str(child.before)
			child.sendline("save")
			child.expect("database?")
			child.sendline("yes")
			child.expect((prompt + ":"), timeout=180)
			child.sendline("exit")
			child.close()
			salvaRunning(da_loggare.replace("show configuration", "").replace("[7m[0m[2K","").replace("","").replace("\r",""), cartella_file + runn_file)
			writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
			writeLog("-----------------------------------------------------------------------------------------------------")
		else:
			if not raggiungibile(ip):
				writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
				writeLog("-----------------------------------------------------------------------------------------------------")
				return
			writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
			child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
			try:
				child.expect("Are you sure you want to continue connecting (yes/no)?", timeout=10)
				child.sendline("yes")
			except:
				pass
			child.expect("Enter password for " + utente + ":")
			child.sendline(password)
			child.expect(prompt + ".1 #")
			child.sendline("show configuration")
			da_loggare = ""
			#
			try:
				z = 0
				while z == 0:
					child.expect("Press <SPACE> to continue or <Q> to quit:", timeout=5)
					da_loggare = da_loggare + str(child.before)
					child.sendline(" ")
			except:
				child.expect(prompt + ".2 #")
				da_loggare = da_loggare + str(child.before)
			child.sendline("save")
			child.expect("(y/N)")
			child.sendline("y")
			child.expect(prompt + ".3 #")
			child.sendline("exit")
			child.close()
			da_loggare = da_loggare.replace("show configuration", "").replace("[7m[0m[2K",
										  "").replace("",
										  "").replace("[7m",
										  "").replace("[m",
										  "").replace("[60;D",
										  "").replace("[K",
										  "").replace("\r",
										  "")
			salvaRunning(da_loggare, cartella_file + runn_file)
			writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
			writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		sys.exit("Errore su salvaSuExtreme: " + str(errore))

def salvaSuH3C(utente, password, secret, ip, prompt, cartella_file, runn_file):
	try:
		if not raggiungibile(ip):
			writeLog("%s - Impossibile salvare la configurazione di %s. Host DOWN!!! " % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt))
			writeLog("-----------------------------------------------------------------------------------------------------")
			return
		writeLog("%s - Salvo la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		child = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no " + utente + "@" + ip)
		child.expect(utente + "@" + ip + "\'s password: ")
		child.sendline(password)
		child.expect("<" + prompt + ">")
		child.sendline("super")
		child.expect(" Password:")
		child.sendline(secret)
		child.expect("<" + prompt + ">")
		child.sendline("screen-length disable")
		child.expect("<" + prompt + ">")
		child.sendline("display current-configuration")
		da_loggare = ""
		#
		child.expect("<" + prompt + ">")
		da_loggare = da_loggare + str(child.before)
		child.sendline("save")
		child.expect("Y/N")
		child.sendline("y")
		child.expect("enter key")
		child.sendline()
		child.expect("Y/N")
		child.sendline("y")
		child.expect("<" + prompt + ">")		
		child.sendline("exit")
		child.close()
		da_loggare = da_loggare.replace("display current-configuration", "").replace("[7m[0m[2K",
								      "").replace("",
								      "").replace("[7m",
								      "").replace("[m",
								      "").replace("[60;D",
								      "").replace("[K",
								      "").replace("\r",
								      "")
		salvaRunning(da_loggare, cartella_file + runn_file)
		writeLog("%s - Salvato la configurazione di %s in \'%s\'" % (time.strftime('%d/%m/%Y %H:%M:%S'), prompt, cartella_file + runn_file))
		writeLog("-----------------------------------------------------------------------------------------------------")
	except Exception, errore:
		sys.exit("Errore su salvaSuH3C: " + str(errore))

def verificaArgomentiPassati(opzioni_short = ['f'], opzioni_long = ['filename']):
	opzioni = ""
	long_opzioni = []
	try:
		for i in opzioni_short:
			opzioni = opzioni + str(i) + ":"
		for i in opzioni_long:
			long_opzioni.append(str(i) + "=")
		try:
			campi, args = getopt.getopt(sys.argv[1:], opzioni, long_opzioni)
		except getopt.GetoptError, err:
			sys.exit(STRINGA+str(err))
		#
		if len(campi) == len(opzioni_short):
			for attributo, value in campi:
				if attributo in ('-f','--filename'):	filename = value
			return filename
		else:
			sys.exit(STRINGA + "Attenzione - Specificare tutti i parametri")
	except Exception, e:
		print "ERRORE: %s" % str(e)

def main():
	try:
	  writeLog("-----------------------------------------------------------------------------------------------------", "w")
	  writeLog("   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ New Attemp at %s @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   " % time.strftime('%d/%m/%Y %H:%M'))
	  writeLog("-----------------------------------------------------------------------------------------------------")
	  #
	  esegui = ""
	  #
	  mail = file(templateFile, "r").read()
	  s = Template(mail)
	  #
	  #
	  filename = verificaArgomentiPassati()
	  xml = leggiFileXML(filename)
	  #
	  stampa = etree.XML(xml.replace('\n','').replace("&","ECOMM"))
	  stampa = stampa.getchildren()
	  #
	  device = []
	  commitDir = []
	  #
	  for i in stampa:
		  iter = i.getiterator()
		  # print iter
		  dizionario = {}
		  for j in iter:
			if j.tag != 'entry':
				dizionario[j.tag] = j.text.replace("ECOMM","&")
		  device.append(dizionario)
	  # 
	  # print device
	  #
	  for valore in device:
		#
			if valore['protocol'] in ('telnet','ssh', 'www'):
				if valore['device'].find("Brocade") != -1:
					if valore['ter_len'] == "yes":  more = True
					elif valore['ter_len'] == "no": more = False
					_prompt_ver = 1
					if valore['device'].find("ICX") != -1: _prompt_ver = 2
					if valore['user'] != "":
						salvaSuBrocade(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   valore['auth_level'],
								   more,
								   valore['protocol'],
								   valore['user'],
								   valore['password'],
								   _prompt_ver)
					else:
						salvaSuBrocade(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   valore['auth_level'],
								   more,
								   valore['protocol'],
								   "test",
								   "test",
								   _prompt_ver)
					#
					svn = "svn add " + valore['file_dir'] + valore['running_file']
					esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					# svn = "svn ci " + valore['file_dir'] + valore['running_file'] + " -m 'Copia configurazioni apparati'"
					# esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					#
				elif valore['device'].find("H3C") != -1:
					salvaSuH3C(valore['user'],
							   valore['password'],
							   valore['secret'],
							   valore['ip'],
							   valore['prompt'], 
							   valore['file_dir'],
							   valore['running_file'])
					#
					svn = "svn add " + valore['file_dir'] + valore['running_file']
					esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					# svn = "svn ci " + valore['file_dir'] + valore['running_file'] + " -m 'Copia configurazioni apparati'"
					# esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					#
				elif valore['device'].find("Extreme") != -1:
					salvaSuExtreme(valore['user'],
							   valore['password'],
							   valore['ip'],
							   valore['prompt'], 
							   valore['file_dir'],
							   valore['running_file'],
							   valore['protocol'])
					#
					svn = "svn add " + valore['file_dir'] + valore['running_file']
					esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					# svn = "svn ci " + valore['file_dir'] + valore['running_file'] + " -m 'Copia configurazioni apparati'"
					# esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					#
				elif valore['device'].find("pfSense2") != -1:
					salvaSuPfSense2(valore['ip'],
							   valore['user'],
							   valore['password'],
							   valore['file_dir'],
							   valore['running_file'],
							   valore['hostname'])
					#
					svn = "svn add " + valore['file_dir'] + valore['running_file']
					esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					# svn = "svn ci " + valore['file_dir'] + valore['running_file'] + " -m 'Copia configurazioni apparati'"
					# esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					#
				elif valore['device'].find("pfSense") != -1:
					salvaSuPfSense(valore['ip'],
							   valore['user'],
							   valore['password'],
							   valore['file_dir'],
							   valore['running_file'],
							   valore['hostname'])
					#
					svn = "svn add " + valore['file_dir'] + valore['running_file']
					esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					# svn = "svn ci " + valore['file_dir'] + valore['running_file'] + " -m 'Copia configurazioni apparati'"
					# esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					#
				elif valore['device'].find("Force10") != -1:
					if valore['ter_len'] == "yes":  more = True
					elif valore['ter_len'] == "no": more = False
					# 
					if valore['device'].find("C300") != -1:
						salvaSuForce10(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   more,
								   valore['protocol'],
								   valore['user'],
								   valore['password'],
								   "C300")
					elif valore['device'].find("TFTP") != -1:
						salvaSuForce10(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   more,
								   valore['protocol'],
								   valore['user'],
								   valore['password'],
								   "C300-TFTP")
					else:
						salvaSuForce10(valore['ip'], 
								   valore['prompt'], 
								   valore['secret'],
								   valore['file_dir'],
								   valore['running_file'],
								   more,
								   valore['protocol'],
								   valore['user'],
								   "")
					#
					svn = "svn add " + valore['file_dir'] + valore['running_file']
					esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					# svn = "svn ci " + valore['file_dir'] + valore['running_file'] + " -m 'Copia configurazioni apparati'"
					# esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					#
				elif valore['device'].find("Cisco") != -1:
					if valore['ter_len'] == "yes":  more = True
					elif valore['ter_len'] == "no": more = False
					# 
					if valore['device'].find("MDS") != -1:
						salvaSuMDS(valore['ip'],
							   valore['prompt'],
							   valore['file_dir'],
							   valore['running_file'],
							   more, 
							   valore['user'],
							   valore['password'])
					if valore['device'].find("Catalyst") != -1 and valore['protocol'] == "ssh":
						salvaSuCatalystSSH(valore['ip'],
								   valore['prompt'],
								   valore['file_dir'],
								   valore['running_file'],
								   more, 
								   valore['user'],
								   valore['password'],
								   valore['secret'])
					if valore['device'].find("Catalyst") != -1 and valore['protocol'] == "telnet":
						salvaSuCatalystTelnet(valore['ip'],
								   valore['prompt'],
								   valore['file_dir'],
								   valore['running_file'],
								   more, 
								   valore['user'],
								   valore['password'],
								   valore['secret'])
					if valore['device'].find("WAP200") != -1:
						salvaSuWAP200(valore['ip'],
							   valore['user'],
							   valore['password'],
							   valore['prompt'],
							   valore['file_dir'],
							   valore['running_file'])
					#
					svn = "svn add " + valore['file_dir'] + valore['running_file']
					esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					# svn = "svn ci " + valore['file_dir'] + valore['running_file'] + " -m 'Copia configurazioni apparati'"
					# esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					#
				elif valore['device'].find("HP") != -1:
					# 
					if valore['device'].find("2626") != -1 or \
					   valore['device'].find("2510") != -1:
						salvaSuHP(valore['ip'],
							   valore['prompt'],
							   valore['file_dir'],
							   valore['running_file'],
							   valore['auth_level'],
							   valore['protocol'],
							   valore['user'],
							   valore['password'],
							   "2626",
							   valore['secret'])
					if valore['device'].find("2848") != -1 or \
					   valore['device'].find("2824") != -1 or \
					   valore['device'].find("2828") != -1:
						salvaSuHP(valore['ip'],
							   valore['prompt'],
							   valore['file_dir'],
							   valore['running_file'],
							   valore['auth_level'],
							   valore['protocol'],
							   valore['user'],
							   valore['password'],
							   "2824",
							   valore['secret'])
					#
					svn = "svn add " + valore['file_dir'] + valore['running_file']
					esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					if valore['file_dir'] not in commitDir: commitDir.append(valore['file_dir'])
					# svn = "svn ci " + valore['file_dir'] + valore['running_file'] + " -m 'Copia configurazioni apparati'"
					# esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
					#
			else:
				s_text = "Problemi durante l'esecuzione del backup\n\n"
				status = "Devices backup problem"
				msg =""" 					<tr>
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							Problemi durante l'esecuzione del backup
						</td>
"""
				sendMail("[Cacti] - Backup Status", s.substitute(STATUS=str(status), MSG=str(msg)), s_text, ['luca@crs4.it'])
				sys.exit("Errore: definizione del protocollo errata")
	  #
	  #
	  for z in commitDir:
		  svn = "svn ci " + z + "* -m 'Copia configurazioni apparati'"
		  esegui = esegui + subprocess.Popen(svn, shell=True, stdout=subprocess.PIPE).stdout.read()
	  #
	  writeLog(esegui)
	  #
	  in_mail = "-----------------------------------------------------------------------------------------------------\n"
	  error = leggiLog().split("\n")
	  #for i in error:
	  #	if i.find("Impossibile") != -1:
	  #		in_mail = in_mail + str(i) + "\n"
	  #in_mail = in_mail + "-----------------------------------------------------------------------------------------------------"
	  #
	  #
	  #
	  r = 0
	  to_log = ""
	  colour1 = "#FFFFFF"
	  colour2 = "#E8E8E8"
	  #
	  for i in error:
		if r % 2:
			color = colour2
		else:
			color = colour1
		if i.find("Impossibile") != -1:
			in_mail = in_mail + str(i) + "\n"
			to_log += """					<tr style="background-color:%s;">
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							%s
						</td>
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							%s
						</td>
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							%s
						</td>
					</tr>
""" % (color, i.split(" - ")[0], i.split(" - ")[1].split(".")[0].replace("Impossibile salvare la configurazione di ",""), i.split(" - ")[1].split(".")[1])
			r += 1
	  msg = ""
	  if to_log != "":
		msg = """					<tr>
						<th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							Date
						</th>
						<th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							Host
						</th>
						<th valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							Message
						</th>
					</tr>
%s""" % to_log
		status = "Devices backup problem"
	  else:
		msg = to_log
		status = "Devices backup done"
	  in_mail = in_mail + "-----------------------------------------------------------------------------------------------------"
	  #
	  #
	  #
	  #
	  s_text = "Backup Eseguiti.\n\n" + delimitatore + "\nErrori riscontrati:\n" + in_mail
	  #sendMail("[Cacti] - Backup Status", "Backup Eseguiti.\n\n" + delimitatore + "\nErrori riscontrati:\n" + in_mail, ['luca@crs4.it'])
	  sendMail("[Cacti] - Backup Status", s.substitute(STATUS=str(status), MSG=str(msg)), s_text)
	except Exception, errore:
	  mail = file(templateFile, "r").read()
	  msg =""" 					<tr>
						<td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">
							%s
						</td>
""" % str(errore)
	  s_text = "Problemi durante l'esecuzione del backup\n\n" + str(errore)
	  status = "Devices backup problem"
	  s = Template(mail)
	  #
	  sendMail("[Cacti] - Backup Status", s.substitute(STATUS=str(status), MSG=str(msg)), s_text, ['luca@crs4.it'])
	  print "Errore: %s" % str(errore)

if __name__ == "__main__":
		main()
