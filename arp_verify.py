#!/root/Envs/ARP/bin/python
#

import SRX as srx
import bigIron as big
import timeit
import time
import ping
import netaddr
from collections import OrderedDict
import struct
import socket
import sys
import urllib
from datetime import datetime
from pymongo import MongoClient
from ConfigParser import ConfigParser

def reLog(message):
	f = file("/var/log/arpScan.log", "a")
	f.write(message+"\r\n")
	f.close()

def parser():
	nets = []
	pars = ConfigParser()
	pars.read("/root/scripts/conf.ini")
	for o in pars.sections():
		nets.append((pars.get(o, "net"), pars.get(o, "no_scan").split(",")))
	#
	return nets

def arpScan():
	tot_start = timeit.default_timer()
	count = 0
	for i in parser():
		net = netaddr.IPNetwork(i[0])
		no_scan = i[1]
		for ip in net:
			if ip != net.broadcast and ip != net.network and str(ip) not in no_scan:
				res = ping.quiet_ping(str(ip), timeout = 0, count = 1, psize = 64)
				count += 1
	tot_stop = timeit.default_timer()
	#
	return str(count) + " IP scanned in " + str(int(tot_stop - tot_start)) + " seconds"

def arpGet():
	tot_start = timeit.default_timer()
	child = srx.connetti("sorveglione",'v1gil4','10.0.0.40','SRX-3600-DOWN')
	arp_fw = srx.getArp(child)
	srx.chiudi(child)
	child = big.connetti("sorveglione",'v1gil4','Qelpd&10','10.0.0.4','SSH@Bora')
	arp_bora = big.getArp(child, "", "include Valid")
	big.chiudi(child)
	child = big.connetti("sorveglione",'v1gil4','Qelpd&10','10.0.0.62','SSH@Kamsin')
	arp_kams = big.getArp(child, "", "include Valid")
	big.chiudi(child)
	child = big.connetti("sorveglione",'v1gil4','Qelpd&10','10.0.0.1','SSH@Maestrale')
	arp_maes = big.getArp(child, "", "include Valid")
	big.chiudi(child)
	#
	for i in arp_fw:	arp_fw[i]["device"]="SRX-3600-DOWN"
	for i in arp_bora:	arp_bora[i]["device"]="Bora"
	for i in arp_maes:	arp_maes[i]["device"]="Maestrale"
	for i in arp_kams:	arp_kams[i]["device"]="Kamsin"
	#
	arp = arp_fw
	arp.update(arp_bora)
	arp.update(arp_maes)
	arp.update(arp_kams)
	#
	arp2 = OrderedDict(sorted(arp.items(), key=lambda (x,y): struct.unpack("!L", socket.inet_aton(x))[0]))
	#
	arr = []
	#
	for i in arp2:
		g = arp2[i]
		g["_id"] = i 
		arr.append(g)
	#
	user = 'root'
	password = '4hbd$spd'
	client = MongoClient()['ARP']
	client.authenticate(user, password, mechanism='MONGODB-CR')
	collect = client.table
	#
	for i in arr:
		s = dict([(x, i[x]) for x in i if x != "_id"])
		s["update"] = datetime.now()
		l = collect.update_one({'_id': i["_id"]}, {"$set": s}, upsert=True)
	#
	tot_stop = timeit.default_timer()
	return "%s records updated in %s seconds" % (len(arr), str(int(tot_stop - tot_start)))

def scan():
	try:
		reLog(datetime.now().strftime("%d/%m/%Y - %H:%M:%S -> ") + arpScan())
		time.sleep(10)
		reLog(datetime.now().strftime("%d/%m/%Y - %H:%M:%S -> ") + arpGet())
	except Exception, e:
		reLog(str(e))

if __name__ == '__main__':
	try:
		reLog(datetime.now().strftime("%d/%m/%Y - %H:%M:%S -> ") + arpScan())
		time.sleep(10)
		reLog(datetime.now().strftime("%d/%m/%Y - %H:%M:%S -> ") + arpGet())
	except Exception, e:
		reLog(str(e))
