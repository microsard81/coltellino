function line_custom_prototype(index) {
      var content, j, row, y, _i, _len, _ref;
      row = this.data[index];
      content = "<div class='morris-hover-row-label'>" + row.label + "</div><div class='morris-data-columns'>";
      _ref = row.y;
      for (j = _i = 0, _len = _ref.length; _i < _len; j = ++_i) {
        y = _ref[j];
        content += "<div class='morris-hover-point' style='color: " + (this.colorFor(row, j, 'label')) + "'>\n  " + this.options.labels[j] + ":\n  " + (this.yLabelFormat(y)) + "\n</div>";
      }
      content += "</div>";
      if (typeof this.options.hoverCallback === 'function') {
        content = this.options.hoverCallback(index, this.options, content, row.src);
      }
      
      return [content, row._x, row._ymax];
    };




$(document).ready( function() {

	PLAN_ID = $('#plan-id').text().trim();



	$.ajax({
		// catturare dinamicamente l'url
        url: "/resume_data/get_data/interval/30/plan/" + PLAN_ID,
        type: "get",
        dataType: "json",
        timeout:3000,
        success : function (resp){
//             var a = Morris.Line({
//                 element: 'usage-slots-daily-chart',
//                 data: resp.slots,

//                 hoverCallback: function(index, options, content) {
// //                	$("#row-content").html("<div>" + "<br />" + options.labels[index] + ": " + options.data[index] + "<br /> </div>");
//                     $("#daily-slots-row-content").html(content);
//            //      	options.labels[index]  options.data[index]
// 			        // return(content);
// 			    },
//                 //data : d,
// 			  xkey: 'when',
// 			  // A list of names of data record attributes that contain y-values.
// 			  ykeys: resp.keys,
// 			  // Labels for the ykeys -- will be displayed when you hover over the
// 			  // chart.
// 			  labels: resp.keys, // ['Value'],
// 			  // hoverContentForRow : line_custom_prototype
// 				            });
//             a.hoverContentForRow = line_custom_prototype;


			var b = Morris.Line;

			b.prototype.hoverContentForRow = line_custom_prototype;
			var a = b({
			                element: 'usage-slots-daily-chart',
			                data: resp.slots,
							hoverCallback: function(index, options, content) {
			
			                    $("#daily-slots-row-content").html(content);
			
						    },
			                			                
						  xkey: 'when',
						  // A list of names of data record attributes that contain y-values.
						  ykeys: resp.keys,
						  // Labels for the ykeys -- will be displayed when you hover over the
						  // chart.
						  labels: resp.keys, // ['Value'],

						  // hoverContentForRow : line_custom_prototype
			            });
			    // a.hoverContentForRow = line_custom_prototype;
				
        },
        error: function(jqXHR, textStatus, ex) {
          console.log(textStatus + "," + ex + "," + jqXHR.responseText);
        }
    });



$.ajax({
		// catturare dinamicamente l'url
        url: "/resume_data/get_data/interval/30/plan/"+ PLAN_ID,
        type: "get",
        dataType: "json",
        timeout:3000,
        success : function (resp){
//             var a = Morris.Line({
//                 element: 'usage-slots-daily-chart',
//                 data: resp.slots,

//                 hoverCallback: function(index, options, content) {
// //                	$("#row-content").html("<div>" + "<br />" + options.labels[index] + ": " + options.data[index] + "<br /> </div>");
//                     $("#daily-slots-row-content").html(content);
//            //      	options.labels[index]  options.data[index]
// 			        // return(content);
// 			    },
//                 //data : d,
// 			  xkey: 'when',
// 			  // A list of names of data record attributes that contain y-values.
// 			  ykeys: resp.keys,
// 			  // Labels for the ykeys -- will be displayed when you hover over the
// 			  // chart.
// 			  labels: resp.keys, // ['Value'],
// 			  // hoverContentForRow : line_custom_prototype
// 				            });
//             a.hoverContentForRow = line_custom_prototype;


			var b = Morris.Bar;
			b.prototype.hoverContentForRow = line_custom_prototype;
			var a = b({
			                element: 'usage-stacked-slots-daily-chart',
			                data: resp.slots,
							hoverCallback: function(index, options, content) {
			
			                    $("#daily-stacked-slots-row-content").html(content);
			
						    },
			                			                
						  xkey: 'when',
						  // A list of names of data record attributes that contain y-values.
						  ykeys: resp.keys,
						  // Labels for the ykeys -- will be displayed when you hover over the
						  // chart.
						  labels: resp.keys, // ['Value'],
						  stacked: true
						  // hoverContentForRow : 	line_custom_prototype
			            });
			    // a.hoverContentForRow = line_custom_prototype;
				
        },
        error: function(jqXHR, textStatus, ex) {
          console.log(textStatus + "," + ex + "," + jqXHR.responseText);
        }
    });




$.ajax({
        url: "/resume_data/get_data/interval/365/plan/" + PLAN_ID,
        type: "get",
        dataType: "json",
        timeout:3000,
        success : function (resp){
            Morris.Line({
                element: 'usage-slots-monthly-chart',
                data: resp.slots,
                hoverCallback: function(index, options, content) {
//                  $("#row-content").html("<div>" + "<br />" + options.labels[index] + ": " + options.data[index] + "<br /> </div>");
                    $("#monthly-slots-row-content").html(content);
           //       options.labels[index]  options.data[index]
                    // return(content);
                },
                //data : d,
			  xkey: 'when',
			  // A list of names of data record attributes that contain y-values.
			  ykeys: resp.keys,
			  // Labels for the ykeys -- will be displayed when you hover over the
			  // chart.
			  labels: resp.keys // ['Value']
				            });
        },
        error: function(jqXHR, textStatus, ex) {
          console.log(textStatus + "," + ex + "," + jqXHR.responseText);
        }
    });


$.ajax({
        url: "/resume_data/get_data/interval/365/plan/5adb1bb6-8e02-4681-ab5d-b8001ae7a5e7",
        type: "get",
        dataType: "json",
        timeout:3000,
        success : function (resp){
            Morris.Line({
                element: 'usage-wall-monthly-chart',
                data: resp.wall,
                hoverCallback: function(index, options, content) {
//                  $("#row-content").html("<div>" + "<br />" + options.labels[index] + ": " + options.data[index] + "<br /> </div>");
                    $("#monthly-wall-row-content").html(content);
           //       options.labels[index]  options.data[index]
                    // return(content);
                },
                //data : d,
			  xkey: 'when',
			  // A list of names of data record attributes that contain y-values.
			  ykeys: resp.keys,
			  // Labels for the ykeys -- will be displayed when you hover over the
			  // chart.
			  labels: resp.keys // ['Value']
				            });
        },
        error: function(jqXHR, textStatus, ex) {
          console.log(textStatus + "," + ex + "," + jqXHR.responseText);
        }
    });


//     new Morris.Line({
//   // ID of the element in which to draw the chart.
//   element: 'usage-chart',
//   // Chart data records -- each entry in this array corresponds to a point on
//   // the chart.
//   data: [
//     { year: '2008', value: 20 },
//     { year: '2009', value: 10 },
//     { year: '2010', value: 5 },
//     { year: '2011', value: 5 },
//     { year: '2012', value: 20 }
//   ],
//   // The name of the data record attribute that contains x-values.
//   xkey: 'year',
//   // A list of names of data record attributes that contain y-values.
//   ykeys: ['value'],
//   // Labels for the ykeys -- will be displayed when you hover over the
//   // chart.
//   labels: ['Value']
// });





});




// data: [
//     { y: '2006', a: 100, b: 90 },
//     { y: '2007', a: 75,  b: 65 },
//     { y: '2008', a: 50,  b: 40 },
//     { y: '2009', a: 75,  b: 65 },
//     { y: '2010', a: 50,  b: 40 },
//     { y: '2011', a: 75,  b: 65 },
//     { y: '2012', a: 100, b: 90 }
//   ],