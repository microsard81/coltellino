if (!library)
   var library = {};

library.json = {
   replacer: function(match, pIndent, pKey, pVal, pEnd) {
      var key = '<span class=json-key>';
      var val = '<span class=json-value>';
      var str = '<span class=json-string>';
      var r = pIndent || '';
      if (pKey)
      	r = r + key + pKey.replace(/[": ]/g, '') + '</span>: ';
      if (pVal)
        r = r + (pVal[0] == '"' ? str : val) + pVal + '</span>';
      return r + (pEnd || '');
      },
   prettyPrint: function(obj) {
      var jsonLine = /^( *)("[\w]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;
      return JSON.stringify(obj, null, 3)
         .replace(/&/g, '&amp;').replace(/\\"/g, '&quot;')
         .replace(/</g, '&lt;').replace(/>/g, '&gt;')

         .replace(jsonLine, library.json.replacer);
         //.replace(/\n/g, '<br/>');
      }
   };

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="details-' + cls + '">' + match + '</span>';
    });
}




$(document).ready( function() {

  
	$('.dataTables-usage').DataTable({
			"columnDefs": [
				            {
				                "targets": [ 0 ],
				                "visible": false,
				                "searchable": false
				            },
				            {
				                "targets": -1,
				                "render": function ( data, type, row ) {
				                    // return data +' ('+ row[3]+')';

									return '<a class="btn btn-info details-btn" data-target="#ModalDetails" href="/usage/measure_details/' + row[0]+'" data-show="true" data-toggle="modal">Details</a>';

				                },
				                // "data": null,
            					// "defaultContent": "<button>More Details</button>"
				            }
				           ]});

	// $( ".dataTables-usage tbody" ).on( "click", "tr", function() {
	// 	console.log( $( this ).text() );
	// 	});

	$('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();

 		table = $('#'+$('a.toggle-vis').attr('data-ref')).DataTable();

        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
 
        // Toggle the visibility
        column.visible( ! column.visible() );
    } );

	// $('.dataTables-usage tbody').on( 'click', 'button', function () {
	// 	table = $('#'+$('a.toggle-vis').attr('data-ref')).DataTable();
 //        var data = table.row( $(this).parents('tr') ).data();
 //        alert( data[0] +"'s salary is: "+ data[ 5 ] );
 //    } );



	$('.details-btn').on('click', function(){
		var infoModal = $('#ModalDetails');
	        $.ajax({ 
	          type: "GET", 
	          // url: 'getJson.php?id='+$(this).data('id'),
	          url: $(this).attr('href'),
	          dataType: 'json',
	          success: function(data){
	          	clean_details = JSON.stringify(data.details, null, '   ');
	          	// htmlData = '<div>'+clean_details+'</div>';
	            // htmlData = '<div>'+syntaxHighlight(clean_details)+'</div>';
	            // htmlData = '<div>'+ library.json.prettyPrint(clean_details)+'</div>';
	            htmlData = '<pre>'+ library.json.prettyPrint(data.details)+'</pre>';
	            
	            infoModal.find('.modal-body').html(htmlData);
	            infoModal.modal('show');
	          }
	        });

	        return false;
	    });
});