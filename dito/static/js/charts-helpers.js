function line_custom_prototype(index) {
      var content, j, row, y, _i, _len, _ref;
      row = this.data[index];
      content = "<div class='morris-hover-row-label'>" + row.label + "</div><div class='morris-data-columns'>";
      _ref = row.y;
      for (j = _i = 0, _len = _ref.length; _i < _len; j = ++_i) {
        y = _ref[j];
        content += "<div class='morris-hover-point' style='color: " + (this.colorFor(row, j, 'label')) + "'>\n  " + this.options.labels[j] + ":\n  " + (this.yLabelFormat(y)) + "\n</div>";
      }
      content += "</div>";
      if (typeof this.options.hoverCallback === 'function') {
        content = this.options.hoverCallback(index, this.options, content, row.src);
      }
      
      return [content, row._x, row._ymax];
    };




$(document).ready( function() {

	PLAN_ID = $('#plan-id').text().trim();
  SUBSCRIPTION_ID = $('#subscription-id').text().trim();

	$.ajax({
		// catturare dinamicamente l'url
        url: "/resume_data/get_data/interval/30/plan/" + PLAN_ID + "/subscription/" + SUBSCRIPTION_ID,
        type: "get",
        dataType: "json",
        timeout:3000,
        success : function (resp){


			var b = Morris.Line;
			
			b.prototype.hoverContentForRow = line_custom_prototype;
			var a = b({
			                element: 'usage-slots-daily-chart',
			                data: resp.slots,
							hoverCallback: function(index, options, content) {
			
			                    $("#daily-slots-row-content").html(content);
			
						    },
			                			                
						  xkey: 'when',
						  // A list of names of data record attributes that contain y-values.
						  ykeys: resp.keys,
						  // Labels for the ykeys -- will be displayed when you hover over the
						  // chart.
						  labels: resp.keys, // ['Value'],

						  // hoverContentForRow : line_custom_prototype
			            });
			    // a.hoverContentForRow = line_custom_prototype;
				
        },
        error: function(jqXHR, textStatus, ex) {
          console.log(textStatus + "," + ex + "," + jqXHR.responseText);
        }
    });




$.ajax({
        url: "/resume_data/get_data/interval/365/plan/" + PLAN_ID + "/subscription/" + SUBSCRIPTION_ID,
        type: "get",
        dataType: "json",
        timeout:3000,
        success : function (resp){
            Morris.Line({
                element: 'usage-slots-monthly-chart',
                data: resp.slots,
                hoverCallback: function(index, options, content) {
                    $("#monthly-slots-row-content").html(content);
           //       options.labels[index]  options.data[index]
                    // return(content);
                },
                //data : d,
			  xkey: 'when',
			  // A list of names of data record attributes that contain y-values.
			  ykeys: resp.keys,
			  // Labels for the ykeys -- will be displayed when you hover over the
			  // chart.
			  labels: resp.keys // ['Value']
				            });
        },
        error: function(jqXHR, textStatus, ex) {
          console.log(textStatus + "," + ex + "," + jqXHR.responseText);
        }
    });


$.ajax({
        url: "/resume_data/get_data/interval/365/plan/" + PLAN_ID + "/subscription/" + SUBSCRIPTION_ID,
        type: "get",
        dataType: "json",
        timeout:3000,
        success : function (resp){
            Morris.Line({
                element: 'usage-wall-monthly-chart',
                data: resp.wall,
                hoverCallback: function(index, options, content) {
//                  $("#row-content").html("<div>" + "<br />" + options.labels[index] + ": " + options.data[index] + "<br /> </div>");
                    $("#monthly-wall-row-content").html(content);
           //       options.labels[index]  options.data[index]
                    // return(content);
                },
                //data : d,
			  xkey: 'when',
			  // A list of names of data record attributes that contain y-values.
			  ykeys: resp.keys,
			  // Labels for the ykeys -- will be displayed when you hover over the
			  // chart.
			  labels: resp.keys // ['Value']
				            });
        },
        error: function(jqXHR, textStatus, ex) {
          console.log(textStatus + "," + ex + "," + jqXHR.responseText);
        }
    });



});


