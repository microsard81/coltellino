$(document).ready( function() {

  $('#confirm-unsubscribe').on('show.bs.modal', function(e) {
      $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
  });
});
