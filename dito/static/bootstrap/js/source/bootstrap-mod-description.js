/*
** Luca Carta - luca@crs4.it
**/

var description = description || (function ($) {
	'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'+
    	'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
		'	<div class="modal-header">' +
		'    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '    <b>Current description:</b>&nbsp;<il></il>' + 
        '	</div>' +
		'	<form method="post" action="/setDesc">'+
		'	<div class="modal-body">' +
		'		<div class="form-group">' +
        '   	 <input type="text" class="form-control" name="desc" placeholder="Type here a new description">' + 
        '   	 <ih1></ih1>' +
        '		</div>' +
        '	</div>' +
        '	<div class="modal-footer">' +
        '		<button type="submit" class="btn btn-primary">Submit</button>' +
        '		<button type="reset" class="btn btn-default">Reset</button>' +
        '	</div>' +
        '	</form>' +
        '</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (host, port, old_desc) {
			// Assigning defaults
			if (typeof host === 'undefined') {
				host = 'Loading';
			}
			if (typeof port === 'undefined') {
				port = '';
			}
			if (typeof old_desc === 'undefined') {
				old_desc = 'Change description';
			}
			// Configuring dialog
			$dialog.find('il').text(old_desc);
			$dialog.find('ih1').html('<input type="hidden" class="form-control" name="port" value="' + port + '">' +
			'<input type="hidden" class="form-control" name="host" value="' + host + '">' +
			'<input type="hidden" class="form-control" name="next" value="/">');
			// Opening dialog
			$dialog.modal();
			//
			setTimeout(function(){
    			//do what you need here
    			document.getElementsByName("desc")[0].focus();
			}, 500);
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);
