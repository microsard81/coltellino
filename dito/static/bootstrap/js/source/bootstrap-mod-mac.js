/*
** Luca Carta - luca@crs4.it
**/


var mac = mac || (function ($) {
	'use strict';

	// Creating modal dialog's DOM
	var $dialog;

	return {
		show: function (mac) {
			// Assigning defaults
			$dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'+
    	'	<div class="modal-dialog modal-m">' +
		'		<div class="modal-content">' +
		'			<div class="modal-header">' +
		'		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '		    Vendor by MAC:&nbsp;<b><il></il></b></h4>' + 
        '			</div>' +
		'			<div class="modal-body scroll-bar">' +
		'				<div id="bar" class="progress progress-striped active iModal" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%">Loading...</div></div>' +
        '			</div>' +
        '			<div class="modal-footer">' +
        '				<div style="position: relative; right: -110px;">Provided by <img src="/static/img/MacVendors.png"></div>' +
        '			</div>' +
        '		</div>'+
        '	</div>'+
        '</div>');
			var toGet = "/getMacVendor?mac="+mac;
			// Configuring dialog
			$dialog.find('il').text(mac);
			// Opening dialog
			$dialog.modal();
			$.getJSON(toGet, function(result){
				if (!result.error) {
					$('div.iModal').replaceWith('<div>' + result['result'] + '</div>');
				}
				else {
					$('div.iModal').replaceWith('<div>Error: ' + result['error'] + '</div>');
				}
			});
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);
