/*
** Luca Carta - luca@crs4.it
**/


var interf = interf || (function ($) {
	'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal modal-wide fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'+
    	'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
		'	<div class="modal-header">' +
		'    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '    Port detail for&nbsp;<b><il></il></b></h4>' + 
        '	</div>' +
		'	<div class="modal-body scroll-bar">' +
		'		<div id="bar" class="progress progress-striped active iModal" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%">Loading...</div></div>' +
        '	</div>' +
        '</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (host, port) {
			// Assigning defaults
			$dialog = $(
		'<div class="modal modal-wide fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'+
    	'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
		'	<div class="modal-header">' +
		'    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '    Port detail for&nbsp;<b><il></il></b></h4>' + 
        '	</div>' +
		'	<div class="modal-body scroll-bar">' +
		'		<div id="bar" class="progress progress-striped active iModal" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%">Loading...</div></div>' +
        '	</div>' +
        '</div></div></div>');
			var toGet = "/getPort?host="+host+"&port="+port;
			if (typeof host === 'undefined') {
				host = '';
			}
			if (typeof port === 'undefined') {
				port = host;
				toGet = "/getPort?host="+host+"&port="
			}
			//console.log(port);
			// Configuring dialog
			$dialog.find('il').text(port);
			// Opening dialog
			$dialog.modal();
			$.getJSON(toGet, function(result){
				$('div.iModal').replaceWith('<pre>' + result['result'] + '</pre>');
			});
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);
