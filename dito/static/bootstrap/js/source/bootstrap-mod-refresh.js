/*
** Luca Carta - luca@crs4.it
**/


var refresh = refresh || (function ($) {
	'use strict';

	// Creating modal dialog's DOM
	var $dialog;

	return {
		show: function () {
			// Assigning defaults
			$dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'+
		'	<div class="modal-dialog modal-m">' +
		'		<div class="modal-content">' +
		'			<div class="modal-header">' +
		'		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
		'		    <b>ARP DB Refresh</b>' + 
		'			</div>' +
		'			<div class="modal-body scroll-bar">' +
		'				<div id="bar" class="progress progress-striped active rModal" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%">Collecting...</div></div>' +
		'			</div>' +
		'			<div class="modal-footer">' +
		'				<div style="position: relative; right: -110px;">&nbsp;</div>' +
		'			</div>' +
		'		</div>'+
		'	</div>'+
		'</div>');
			var toGet = "/refresh";
			// Configuring dialog
			// Opening dialog
			$dialog.modal();
			$.getJSON(toGet, function(result){
				if (result.error) {
					$('div.rModal').replaceWith('<div>Error: ' + result['error'] + '</div>');
				}
				else {
					$('div.rModal').replaceWith('<div id="bar" class="progress progress-striped progress-bar-danger active rModal" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%">Done</div></div>');
					document.location.reload();
				}
			});
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		}
	};

})(jQuery);
