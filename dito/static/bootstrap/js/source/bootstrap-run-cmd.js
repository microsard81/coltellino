/*
** Luca Carta - luca@crs4.it
**/


var runCmd = runCmd || (function ($) {
	'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal modal-wide fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'+
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
		'	<div class="modal-header">' +
		'	 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
		'	 <b>Device interaction</b>' + 
		'	</div>' +
		'	<div class="modal-body scroll-bar">' +
		'	 <form role="search" id="commandForm" method="post">' +
		'	 	<div class="input-group">' +
		'	 		<input name="device" value="" type="hidden" id="deviceItem" class="form-control">' +
		'	 		<input placeholder="Digit command here..." name="command" type="text" class="form-control" id="commandItem">' +
		'	 		<span class="input-group-btn">' +
		'				<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>' +
		'			</span>' +
		'	 	</div>' +
		'	 </form>' +
		'		<pre></pre>' +
		'	</div>' +
		'</div></div></div>');

	return {
		/**
		 * Show dialog
		 */
		show: function (host) {
			// Configuring dialog
						$dialog = $(
		'<div class="modal modal-wide fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'+
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
		'	<div class="modal-header">' +
		'	 <form role="search" id="commandForm" method="post">' +
		'	 	<div class="input-group">' +
		'	 		<input placeholder="Digit command here..." name="command" type="text" class="form-control" id="commandItem">' +
		'	 		<span class="input-group-btn">' +
		'				<button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Run"><i class="fa fa-terminal"></i></button>' +
		'				<button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="tooltip" data-placement="right" title="Close"><i class="fa fa-times"></i></button>' +
		'			</span>' +
		'	 	</div>' +
		'		<input name="device" value="'+ host +'" type="hidden" id="deviceItem" class="form-control">' +
		'	 </form>' +
		'	</div>' +
		'	<div class="modal-body scroll-bar">' +
		'	 <pre></pre>' +
		'	</div>' +
		'</div></div></div>');
			// Opening dialog
			$dialog.modal();
			// Customize form submit event
			$dialog.find('form').submit(function (e) {
				e.preventDefault();
				runCmd.load();
			});
			setTimeout(function(){
				document.getElementsByName("command")[0].focus();
			}, 500);
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		},
		/**
		 * Load dialog content
		 */
		load: function () {
			var toGet = "/showCmd";
			$('pre').replaceWith('<div class="progress progress-striped active rModal" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"><i class="fa fa-spinner fa-spin"></i> Loading...</div></div>')
			var dataToBeSent = $dialog.find('form').serialize();
			console.log(dataToBeSent);
			$.post(toGet, dataToBeSent, function(result){
				var res = result;
				if (res['errors'].length > 0 ) {
					$('div.rModal').replaceWith('<pre>' + res['errors'] + '</pre>');
				} else {
					$('div.rModal').replaceWith('<pre>' + res['result'] + '</pre>');
				}
			}, "json");
		}
	};

})(jQuery);