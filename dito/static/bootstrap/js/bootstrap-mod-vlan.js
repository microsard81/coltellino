/*
** Luca Carta - luca@crs4.it
**/

var vlan = vlan || (function ($) {
	'use strict';

	// Creating modal dialog's DOM
	var $dialog ;
	var table;
	
	$.post("/getVlanSummary", "", function(result){
		var res = result['results'];
		var to_w;
		to_w = '<table class="table table-hover">' +
		'<thead><tr>' +
		'<th width="10%">Tag</th><th width="60%">Description</th><th width="30%">Net</th>'+
		'</tr></thead></table><div style="height: 15vh; overflow-y: scroll;"><table class="table table-hover"><tbody>';
		$.each(res, function(value) { 
			to_w = to_w + '<tr class="drag" style="cursor: copy;" id="' + res[value].tag + '">' +
			'<th width="10%">'+ 
			res[value].tag +
			'</th><td width="60%">'+ 
			res[value].description +
			'</td><td width="30%">'+ 
			res[value].net +
			'</td>' +
			'</tr>'; 
		});
		to_w = to_w + '</tbody></table></div>';
		table = to_w;
		//
		//
	}, "json");

	return {
		/**
		 * Show dialog
		 */
		show: function (host) {
			if (typeof $('#modal-'+host).data() !== "undefined") {
				$('#modal-'+host).modal('toggle');
				return;
			}
			var dataToBeSent = "device="+host;
			$dialog = $(
		'<div id="modal-' + host + '" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible; margin-top: -25vh;">'+
		'<div class="modal-dialog modal-m">' +
		'   <div class="modal-content">' +
		'       <div class="modal-header">' +
		'           <button type="button" class="close" aria-label="Close" onClick="javascript: vlan.hide(\'' + host + '\')"><span aria-hidden="true">&times;</span></button>' +
		'           <strong>' + host + ' vlan settings</strong>' + 
		'       </div>' +
		'       <form action="/">' +
		'       <input type="hidden" name="device" value="' + host + '">' +
		'       <div class="modal-body scroll-bar">' +
		'           <a data-toggle="collapse" href="#' + host + '" aria-expanded="false" aria-controls="' + host + '">' +
		'             Show Vlans list' +
		'           </a>' +
		'           <div>&nbsp;</div>' +
		'           <div class="collapse" id="' + host + '">' +
		'             <div class="mia panel panel-default" style="overflow-y: hidden; color: #31708f; background-color: #d9edf7; padding-left: 5px; padding-right: 5px;">' +
		'               ' + table +
		'             </div>' +
		'           </div>' +
		'           <div class="form-group">' +
		'               <label for="vlan-untag'+host+'">Vlan untagged</label>' +
		'               <select name="vlan-untag" id="vlan-untag'+host+'" class="vlan-untag'+host+' form-control" multiple="multiple">'+
		'                   <option></option>' +
		'               </select>' +
		'           </div>' +
		'           <div class="form-group">' +
		'               <label for="vlan-tag'+host+'">Vlan tagged</label>' +
		'               <select name="vlan-tag" id="vlan-tag'+host+'" class="vlan-tag'+host+' form-control" multiple="multiple">'+
		'                   <option></option>' +
		'               </select>' +
		'           </div>' +
		'           <div class="form-group">' +
		'               <label for="port'+host+'">Port</label>' +
		'               <select name="port" id="port'+host+'" class="port'+host+' form-control" multiple="multiple">'+
		'                   <option></option>' +
		'               </select>' +
		'           </div>' +
		'       </div>' +
		'       <div class="modal-footer">' +
		'           <div style="width: 72%; height: 10px;"><div id="load" class="progress progress-striped active vModal"><div class="progress-bar" style="width: 100%"><i class="fa fa-spinner fa-spin"></i> Loading content...</div></div></div>' +
		'           <button type="reset" class="btn btn-default reset" style="margin-top: -21px;">Reset</button>' +
		'           <button type="submit" class="btn btn-primary" style="margin-top: -21px;">Submit</button>' +
		'       </div>' +
		'       </form>' +
		'   </div>' +
		'</div>'+
		'</div>');
			//
			// Opening dialog
			$dialog.modal();
			//
			setTimeout(function(){
				$('select.vlan-untag'+host).select2({ 
					placeholder: "Select a vlan",
					maximumSelectionLength: 1
				}).prop("disabled", true);
				$('select.vlan-tag'+host).select2({ 
					placeholder: "Select vlans"
				}).prop("disabled", true);
				$('select.port'+host).select2({ 
					placeholder: "Select a port",
					maximumSelectionLength: 1
				});
			}, 500);
			setTimeout(function(){
				$('select.vlan-untag'+host).on('select2:select', function(e) {
					var ind = e.params.data.element.index;
					$('select.vlan-tag'+host).select2({placeholder: "Select vlans"})[0][ind].disabled = true;
				});
				$('select.vlan-untag'+host).on('select2:unselect', function(e) {
					var ind = e.params.data.element.index;
					$('select.vlan-tag'+host).select2({placeholder: "Select vlans"})[0][ind].disabled = false;
				});
				$('select.vlan-tag'+host).on('select2:select', function(e) {
					var ind = e.params.data.element.index;
					$('select.vlan-untag'+host).select2({placeholder: "Select a vlan", maximumSelectionLength: 1})[0][ind].disabled = true;
				});
				$('select.vlan-tag'+host).on('select2:unselect', function(e) {
					var ind = e.params.data.element.index;
					$('select.vlan-untag'+host).select2({placeholder: "Select a vlan", maximumSelectionLength: 1})[0][ind].disabled = false;
				});
				$('select.port'+host).on('select2:select', function(e) {
					$('select.vlan-tag'+host).prop("disabled", false);
					$('select.vlan-untag'+host).prop("disabled", false);
					var ind = e.params.data.element.text;
					$('div.vModal').addClass('progress-striped active');
					$('div.progress-bar').replaceWith('<div class="progress-bar" style="width: 100%"><i class="fa fa-spinner fa-spin"></i> Loading port detail...</div>');
					$('div.vModal').fadeIn("slow");
					$.post("getPortVlan", $dialog.find('form').serialize(), function(res) {
						if (res['errors'].length > 0 ) {
							$('div.vModal').removeClass('progress-striped active');
							$('div.progress-bar').replaceWith('<div class="progress-bar progress-bar-danger" style="width: 100%; text-align: left; padding-left: 10px;"><i>' + res['errors'] + '</i></div>');
							setTimeout(function(){
								$('div.progress').fadeOut("slow");
							}, 5000);
						} else {
							if (res.result[0][ind].tagged) {
								$.each(res.result[0][ind].tagged, function(value){
									$('select.vlan-untag'+host+' option[value=' + res.result[0][ind].tagged[value] + ']').attr("disabled","disabled");
								});
								$('select.vlan-untag'+host).select2({placeholder: "Select a vlan", maximumSelectionLength: 1});
								$('select.vlan-tag'+host).val(res.result[0][ind].tagged);
								$('select.vlan-tag'+host).select2({placeholder: "Select vlans"});           
							}
							if (res.result[0][ind].untagged) {
								$('select.vlan-tag'+host+' option[value=' + res.result[0][ind].untagged[0] + ']').attr("disabled","disabled");
								$('select.vlan-tag'+host).select2({placeholder: "Select vlans"});
								$('select.vlan-untag'+host).val(res.result[0][ind].untagged[0]).trigger("change");
							}
							$('div.vModal').fadeOut("slow");
						}
					}, "json");
				});
				$('select.port'+host).on('select2:unselect', function(e) {
					$('select.vlan-untag'+host).val(null).trigger("change"); 
					$('select.vlan-tag'+host).val(null).trigger("change"); 
					$('select.vlan-untag'+host).prop("disabled", true);
					$('select.vlan-tag'+host).prop("disabled", true);
				});
				var c = {};
				$('tr.drag').draggable({
					helper: 'clone',
					stack: '.drag',
					start: function(event, ui) {
						c.tr = this;
						c.helper = ui.helper;
					}
				});
				$(".form-group").droppable({
					drop: function(event, ui) {
						var toW, inventor = ui.draggable.attr('id'), oldValue = $(this).find('select').val();
						var elId = $(this).find('select')[0].id;
						//
						if (elId.indexOf("vlan-untag") > -1) {
							$('select.vlan-tag'+host+' option[value=' + inventor + ']').attr("disabled","disabled");
							$('select.vlan-tag'+host).select2({placeholder: "Select vlans"});
						} else if (elId.indexOf("vlan-tag") > -1) {
							$('select.vlan-untag'+host+' option[value=' + inventor + ']').attr("disabled","disabled");
							$('select.vlan-untag'+host).select2({placeholder: "Select a vlan", maximumSelectionLength: 1});
						} else {
							$(c.helper).remove();
							return;
						}
						if (oldValue == null){ 
							toW = inventor;
						} else {
							toW = oldValue.concat(inventor);
						}
						$(this).find('select').val(toW).trigger("change");
						$(c.helper).remove();
					}
				});
			}, 200)
			//
			$.post("/getVlan", dataToBeSent, function(result){
				var res = result;
				if (res['errors'].length > 0 ) {
					$('select.vlan-tag'+host).replaceWith('<div class="alert alert-danger" role="alert">' + res.result.errors + '</div>');
					$('select.vlan-untag'+host).replaceWith('<div class="alert alert-danger" role="alert">' + res.result.errors + '</div>');
					$('select.port'+host).replaceWith('<div class="alert alert-danger" role="alert">' + res.result.errors + '</div>');
				} else {
					$.each(res['result']['vlans'], function(value) {   
						$('select.vlan-tag'+host)
							.append($("<option></option>")
							.attr("value", res['result']['vlans'][value])
							.text(res['result']['vlans'][value])); 
						$('select.vlan-untag'+host)
							.append($("<option></option>")
							.attr("value", res['result']['vlans'][value])
							.text(res['result']['vlans'][value]));
					});
					$.each(res['result']['ports'], function(value) {
						$('select.port'+host)
							.append($("<option></option>")
							.attr("value", res['result']['ports'][value])
							.text(res['result']['ports'][value])); 
						$('div.vModal').fadeOut('slow');
					});
				}
			}, "json");
			
			// Customize form submit event
			$dialog.find('form').submit(function (e) {
				e.preventDefault();
				vlan.load();
				//e.submit();
			});

			// Customize form reset event
			$dialog.find('form').bind('reset', function (e) {
				$.each($('select.vlan-untag'+host).select2({placeholder: "Select a vlan", maximumSelectionLength: 1})[0], function(value) {
					$('select.vlan-untag'+host).select2({placeholder: "Select a vlan", maximumSelectionLength: 1})[0][value].disabled = false;
				});
				$.each($('select.vlan-tag'+host).select2({placeholder: "Select vlans"})[0], function(value) {
					$('select.vlan-tag'+host).select2({placeholder: "Select vlans"})[0][value].disabled = false;
				});
				$('select.vlan-untag'+host).val(null).trigger("change"); 
				$('select.vlan-tag'+host).val(null).trigger("change"); 
				$('select.port'+host).val(null).trigger("change");
				$('select.vlan-untag'+host).prop("disabled", true);
				$('select.vlan-tag'+host).prop("disabled", true);
			});
		},
		/**
		 * Closes dialog
		 */
		hide: function (host) {
			$('#modal-'+host).modal('toggle');
		},
		/**
		 * Load dialog content
		 */
		load: function () {
			$('div.progress').addClass('progress-striped active');
			$('div.progress').fadeIn("slow");
			$('div.progress-bar').replaceWith('<div class="progress-bar" style="width: 100%"><i class="fa fa-spinner fa-spin"></i> Configuring...</div>');
			var toGet = "/setVlan";
			var dataToBeSent = $dialog.find('form').serialize();
			$.post(toGet, dataToBeSent, function(result){
				var res = result;
				if (res['errors'].length > 0 ) {
					$('div.vModal').removeClass('progress-striped active');
					$('div.progress-bar').replaceWith('<div class="progress-bar progress-bar-danger" style="width: 100%; text-align: left; padding-left: 10px;"><i>' + res['errors'] + '</i></div>');
				} else {
					$('div.vModal').removeClass('progress-striped active');
					$('div.progress-bar').replaceWith('<div class="progress-bar progress-bar-success" style="width: 100%; text-align: left; padding-left: 10px;"><i>' + res['result'] + '</i></div>');
				}
				setTimeout(function(){
					$('div.vModal').fadeOut("slow");
				}, 5000);
			}, "json");
		}
	};

})(jQuery);