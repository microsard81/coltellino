#!/root/Envs/ARP/bin/python
#

import simplejson
import base64
import simpleldap
import struct
import socket
import SRX as srx
import brocade as big
import extreme
import c300
import cisco
import hp
import threading
import ping
import NetDevices
import sys
import keyring
import re
import ipaddress
import urllib2

sys.path.append("/root/scripts")
import arp_verify

from timeit import timeit
from Queue import Queue
from collections import OrderedDict
from datetime import *
from flask import Flask, render_template, flash, session, redirect, url_for, g, request, make_response
from flask import jsonify, Markup
from flask.ext.login import LoginManager, login_user, UserMixin, logout_user, current_user, login_required
from wtforms import TextField, PasswordField, validators
from flask.ext.wtf import Form
from functools import wraps
from pymongo import MongoClient
from OpenSSL import SSL


DEBUG = True
SECRET_KEY = 'secret'
LOGFILE = "/var/log/coltellino.log"

app = Flask(__name__)
app.debug = DEBUG
app.config.from_object(__name__)
app.permanent_session_lifetime = timedelta(minutes=20)


login_manager = LoginManager()
login_manager.login_view = "/login"
login_manager.init_app(app)

ldapsrv = 'ldapcluster.crs4.it'
basedn = 'ou=People,dc=crs4'
groupdn = 'cn=dns,ou=alt_groups,dc=crs4'

cisco_dev = ['catddn-down',
			'catddn-up']
extreme_dev = ['feun']
force10_dev = ['louisiane', 
			'quebec']
brocade_dev = ['bora', 
			'maestrale', 
			'fws-distretti', 
			'dr-backup', 
			'kamsin', 
			'genotyping', 
			'genotyping-2', 
			'poe-vt',
			'poe-cr',
			'zefiro',
			'scirocco']
hp_dev = ['amministrazione',
			'hp2626-dmz',
			'hp2828clusterup',
			'hp2848clusterdown']
h3c_dev = ['medusa-sw']


keyring.core.get_keyring().keyring_key = "4hbd$spd"
dev_username = keyring.get_password("Devices", "username")
dev_password = keyring.get_password("Devices", "password")
dev_secret = keyring.get_password("Devices", "secret")


class LoginForm(Form):
	username = TextField("username", [validators.Length(min=2, max=25)])
	password = PasswordField('password', [validators.Required()])


class User(UserMixin):
	def __init__(self, uid=None, name=None, passwd=None):

		self.active = False

		ldapres = ldap_fetch(uid=uid, name=name, passwd=passwd)

		if ldapres is not None:
			self.name = ldapres['name']
			self.id = ldapres['id']
			self.pname = ldapres['pname']
			# assume that a disabled user belongs to group 404
			if ldapres['gid'] != 404:
				self.active = True
			self.gid = ldapres['gid']

	def is_active(self):
		return self.active

	def get_id(self):
		return self.id


@login_manager.user_loader
def load_user(userid):
	return User(uid=userid)


@app.route("/login", methods=["GET", "POST"])
def login():
	form = LoginForm(request.form)
	if request.method == 'POST' and form.validate():
		user = User(name=form.username.data, passwd=form.password.data)
		if user.active is not False:
			login_user(user)
			flash(Markup("Welcome back <b>%s</b>" % user.pname))
			reLog(user.pname + ' - Login')
			response = make_response(redirect(request.args.get('next') or url_for("arp")))
			response.set_cookie('UserName',value=user.pname)				
			return response
		else:
			flash("Username or password you entered is not valid. Please try again")
	if current_user.is_authenticated():
		return redirect(request.args.get('next') or url_for("arp"))
	return render_template("login.html", form=form)


@app.route("/logout", methods=["GET", "POST"])
@login_required
def logout():
	user = request.cookies.get('UserName')
	reLog(user + ' - Logout')
	logout_user()
	response = make_response(redirect(url_for("login")))
	response.set_cookie('UserName', '', expires=0)
	return response


@app.route("/")
def index():    
	return redirect(url_for("login"))


@app.route("/arp")
@login_required
def arp(form=None, stream=None):
	"""
		Get data from MongoDB and put them in "index.html" template.
	"""
	#import pudb
	#pu.db
	start = datetime.now()
	data = fetch_data()
	#
	if type(data) != dict:
		stop = datetime.now()
		reLog("/arp - Execution time: " + str(stop - start))
		return render_template("index.html", data=data)


@app.route("/desc", methods=["GET"])
@login_required
def desc():
	"""
		Get description and port by MAC and put them in "desc.html" template.
	"""
	start = datetime.now()
	mac = request.args.get("mac")
	ip = request.args.get("ip")
	res = ping.quiet_ping(str(ip.split()[0]), timeout = 0, count = 1, psize = 64)
	data, errors = portDescGet(mac)
	#
	if (len(errors)<4) or (not "error" in data):
		stop = datetime.now()
		reLog("/desc - Execution time: " + str(stop - start))
		return render_template("desc.html", data=data, ip=ip)
	else:
		reLog(errors)
		reLog(data)
		return redirect(url_for("logout"))


@app.route("/setDesc", methods=["POST"])
@login_required
def setDesc():
	"""
		Set port description and reload last page.
	"""
	start = datetime.now()
	_dict = {}
	_dict['port'] = request.form['port']
	_dict['host'] = request.form['host'].lower()
	_dict['desc'] = request.form['desc']
	_dict['real-host'] = request.form['host']
	url = request.form['next']
	#
	if _dict['host'] in cisco_dev:
		toW, errors = descCisco(_dict)
	elif _dict['host'] in extreme_dev:
		toW, errors = descExtreme(_dict)
	elif _dict['host'] in force10_dev:
		toW, errors = descForce10(_dict)
	elif _dict['host'] in hp_dev:
		toW, errors = descHP(_dict)
	else:
		toW, errors = descBrocade(_dict)
	#
	if not errors:
		flash(Markup(toW))
		stop = datetime.now()
		reLog("/set - Execution time:" + str(stop - start))
		reLog(request.cookies.get("UserName") + " - " + str(toW))
		return redirect(url or url_for("arp"))
	else:
		reLog(errors)
		return redirect(url_for("logout"))


@app.route("/getPort", methods=["GET"])
@login_required
def getPort():
	"""
		Set port description and reload last page.
	"""
	start = datetime.now()
	_dict = {}
	_dict['port'] = request.args.get('port')
	_dict['host'] = request.args.get('host').lower()
	#
	if _dict['host'] in cisco_dev:
		toW, errors = portDetCisco(_dict)
	elif _dict['host'] in extreme_dev:
		toW, errors = portDetExtreme(_dict)
	elif _dict['host'] in force10_dev:
		toW, errors = portDetForce10(_dict)
	elif _dict['host'] in hp_dev:
		toW, errors = portDetHP(_dict)
	else:
		toW, errors = portDetBrocade(_dict)
	#toW, errors = portDetBrocade(_dict)
	#
	res = {}
	res['errors'] = errors
	res['result'] = toW
	stop = datetime.now()
	reLog("/getPort - Execution time:" + str(stop - start))
	return jsonify(**res)


@app.route("/showCmd", methods=["POST", "GET"])
@login_required
def showCmd():
	"""
		Show device command.
	"""
	start = datetime.now()
	_dict = {}
	_dict['command'] = request.form['command']
	_dict['host'] = request.form['device'].lower()
	#
	if _dict['command'][0:2] != "sh":
		res = {}
		res['errors'] = ["Only show commands are permitted!"]
		res['result'] = ""
		return jsonify(**res)
	#
	if _dict['host'] in cisco_dev:
		toW, errors = runCmdCisco(_dict)
	elif _dict['host'] in extreme_dev:
		toW, errors = runCmdExtreme(_dict)
	elif _dict['host'] in force10_dev:
		toW, errors = runCmdForce10(_dict)
	elif _dict['host'] in hp_dev:
		toW, errors = runCmdHP(_dict)
	elif _dict['host'] in h3c_dev:
		toW, errors = runCmdH3C(_dict)
	else:
		toW, errors = runCmdBrocade(_dict)
	#
	res = {}
	res['errors'] = errors
	res['result'] = "\n".join(toW)
	stop = datetime.now()
	reLog("/showCmd - Execution time:" + str(stop - start))
	return jsonify(**res)


@app.route("/getVlan", methods=["POST", "GET"])
@login_required
def getVlan():
	"""
		Get Device vlans.
	"""
	start = datetime.now()
	_dict = {}
	try:
		_dict['host'] = request.args.get('device').lower()
	except:
		_dict['host'] = request.form['device'].lower()
	#
	#
	toW, toW1, errors = vlan(_dict)
	#
	res = {}
	res['errors'] = errors
	
	toW = sorted(toW, key=lambda x: int(x))
	res['result'] = {'vlans': toW, 'ports': toW1}

	stop = datetime.now()
	reLog("/getVlan - Execution time:" + str(stop - start))
	return jsonify(**res)

@app.route("/setVlan", methods=["POST"])
@login_required
def setVlan():
	"""
		Set Vlan port.
	"""
	start = datetime.now()
	_dict = {}
	_dict['vlan-tag'] = ""
	_dict['vlan-untag'] = ""
	_dict['host'] = ""
	#
	if not 'port' in request.form.keys():
		return jsonify(**{'errors': 'Port not specified'})
	if not 'device' in request.form.keys():
		return jsonify(**{'errors': 'Device not specified'})
	if not 'vlan-tag' in request.form.keys() and not 'vlan-untag' in request.form.keys():
		return jsonify(**{'errors': 'You have to specify at least a Vlan tagged/untagged'})
	#
	if 'vlan-tag' in request.form.keys():
		_dict['vlan-tag'] = request.form.getlist('vlan-tag')
	if 'vlan-untag' in request.form.keys():
		_dict['vlan-untag'] = request.form['vlan-untag']
	_dict['host'] = request.form['device'].lower()
	_dict['port'] = request.form['port']
	#
	#
	toW, errors = vlanSet(_dict)
	#
	#toW = _dict
	#errors = ""
	#
	res = {}
	res['errors'] = errors
	res['result'] = toW
	stop = datetime.now()
	reLog("/setVlan - Execution time:" + str(stop - start))
	reLog(request.cookies.get("UserName") + " - " + str(res['result']))
	return jsonify(**res)

@app.route("/getPortVlan", methods=["POST", "GET"])
@login_required
def getPortVlan():
	"""
		Get Device port vlan.
	"""
	start = datetime.now()
	_dict = {}
	try:
		_dict['host'] = request.args.get('device').lower()
		_dict['port'] = request.args.get('port')
	except:
		_dict['host'] = request.form['device'].lower()
		_dict['port'] = request.form['port']
	#
	#
	toW, errors = portsVlan(_dict)
	#
	res = {}
	res['errors'] = errors
	res['result'] = [toW]
	stop = datetime.now()
	reLog("/getPortVlan - Execution time:" + str(stop - start))
	return jsonify(**res)

def ldap_fetch(uid=None, name=None, passwd=None):
	try:
		if name is not None and passwd is not None:
			l = simpleldap.Connection(ldapsrv,
				dn='uid={0},{1}'.format(name, basedn), password=passwd)
			r = l.search('uid={0}'.format(name), base_dn=basedn)
			_dn='uid={0},{1}'.format(name, basedn)
			s = l.search('uniqueMember={0}'.format(_dn), base_dn=groupdn)
		else:
			l = simpleldap.Connection(ldapsrv)
			r = l.search('uidNumber={0}'.format(uid), base_dn=basedn)

		if s:
			return {
				'name': r[0]['uid'][0],
				'id': unicode(r[0]['uidNumber'][0]),
				'gid': int(r[0]['gidNumber'][0]),
				'pname': r[0]['cn'][0]
			}
		else:
			return None
	except:
		return None


def fetch_data():
	try:
		user = 'root'
		password = '4hbd$spd'
		client = MongoClient()['ARP']
		client.authenticate(user, password, mechanism='MONGODB-CR')
		collect = client.table
		arr = collect.find().sort('_id', 1)
		#
		to_g = []
		count = 0
		for i in arr:
			to_g.append(i)
		#
		_g = []
		#
		s = sorted(to_g, key=lambda x: struct.unpack("!L", socket.inet_aton(x['_id']))[0])
		for i in s:
			i['count'] = count
			_g.append(i)
			count += 1
		return _g
	except Exception, e:
		return {'error': str(e)}

@app.route("/getMacVendor", methods=["GET", "POST"])
def getMacVendor():
	try:
		try:
			MAC = request.args.get('mac').upper()
		except:
			MAC = request.form['mac'].upper()
		#
		req = urllib2.Request(url="http://api.macvendors.com/"+MAC)
		f = urllib2.urlopen(req)
		res =  { "result" : [f.read().title()] }
		return jsonify(**res)
	except Exception, e:
		if e.getcode() == 404:
			res = { "result" : ["Not found"] }
			return jsonify(**res)
		else:
			res = { "error" : [str(e)] }

@app.route("/refresh", methods=["GET", "POST"])
def refresh():
	try:
		#
		arp_verify.scan()
		res =  { "result" : ["refreshed"] }
		return jsonify(**res)
	except Exception, e:
		if e.getcode() == 404:
			res = { "result" : [str(e)] }
			return jsonify(**res)
		else:
			res = { "error" : [str(e)] }

@app.route("/getVlanSummary", methods=["GET", "POST"])
def fetch_vlan():
	"""
		Return a dict with all vlans details.
	"""
	try:
		user = 'root'
		password = '4hbd$spd'
		client = MongoClient()['ARP']
		client.authenticate(user, password, mechanism='MONGODB-CR')
		collect = client.vlans
		arr = collect.find().sort('tag', 1)
		#
		to_g = []
		for i in arr:
			to_g.append(i)
		#
		res = { "results" : to_g }
		#
		return jsonify(**res)
	except Exception, e:
		return jsonify({'error': str(e)})

@app.route("/getUnusedIP", methods=["GET"])
def getUnusedIP():
	"""
		Return a dict with unused IP address.
	"""
	start = datetime.now()
	_dict = {}
	#toW, errors = portDetBrocade(_dict)
	#
	res = old_ip()
	stop = datetime.now()
	reLog("/getUnusedIP - Execution time:" + str(stop - start))
	return jsonify(**res)

def old_ip():
	try:
		user = 'root'
		password = '4hbd$spd'
		client = MongoClient()['ARP']
		client.authenticate(user, password, mechanism='MONGODB-CR')
		collect = client.table
		arr = collect.find({'update': {'$lt': datetime.now() - timedelta(days=120)}}, {'_id': 1, 'update': 1}).sort('update', 1)
		#
		to_g = [x for x in arr]
		count = arr.count()
		#
		_g = []
		#
		# s = sorted(to_g, key=lambda x: struct.unpack("!L", socket.inet_aton(x['_id']))[0])
		s = sorted(to_g, key=lambda x: x['update'])
		for i in s:
			_g.append({'ip': i['_id'], 'last_update': i['update']})
		return {'days_before': 120, 'total': count, 'result': _g}
	except Exception, e:
		return {'error': str(e)}

@app.route("/getUsedIP", methods=["GET"])
def getUsedIP():
	"""
		Return a dict with used IP address.
	"""
	start = datetime.now()
	_dict = {}
	days = int(request.args.get('days'))
	vlan = request.args.get('vlan')
	#
	res = used_ip(days, vlan)
	stop = datetime.now()
	reLog("/getUsedIP - Execution time:" + str(stop - start))
	return jsonify(**res)

def used_ip(d, vlan):
	try:
		user = 'root'
		password = '4hbd$spd'
		client = MongoClient()['ARP']
		client.authenticate(user, password, mechanism='MONGODB-CR')
		collect = client.table
		if vlan:
			vlans = client.vlans.find().sort('tag', 1)
			vlans = { i['_id']: i['net']  for i in vlans if i['net']}
			### print {i: [str(vlans[i].split("/")[0]), ipaddress.ip_network(vlans[i]).prefixlen] for i in sorted(vlans, key=lambda x: x) if ipaddress.ip_network(vlans[i]).prefixlen < 23}
			net = ipaddress.ip_network(vlans[int(vlan)])
			prefix = net.prefixlen
			net = [ str(x) for x in list(net) ]
			#
			limit = 1
			if prefix > 8:	limit = 2
			if prefix > 16:	limit = 3
			if prefix >	24:	limit = 4
			#
			FILTER = "|".join(list(set([ "(" + "\.".join(z.split(".")[0:limit]) + "\.)" for z in net ])))
			arr = collect.find({'$and': [{ 'update': {'$gt': datetime.now() - timedelta(days=d)}}, {'_id': re.compile(FILTER, re.IGNORECASE)} ] }, {'_id': 1, 'update': 1}).sort('_id', 1)
			to_g = [ x for x in arr if x['_id'] not in [ str(z) for z in net[:10] ] ]
		else:
			arr = collect.find({'update': {'$gt': datetime.now() - timedelta(days=d)}}, {'_id': 1, 'update': 1}).sort('_id', 1)
			to_g = [ x for x in arr ]
		#
		count = len(to_g)
		#
		_g = []
		#
		s = sorted(to_g, key=lambda x: struct.unpack("!L", socket.inet_aton(x['_id']))[0])
		#
		for i in s:
			_g.append({ 'ip': i['_id'], 'last_update': i['update'], 'dns': dns(i['_id']) })
		return { 'days_before': d, 'total': count, 'result': _g }
	except Exception, e:
		return { 'error': str(e) }

def fDate(date):
	try:
		return date.strftime("%d/%m/%Y - %H:%M:%S")
	except Exception, e:
		return date

def dns(ip):
	try:
		return socket.gethostbyaddr(ip)[0]
	except Exception, e:
		return "No dns record found"

def is_even(data):
	try:
		if int(data) % 2:
			return True
		else:
			return False
	except:
		return False

def reLog(string):
	try:
		with open(LOGFILE, "a") as f:
			f.write(datetime.now().strftime("[%d/%b/%Y:%H:%M:%S]")+" - "+string+"\n")
	except Exception, e:
		return "Error: " + str(e)


def vlan(_dict):
	#
	host = _dict['host'].lower()
	devices = {}
	errors = []
	dev = None
	#
	devices['bora'] = ['SSH@Bora', '10.0.0.4']
	devices['maestrale'] = ['SSH@Maestrale', '10.0.0.1']
	devices['dr-backup'] = ['SSH@DR-Backup', '10.0.0.29']
	devices['fws-distretti'] = ['SSH@fws-distretti', '10.0.0.153']
	devices['kamsin'] = ['SSH@Kamsin', '10.0.0.62']
	devices['genotyping'] = ['SSH@Genotyping', '10.0.0.25']
	devices['genotyping-2'] = ['SW-SSH@Genotyping-2', '10.0.0.28']
	devices['zefiro'] = ['SSH@zefiro', '10.0.0.45']
	devices['poe-vt'] = ['SSH@Poe-VT', '10.0.0.152']
	devices['poe-cr'] = ['SSH@poe-cr', '10.0.0.151']
	devices['scirocco'] = ['SSH@Scirocco', '10.0.0.6']
	#
	devices['louisiane'] = ['Louisiane', '10.0.0.5']
	devices['quebec'] = ['Quebec', '10.0.0.2']
	#
	devices['catddn-down'] = ['CatDDN-Down', '10.0.0.68']
	devices['catddn-up'] = ['CatDDN-Up', '10.0.0.67']
	#
	devices['feun'] = ['Slot-\d Feun', '10.0.0.61']
	#
	devices['amministrazione'] = ['Amministrazione', '10.0.0.150']
	devices['hp2626-dmz'] = ['HP2626-DMZ', '10.0.0.23']
	devices['hp2828clusterup'] = ['HP2828ClusterUp', '10.0.0.21']
	devices['hp2848clusterdown'] = ['HP2848ClusterDown', '10.0.0.22']
	#
	devices['medusa-sw'] = ['Medusa-SW', '10.0.0.211']
	#
	isHP = False
	#
	if _dict['host'] in cisco_dev:
		dev = NetDevices.Cisco(name=host+"vlan")
	elif _dict['host'] in extreme_dev:
		dev = NetDevices.Extreme(name=host+"vlan")
	elif _dict['host'] in force10_dev:
		dev = NetDevices.Force10(name=host+"vlan")
	elif _dict['host'] in hp_dev:
		isHP = True
		dev = NetDevices.HP(name=host+"vlan")
	elif _dict['host'] in h3c_dev:
		dev = NetDevices.H3C(name=host+"vlan")
	else:
		dev = NetDevices.Brocade(name=host+"vlan")
	#
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	res1 = []
	_res = []
	res = dev.getVlans()
	res1 = dev.getPorts()
	if isHP: 
		res2 = dev.getTrunks()
		for i in res2.keys():
			for z in res2[i]:
				res1.remove(z)
		res1 = res1 + sorted(res2.keys())
	#
	for i in res1:
		if "vlan" not in i.lower() and "manage" not in i.lower():
			_res.append(i)
	res1 = _res
	#
	dev.close()
	#
	#
	if not res or not res1:
		errors.append("Somethings was wrong!")
	return res, res1, errors


def vlanSet(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	vlanT = [ int(x) for x in _dict['vlan-tag'] if x ]
	vlanU = [ int(x) for x in [_dict['vlan-untag']] if x ]
	devices = {}
	errors = []
	dev = None
	#
	devices['bora'] = ['SSH@Bora', '10.0.0.4']
	devices['maestrale'] = ['SSH@Maestrale', '10.0.0.1']
	devices['dr-backup'] = ['SSH@DR-Backup', '10.0.0.29']
	devices['fws-distretti'] = ['SSH@fws-distretti', '10.0.0.153']
	devices['kamsin'] = ['SSH@Kamsin', '10.0.0.62']
	devices['genotyping'] = ['SSH@Genotyping', '10.0.0.25']
	devices['genotyping-2'] = ['SW-SSH@Genotyping-2', '10.0.0.28']
	devices['zefiro'] = ['SSH@zefiro', '10.0.0.45']
	devices['poe-vt'] = ['SSH@Poe-VT', '10.0.0.152']
	devices['poe-cr'] = ['SSH@poe-cr', '10.0.0.151']
	devices['scirocco'] = ['SSH@Scirocco', '10.0.0.6']
	#
	devices['louisiane'] = ['Louisiane', '10.0.0.5']
	devices['quebec'] = ['Quebec', '10.0.0.2']
	#
	devices['catddn-down'] = ['CatDDN-Down', '10.0.0.68']
	devices['catddn-up'] = ['CatDDN-Up', '10.0.0.67']
	#
	devices['feun'] = ['Slot-\d Feun', '10.0.0.61']
	#
	devices['amministrazione'] = ['Amministrazione', '10.0.0.150']
	devices['hp2626-dmz'] = ['HP2626-DMZ', '10.0.0.23']
	devices['hp2828clusterup'] = ['HP2828ClusterUp', '10.0.0.21']
	devices['hp2848clusterdown'] = ['HP2848ClusterDown', '10.0.0.22']
	#
	devices['medusa-sw'] = ['Medusa-SW', '10.0.0.211']
	#
	_brocade = False
	#
	if _dict['host'] in cisco_dev:
		dev = NetDevices.Cisco(name=host+"vlan")
	elif _dict['host'] in extreme_dev:
		dev = NetDevices.Extreme(name=host+"vlan")
	elif _dict['host'] in force10_dev:
		dev = NetDevices.Force10(name=host+"vlan")
	elif _dict['host'] in hp_dev:
		dev = NetDevices.HP(name=host+"vlan")
	elif _dict['host'] in h3c_dev:
		dev = NetDevices.H3C(name=host+"vlan")
	else:
		dev = NetDevices.Brocade(name=host+"vlan")
		if host in ['bora', 'maestrale']:	_brocade = True
	#
	res = ""
	#
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	_port = dev.getPortVlan(port)
	#
	# #1 Remove tagged vlan to port
	if list(set(_port[port]['tagged']) - set(vlanT)):
		if _brocade:
			_res = dev.setPortVlan(port=port, vlan={"id": 5000, "tagged": True, "remove": list(set(_port[port]['tagged']) - set(vlanT))}, novsrp=False)
		else:	
			_res = dev.setPortVlan(port=port, vlan={"id": 5000, "tagged": True, "remove": list(set(_port[port]['tagged']) - set(vlanT))})
		if type(_res) is dict:	errors.append(_res['error'])
		print _res
		#
		if not vlanT:	to_res = "ALL"
		else:	to_res = ",".join([str(x) for x in list(set(_port[port]['tagged']) - set(vlanT))])	
		res += "Remove %s tagged vlans" % to_res
		#
		_port = dev.getPortVlan(port)
		print "1", _port
	#
	#
	#
	# #2 Change untagged vlan to port
	if vlanU and vlanU[0] != _port[port]['untagged'][0]:
		if _brocade:
			_res = dev.setPortVlan(port=port, vlan={"id": vlanU[0], "tagged": False, "remove": []}, novsrp=False)
		else:
			_res = dev.setPortVlan(port=port, vlan={"id": vlanU[0], "tagged": False, "remove": []})
		if type(_res) is dict:	errors.append(_res['error'])
		print _res
		#
		if res: res += "; "
		res += "Change untagged vlan from %d to %d, on port %s" % (_port[port]['untagged'][0], vlanU[0], port)
		#
		_port = dev.getPortVlan(port)
		print "2", _port
	#
	#
	#
	# #3 Add tagged vlan to port
	if set(_port[port]['untagged']) <= set(vlanT):
		dev.close()
		errors.append("You are trying to set vlan %d as tagged and untagged in the same port!" % _port[port]['untagged'][0])
		return res, errors
	if list(set(vlanT) - set(_port[port]['tagged'])):
		for i in list(set(vlanT) - set(_port[port]['tagged'])):
			if _brocade:
				_res = dev.setPortVlan(port=port, vlan={"id": i, "tagged": True, "remove": []}, novsrp=False)
			else:
				_res = dev.setPortVlan(port=port, vlan={"id": i, "tagged": True, "remove": []})
			if type(_res) is dict:	errors.append(_res['error'])
			print _res
		#
		if res: res += "; "
		res += "Add tagged vlans %s, on port %s" % (",".join([str(x) for x in list(set(vlanT) - set(_port[port]['tagged']))]), port)
		print dev.run(" ")
		print dev.run(" ")
		print dev.run(" ")
		#
		_port = dev.getPortVlan(port)
		print "3", _port
	#
	#
	#
	#res = dev.setPortVlan(port=port, vlan={"id": 0, "tagged": True, "remove": []})
	#
	#if type(res) is dict:
	#	errors.append(res['error'])
	#
	dev.close()
	#
	#
	if not res:
		errors.append("Somethings was wrong!")
	return res, errors


def portsVlan(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	devices = {}
	errors = []
	dev = None
	#
	devices['bora'] = ['SSH@Bora', '10.0.0.4']
	devices['maestrale'] = ['SSH@Maestrale', '10.0.0.1']
	devices['dr-backup'] = ['SSH@DR-Backup', '10.0.0.29']
	devices['fws-distretti'] = ['SSH@fws-distretti', '10.0.0.153']
	devices['kamsin'] = ['SSH@Kamsin', '10.0.0.62']
	devices['genotyping'] = ['SSH@Genotyping', '10.0.0.25']
	devices['genotyping-2'] = ['SW-SSH@Genotyping-2', '10.0.0.28']
	devices['zefiro'] = ['SSH@zefiro', '10.0.0.45']
	devices['poe-vt'] = ['SSH@Poe-VT', '10.0.0.152']
	devices['poe-cr'] = ['SSH@poe-cr', '10.0.0.151']
	devices['scirocco'] = ['SSH@Scirocco', '10.0.0.6']
	#
	devices['louisiane'] = ['Louisiane', '10.0.0.5']
	devices['quebec'] = ['Quebec', '10.0.0.2']
	#
	devices['catddn-down'] = ['CatDDN-Down', '10.0.0.68']
	devices['catddn-up'] = ['CatDDN-Up', '10.0.0.67']
	#
	devices['feun'] = ['Slot-\d Feun', '10.0.0.61']
	#
	devices['amministrazione'] = ['Amministrazione', '10.0.0.150']
	devices['hp2626-dmz'] = ['HP2626-DMZ', '10.0.0.23']
	devices['hp2828clusterup'] = ['HP2828ClusterUp', '10.0.0.21']
	devices['hp2848clusterdown'] = ['HP2848ClusterDown', '10.0.0.22']
	#
	devices['medusa-sw'] = ['Medusa-SW', '10.0.0.211']
	#
	if host in cisco_dev:
		dev = NetDevices.Cisco(name=host+"port")
	elif host in extreme_dev:
		dev = NetDevices.Extreme(name=host+"port")
	elif host in force10_dev:
		dev = NetDevices.Force10(name=host+"port")
	elif host in hp_dev:
		dev = NetDevices.HP(name=host+"port")
	elif host in h3c_dev:
		dev = NetDevices.H3C(name=host+"port")
	else:
		dev = NetDevices.Brocade(name=host+"port")
	#
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	res = dev.getPortVlan(port)
	#
	dev.close()
	#
	#
	if not res:
		errors.append("Somethings was wrong!")
	return res, errors


def runCmdBrocade(_dict):
	#
	host = _dict['host'].lower()
	command = _dict['command']
	devices = {}
	errors = []
	#
	devices['bora'] = ['SSH@Bora', '10.0.0.4']
	devices['maestrale'] = ['SSH@Maestrale', '10.0.0.1']
	devices['dr-backup'] = ['SSH@DR-Backup', '10.0.0.29']
	devices['fws-distretti'] = ['SSH@fws-distretti', '10.0.0.153']
	devices['kamsin'] = ['SSH@Kamsin', '10.0.0.62']
	devices['genotyping'] = ['SSH@Genotyping', '10.0.0.25']
	devices['genotyping-2'] = ['SW-SSH@Genotyping-2', '10.0.0.28']
	devices['zefiro'] = ['SSH@zefiro', '10.0.0.45']
	devices['poe-vt'] = ['SSH@Poe-VT', '10.0.0.152']
	devices['poe-cr'] = ['SSH@poe-cr', '10.0.0.151']
	devices['scirocco'] = ['SSH@Scirocco', '10.0.0.6']
	#
	dev = NetDevices.Brocade(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	res = dev.run(command)
	dev.close()
	#
	#
	if not res:
		errors.append("Somethings was wrong!")
	return res, errors


def runCmdForce10(_dict):
	#
	host = _dict['host'].lower()
	command = _dict['command']
	devices = {}
	errors = []
	#
	devices['louisiane'] = ['Louisiane', '10.0.0.5']
	devices['quebec'] = ['Quebec', '10.0.0.2']
	#
	dev = NetDevices.Force10(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	res = dev.run(command)
	dev.close()
	#
	#
	if not res:
		errors.append("Somethings was wrong!")
	return res, errors


def runCmdCisco(_dict):
	#
	host = _dict['host'].lower()
	command = _dict['command']
	devices = {}
	errors = []
	#
	devices['catddn-down'] = ['CatDDN-Down', '10.0.0.68']
	devices['catddn-up'] = ['CatDDN-Up', '10.0.0.67']
	#
	dev = NetDevices.Cisco(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	res = dev.run(command)
	dev.close()
	#
	#
	if not res:
		errors.append("Somethings was wrong!")
	return res, errors


def runCmdHP(_dict):
	#
	host = _dict['host'].lower()
	command = _dict['command']
	devices = {}
	errors = []
	#
	devices['amministrazione'] = ['Amministrazione', '10.0.0.150']
	devices['hp2626-dmz'] = ['HP2626-DMZ', '10.0.0.23']
	devices['hp2828clusterup'] = ['HP2828ClusterUp', '10.0.0.21']
	devices['hp2848clusterdown'] = ['HP2848ClusterDown', '10.0.0.22']
	#
	dev = NetDevices.HP(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	res = dev.run(command)
	dev.close()
	#
	#
	if not res:
		errors.append("Somethings was wrong!")
	return res, errors


def runCmdExtreme(_dict):
	#
	host = _dict['host'].lower()
	command = _dict['command']
	devices = {}
	errors = []
	#
	devices['feun'] = ['Slot-\d Feun', '10.0.0.61']
	#
	dev = NetDevices.Extreme(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	res = dev.run(command)
	dev.close()
	#
	#
	if not res:
		errors.append("Somethings was wrong!")
	return res, errors


def runCmdH3C(_dict):
	#
	host = _dict['host'].lower()
	command = _dict['command']
	devices = {}
	errors = []
	#
	devices['medusa-sw'] = ['Medusa-SW', '10.0.0.211']
	#
	dev = NetDevices.H3C(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	res = dev.run(command)
	dev.close()
	#
	#
	if not res:
		errors.append("Somethings was wrong!")
	return res, errors


def portDetBrocade(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	devices = {}
	errors = []
	#
	devices['bora'] = ['SSH@Bora', '10.0.0.4']
	devices['maestrale'] = ['SSH@Maestrale', '10.0.0.1']
	devices['dr-backup'] = ['SSH@DR-Backup', '10.0.0.29']
	devices['fws-distretti'] = ['SSH@fws-distretti', '10.0.0.153']
	devices['kamsin'] = ['SSH@Kamsin', '10.0.0.62']
	devices['genotyping'] = ['SSH@Genotyping', '10.0.0.25']
	devices['genotyping-2'] = ['SW-SSH@Genotyping-2', '10.0.0.28']
	devices['zefiro'] = ['SSH@zefiro', '10.0.0.45']
	devices['poe-vt'] = ['SSH@Poe-VT', '10.0.0.152']
	devices['poe-cr'] = ['SSH@poe-cr', '10.0.0.151']
	devices['scirocco'] = ['SSH@Scirocco', '10.0.0.6']
	#
	dev = NetDevices.Brocade(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	if port:	
		toW = dev.getPortsDetail(port)
		res = toW[port]
		dev.close()
	else:	
		toW = dev.getPortsDetail()
		for i in toW:
			res += str(toW[i]) + "\n--------------------------------------------------------------------- \n\n"
		dev.close()
	#
	#
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def portDetForce10(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	devices = {}
	errors = []
	#
	devices['louisiane'] = ['Louisiane', '10.0.0.5']
	devices['quebec'] = ['Quebec', '10.0.0.2']
	#
	dev = NetDevices.Force10(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	if port:	
		toW = dev.getPortsDetail(port)
		res = toW[toW.keys()[0]]
		dev.close()
	else:	
		toW = dev.getPortsDetail()
		for i in toW:
			res += str(toW[i]) + "\n--------------------------------------------------------------------- \n\n"
		dev.close()
	#
	#print toW
	#
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def portDetCisco(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	devices = {}
	errors = []
	#
	devices['catddn-down'] = ['CatDDN-Down', '10.0.0.68']
	devices['catddn-up'] = ['CatDDN-Up', '10.0.0.67']
	#
	dev = NetDevices.Cisco(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	if port:	
		toW = dev.getPortsDetail(port)
		res = toW[toW.keys()[0]]
		dev.close()
	else:	
		toW = dev.getPortsDetail()
		for i in toW:
			res += str(toW[i]) + "\n--------------------------------------------------------------------- \n\n"
		dev.close()
	#
	#
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def portDetHP(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	devices = {}
	errors = []
	#
	devices['amministrazione'] = ['Amministrazione', '10.0.0.150']
	devices['hp2626-dmz'] = ['HP2626-DMZ', '10.0.0.23']
	devices['hp2828clusterup'] = ['HP2828ClusterUp', '10.0.0.21']
	devices['hp2848clusterdown'] = ['HP2848ClusterDown', '10.0.0.22']
	#
	dev = NetDevices.HP(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	if port:	
		toW = dev.getPortsDetail(port)
		res = toW[toW.keys()[0]]
		dev.close()
	else:	
		toW = dev.getPortsDetail()
		for i in toW:
			res += str(toW[i]) + "\n--------------------------------------------------------------------- \n\n"
		dev.close()
	#
	#
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def portDetExtreme(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	devices = {}
	errors = []
	#
	devices['feun'] = ['Slot-\d Feun', '10.0.0.61']
	#
	dev = NetDevices.Extreme(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	res = ""
	if port:	
		toW = dev.getPortsDetail(port)
	else:	
		toW = dev.getPortsDetail()
	#
	dev.close()
	#
	for i in toW:
		res += i + "\n"
		for r in toW[i]:
			res += str(r) + "\n"
		res += "\n--------------------------------------------------------------------- \n\n"
	#
	#
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def descForce10(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	desc = _dict['desc']
	devices = {}
	errors = []
	#
	devices['louisiane'] = ['Louisiane', '10.0.0.5']
	devices['quebec'] = ['Quebec', '10.0.0.2']
	#
	dev = NetDevices.Force10(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	toW = dev.setPortsDesc(port, desc)
	#
	dev.close()
	#
	res = "<b>Port description %s on %s changed in:</b> %s " % (port, _dict['real-host'], toW[port][0])
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def descBrocade(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	desc = _dict['desc']
	devices = {}
	errors = []
	#
	devices['bora'] = ['SSH@Bora', '10.0.0.4']
	devices['maestrale'] = ['SSH@Maestrale', '10.0.0.1']
	devices['dr-backup'] = ['SSH@DR-Backup', '10.0.0.29']
	devices['fws-distretti'] = ['SSH@fws-distretti', '10.0.0.153']
	devices['kamsin'] = ['SSH@Kamsin', '10.0.0.62']
	devices['genotyping'] = ['SSH@Genotyping', '10.0.0.25']
	devices['genotyping-2'] = ['SW-SSH@Genotyping-2', '10.0.0.28']
	devices['zefiro'] = ['SSH@zefiro', '10.0.0.45']
	devices['poe-vt'] = ['SSH@Poe-VT', '10.0.0.152']
	devices['poe-cr'] = ['SSH@poe-cr', '10.0.0.151']
	devices['scirocco'] = ['SSH@Scirocco', '10.0.0.6']
	#
	dev = NetDevices.Brocade(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	toW = dev.setPortsDesc(port, desc)
	#
	dev.close()
	#
	res = "<b>Port description %s on %s changed in:</b> %s " % (port, _dict['real-host'], toW[port][0])
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def descExtreme(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	desc = _dict['desc']
	devices = {}
	errors = []
	#
	devices['feun'] = ['Slot-\d Feun', '10.0.0.61']
	#
	dev = NetDevices.Extreme(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	toW = dev.setPortsDesc(port, desc)
	#
	dev.close()
	#
	res = "<b>Port description %s on %s changed in:</b> %s " % (port, _dict['real-host'], toW[port][0])
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors

	
def descCisco(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	desc = _dict['desc']
	devices = {}
	errors = []
	#
	devices['catddn-down'] = ['CatDDN-Down', '10.0.0.68']
	devices['catddn-up'] = ['CatDDN-Up', '10.0.0.67']
	#
	dev = NetDevices.Cisco(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	toW = dev.setPortsDesc(port, desc)
	#
	dev.close()
	#
	res = "<b>Port description %s on %s changed in:</b> %s " % (port, _dict['real-host'], toW[port][0])
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def descHP(_dict):
	#
	host = _dict['host'].lower()
	port = _dict['port']
	desc = _dict['desc']
	devices = {}
	errors = []
	#
	devices['amministrazione'] = ['Amministrazione', '10.0.0.150']
	devices['hp2626-dmz'] = ['HP2626-DMZ', '10.0.0.23']
	devices['hp2828clusterup'] = ['HP2828ClusterUp', '10.0.0.21']
	devices['hp2848clusterdown'] = ['HP2848ClusterDown', '10.0.0.22']
	#
	dev = NetDevices.HP(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	toW = dev.setPortsDesc(port, desc)
	#
	dev.close()
	#
	res = "<b>Port description %s on %s changed in:</b> %s " % (port, _dict['real-host'], toW[port][0])
	if not toW:
		errors.append("Somethings was wrong!")
	return res, errors


def Brocade(toSrc, host='maestrale', q=None):
	#
	devices = {}
	desc = {}
	errors = []
	#
	devices['bora'] = ['SSH@Bora', '10.0.0.4', 'Bora']
	devices['maestrale'] = ['SSH@Maestrale', '10.0.0.1', 'Maestrale']
	devices['dr-backup'] = ['SSH@DR-Backup', '10.0.0.29', 'DR-Backup']
	devices['fws-distretti'] = ['SSH@fws-distretti', '10.0.0.153', 'FWS-Distretti']
	devices['kamsin'] = ['SSH@Kamsin', '10.0.0.62', 'Kamsin']
	devices['genotyping'] = ['SSH@Genotyping', '10.0.0.25', 'Genotyping']
	devices['genotyping-2'] = ['SW-SSH@Genotyping-2', '10.0.0.28', 'Genotyping-2']
	devices['zefiro'] = ['SSH@zefiro', '10.0.0.45', 'Zefiro']
	devices['poe-vt'] = ['SSH@Poe-VT', '10.0.0.152', 'Poe-VT']
	devices['poe-cr'] = ['SSH@poe-cr', '10.0.0.151', 'Poe-CR']
	devices['scirocco'] = ['SSH@Scirocco', '10.0.0.6', 'Scirocco']
	dev = NetDevices.Brocade(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	mac = dev.getMacAddress(toSrc)
	#
	try:	
		desc = dev.getPortsAndDesc(mac[toSrc]['port'])
		if not desc:	desc[mac[toSrc]['port']] = "No description found"
	except:
		desc['not found'] = "not found"
		errors.append("Error in desc_Brocade")
	#
	in_host = False
	if mac and 'error' not in mac.keys():
		_port = ""
		if "*" in mac[toSrc]['port']:	_port = mac[toSrc]['port'].split("*")[0]
		elif "-" in mac[toSrc]['port']:	_port = mac[toSrc]['port'].split("-")[0]
		else:	_port = mac[toSrc]['port']
		vlan = dev.getPortVlan(_port)
		if int(mac[toSrc]['vlan']) in vlan[vlan.keys()[0]]['untagged']:
			# print int(mac[toSrc]['vlan']), vlan[vlan.keys()[0]]['untagged']
			in_host = True
		mac[toSrc]['device'] = devices[host][2]
		mac[toSrc]['description'] = desc[desc.keys()[0]]
		# print _port, dev.getName(), vlan, in_host
	else:
		mac = {}
	q.put({ 'mac': mac, 'this': in_host, 'errors': errors })
	dev.close()


def Force10(toSrc, host='quebec', q=None):
	#
	devices = {}
	desc = {}
	errors = []
	#
	devices['louisiane'] = ['Louisiane', '10.0.0.5', 'Louisiane']
	devices['quebec'] = ['Quebec', '10.0.0.2', 'Quebec']
	dev = NetDevices.Force10(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	mac = dev.getMacAddress(toSrc)
	#
	try:	
		desc = dev.getPortsAndDesc(mac[toSrc]['port'])
		if not desc:	desc[mac[toSrc]['port']] = "No description found"
	except:
		desc['not found'] = "not found"
		errors.append("Error in desc_Force10")
	#
	in_host = False
	if mac and 'error' not in mac.keys():
		vlan = dev.getPortVlan(mac[toSrc]['port'])
		if int(mac[toSrc]['vlan']) in vlan[vlan.keys()[0]]['untagged']:
			in_host = True
		mac[toSrc]['device'] = devices[host][2]
		mac[toSrc]['description'] = desc[desc.keys()[0]]
	else:
		mac = {}
	q.put({ 'mac': mac, 'this': in_host, 'errors': errors })
	dev.close()


def Extreme(toSrc, host='feun', q=None):
	#
	devices = {}
	desc = {}
	errors = []
	#
	devices['feun'] = ['Slot-\d Feun', '10.0.0.61', 'Feun']
	dev = NetDevices.Extreme(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	mac = dev.getMacAddress(toSrc)
	#
	try:	
		desc = dev.getPortsAndDesc(mac[toSrc]['port'])
		if not desc:	desc[mac[toSrc]['port']] = "No description found"
	except:
		desc['not found'] = "not found"
		errors.append("Error in desc_Extreme")
	#
	in_host = False
	if mac and 'error' not in mac.keys():
		vlan = dev.getPortVlan(mac[toSrc]['port'])
		if int(mac[toSrc]['vlan']) in vlan[vlan.keys()[0]]['untagged']:
			in_host = True
		mac[toSrc]['device'] = devices[host][2]
		mac[toSrc]['description'] = desc[desc.keys()[0]]
	else:
		mac = {}
	q.put({ 'mac': mac, 'this': in_host, 'errors': errors })
	dev.close()


def Cisco(toSrc, host='catddn-down', q=None):
	#
	devices = {}
	desc = {}
	errors = []
	#
	devices['catddn-down'] = ['CatDDN-Down', '10.0.0.68', 'CatDDN-Down']
	devices['catddn-up'] = ['CatDDN-Up', '10.0.0.67', 'CatDDN-Up']
	dev = NetDevices.Cisco(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	mac = dev.getMacAddress(toSrc)
	#
	try:	
		desc = dev.getPortsAndDesc(mac[toSrc]['port'])
		if not desc:	desc[mac[toSrc]['port']] = "No description found"
	except:
		desc['not found'] = "not found"
		errors.append("Error in desc_Cisco")
	#
	in_host = False
	if mac and 'error' not in mac.keys():
		vlan = dev.getPortVlan(mac[toSrc]['port'])
		if int(mac[toSrc]['vlan']) in vlan[vlan.keys()[0]]['untagged']:
			in_host = True
		mac[toSrc]['device'] = devices[host][2]
		mac[toSrc]['description'] = desc[desc.keys()[0]]
	else:
		mac = {}
	q.put({ 'mac': mac, 'this': in_host, 'errors': errors })
	dev.close()


def HP(toSrc, host='amministrazione', q=None):
	#
	devices = {}
	desc = {}
	errors = []
	#
	devices['amministrazione'] = ['Amministrazione', '10.0.0.150', 'Amministrazione']
	devices['hp2626-dmz'] = ['HP2626-DMZ', '10.0.0.23', 'HP2626-DMZ']
	devices['hp2828clusterup'] = ['HP2828ClusterUp', '10.0.0.21', 'HP2828ClusterUp']
	devices['hp2848clusterdown'] = ['HP2848ClusterDown', '10.0.0.22', 'HP2848ClusterDown']
	dev = NetDevices.HP(name=host)
	dev.connect(dev_username,dev_password,dev_secret,devices[host][1],devices[host][0])
	mac = dev.getMacAddress(toSrc)
	#
	try:	
		desc = dev.getPortsAndDesc(mac[toSrc]['port'])
		if not desc:	desc[mac[toSrc]['port']] = "No description found"
	except:
		desc['not found'] = "not found"
		errors.append("Error in desc_HP")
	#
	in_host = False
	if mac and 'error' not in mac.keys():
		vlan = dev.getPortVlan(mac[toSrc]['port'])
		if int(mac[toSrc]['vlan']) in vlan[vlan.keys()[0]]['untagged']:
			in_host = True
		mac[toSrc]['device'] = devices[host][2]
		mac[toSrc]['description'] = desc[desc.keys()[0]]
	else:
		mac = {}
	q.put({ 'mac': mac, 'this': in_host, 'errors': errors })
	dev.close()

def qStat(q, toSrc):
	r_queue = []
	count = 0
	#
	while True:
		if q.qsize():
			item = q.get()
			r_queue.append(item)
		else:
			break
	mac = []
	errors = []
	#
	for i in r_queue:
		# print i
		if i['mac'] and i['this']:
			mac.append(i['mac'][toSrc])
		errors += i['errors']
	#
	if not mac:
		for i in r_queue:
			if i['mac']:
				mac.append(i['mac'][toSrc])
	#
	for i in range(len(mac)):
		mac[i]["count"] = count
		count += 1
	#
	return mac, errors


def portDescGet(toSrc="00:01:E8:D5:D1:03"):
	q = Queue()
	#
	thread_l = [ 
	threading.Thread(target=Brocade, name='bora', args=(toSrc, 'bora', q,)),
	threading.Thread(target=Brocade, name='maestrale', args=(toSrc, 'maestrale', q,)),
	threading.Thread(target=Brocade, name='kamsin', args=(toSrc, 'kamsin', q,)),
	threading.Thread(target=Brocade, name='dr-backup', args=(toSrc, 'dr-backup', q,)),
	threading.Thread(target=Brocade, name='genotyping', args=(toSrc, 'genotyping', q,)),
	threading.Thread(target=Brocade, name='genotyping-2', args=(toSrc, 'genotyping-2', q,)),
	threading.Thread(target=Brocade, name='fws-distretti', args=(toSrc, 'fws-distretti', q,)),
	threading.Thread(target=Brocade, name='zefiro', args=(toSrc, 'zefiro', q,)),
	threading.Thread(target=Brocade, name='scirocco', args=(toSrc, 'scirocco', q,)),
	threading.Thread(target=Brocade, name='poe-vt', args=(toSrc, 'poe-vt', q,)),
	threading.Thread(target=Brocade, name='poe-cr', args=(toSrc, 'poe-cr', q,)),
	threading.Thread(target=Force10, name='louisiane', args=(toSrc, 'louisiane', q,)),
	threading.Thread(target=Force10, name='quebec', args=(toSrc, 'quebec', q,)),
	threading.Thread(target=Cisco, name='catddn-down', args=(toSrc, 'catddn-down', q,)),
	threading.Thread(target=Cisco, name='catddn-up', args=(toSrc, 'catddn-up', q,)),
	threading.Thread(target=Extreme, name='feun', args=(toSrc, 'feun', q,)),
	threading.Thread(target=HP, name='amministrazione', args=(toSrc, 'amministrazione', q,)),
	threading.Thread(target=HP, name='hp2828clusterup', args=(toSrc, 'hp2828clusterup', q,)),
	threading.Thread(target=HP, name='hp2848clusterdown', args=(toSrc, 'hp2848clusterdown', q,)),
	threading.Thread(target=HP, name='hp2626-dmz', args=(toSrc, 'hp2626-dmz', q,))
	]
	#
	for thread in thread_l:
		thread.start() 
	#
	for thread in thread_l:
		thread.join()
	#
	mac, errors = qStat(q, toSrc)
	#
	#
	return mac, errors


app.jinja_env.globals.update(fDate=fDate)
app.jinja_env.globals.update(dns=dns)
app.jinja_env.globals.update(is_even=is_even)


if __name__ == "__main__":
	app.run(debug=DEBUG, host='0.0.0.0', port=8000, threaded=True)
